#ifndef __NOTIMPLEMENTEDEXCEPTION_H
#define __NOTIMPLEMENTEDEXCEPTION_H

#include <exception>
#include <string>

namespace op { namespace error
{
	class NotImplementedException : public std::exception
	{
	public:

		NotImplementedException() = default;

		NotImplementedException(const std::string& message);

		virtual ~NotImplementedException();

	private:
	};
}}

#endif