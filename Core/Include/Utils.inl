//
#include <sstream>


namespace core { namespace utils
{
	template<typename T>
	std::vector< std::vector< T > > SplitVector(const std::vector<T>& inputVector, size_t subvectorSize)
	{
		if (subvectorSize < 2)
		{
			throw std::invalid_argument("subvectorSize < 2");
		}

		std::vector< std::vector< T > > subVectors;
		for (size_t index = 0;; index++)
		{
			size_t subvectorStart	= index * (subvectorSize-1);
			size_t subvectorEnd		= (index + 1) * (subvectorSize-1) + 1;
				
			if (subvectorEnd > inputVector.size())
			{
				subvectorEnd = inputVector.size();
			}

			//subVectors.push_back(std::vector<T>( startIter, endIter));
			subVectors.push_back(std::vector<T>(inputVector.begin() + subvectorStart, inputVector.begin() + subvectorEnd));

			if (subvectorEnd ==inputVector.size())
			{
				break;
			}
		}

		return subVectors;
	}



	template<typename T>
	std::string VectorToString(const std::vector<T>& inputVector, std::function<std::string(T)> toStrConverter)
	{
		std::ostringstream oss;
		oss << "[";
		for (auto iter = inputVector.begin(); iter != inputVector.end(); iter++)
		{
			oss << toStrConverter(*iter);

			if (iter != inputVector.end())
			{
				oss << ",";
			}
		}
		oss << "]";

		return oss.str();
	}
}}
