#ifndef UTILS_H
#define UTILS_H

#include <functional>
#include <ostream>
#include <vector>

namespace core { namespace utils
{
	template<typename T>
	std::vector< std::vector< T > > SplitVector(const std::vector<T>& inputVector, size_t subvectorSize);


	template<typename T>
	std::string VectorToString(const std::vector<T>& inputVector, std::function<std::string(T)> tToStrConverter );


	template < typename T1 >
	void ToStream(std::ostream& s, T1& t1)
	{
		s << t1;
	}

	template < typename T1, typename ... ArgTypes >
	void ToStream(std::ostream& s, T1& t1, ArgTypes ... args)
	{
		s << t1;
		ToStream(s, args...);
	}


	template < typename ... ArgTypes >
	std::string ToString(ArgTypes ... args)
	{
		std::stringstream ss;
		ToStream(ss, args...);

		return ss.str();
	}

}}



#include "Utils.inl"

#endif
