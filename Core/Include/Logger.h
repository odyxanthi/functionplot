#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <ostream>
#include <fstream>

namespace core
{
	class Logger
	{
	public:
		Logger();

		Logger(const std::string& filename);

		virtual ~Logger();

	public:

		void Write(const std::string& msg);

		void WriteLine(const std::string& msg);

	public:

		template <typename T>
		std::ostream& operator << (const T& t);

	private:

		const std::string filename;
		std::ofstream ofs;
		

	};
}


#endif
