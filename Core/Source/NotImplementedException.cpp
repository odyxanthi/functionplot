#include "NotImplementedException.h"


namespace op { namespace error 
{
	NotImplementedException::NotImplementedException( const std::string& message)
		: std::exception(message.c_str())
	{}

	NotImplementedException::~NotImplementedException()
	{}

}}
