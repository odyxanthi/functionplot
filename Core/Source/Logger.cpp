#include "Logger.h"

namespace core
{
	Logger::Logger(const std::string& filename_)
		:
		filename(filename_)
	{
		//this->ofs.exceptions(std::ofstream::failbit | std::ofstream::badbit);		// will throw exception when failbit or badbit are set.
		this->ofs.open(this->filename.c_str(), std::ofstream::out);
	}


	Logger::Logger() : Logger::Logger("FunctionVisualizer.Log.txt")
	{}

	
	Logger::~Logger()
	{
		this->ofs.close();  
	}


	void Logger::Write(const std::string& msg)
	{
		this->ofs << msg;
		this->ofs.flush();
	}


	void Logger::WriteLine(const std::string& msg)
	{
		this->ofs << msg << std::endl;
		this->ofs.flush();
	}


	template <typename T>
	std::ostream& Logger::operator << (const T& t)
	{
		this->ofs << t;
		this->ofs.flush();

		return this->ofs;
	}
}
