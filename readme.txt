Creates the plot of a mathematical function of 2 variables, within a bounded area specified by the user.
It uses the Python interpreter to evaluate the function. The Python module that does the evaluation is included in the code.
