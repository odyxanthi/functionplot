

namespace op { namespace lib3D 
{

	template <class vector3d_Range>
	PolylineModel3D::PolylineModel3D(const vector3d_Range& v3d_range)
	{
		for (vector3d& v : v3d_range)
		{
			_points3D.push_back(v);
		}

		this->UpdateRendering();
	}


	template <class IterType>
	void PolylineModel3D::SetPoints(IterType rangeBegin, IterType rangeEnd)
	{
		_points3D.clear();
		for (auto it = rangeBegin; it != rangeEnd; ++it)
		{
			vector3d v(it->X(), it->Y(), it->Z());
			_points3D.push_back(v);
		}
		this->UpdateRendering();
	}

}}
