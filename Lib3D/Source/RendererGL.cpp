#include "stdafx.h"
#include "RendererGL.h"
#include "Material.h"
#include "Vector.h"
#include "Transform3D.h"
#include "HardwareBufferGL.h"
#include "MatrixUtils.h"
#include "FontManager.h"
#include "TextModel3D.h"
#include "FontFaceGL.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"
#include <glm/gtx/transform.hpp>
#include "glm/gtc/matrix_transform.hpp"

#include <fstream>

#include "ObjImporter.h"
#include "lodepng.h"

using namespace std;
using op::math::vector3d;



const char* BASIC_LIGHTING = "BasicLighting";
const char* BASIC_TEXTURING = "BasicTexturing";
const char* COLOR_MATERIAL = "ColorMaterial";
const char* ZCOORD_COLORMAP = "ZCoordColorMap";



namespace op { namespace lib3D
{
	void printShaderInfoLog(GLint shader);
	char* loadFile(const char *fname, GLint &fSize);


	RendererGL::RendererGL()
	{
		_fontMgr = make_unique<FontManager>();
	}


	RendererGL::~RendererGL()
	{}


	void RendererGL::InitializeShaders()
	{
		this->CreateShader(".\\Shaders\\BasicLighting.vert", ".\\Shaders\\BasicLighting.frag", BASIC_LIGHTING);
		this->CreateShader(".\\Shaders\\BasicTexturing.vert", ".\\Shaders\\BasicTexturing.frag", BASIC_TEXTURING);
		this->CreateShader(".\\Shaders\\ZCoordColorMap.vert", ".\\Shaders\\ZCoordColorMap.frag", ZCOORD_COLORMAP);
		this->CreateShader(".\\Shaders\\BasicColorMaterial.vert", ".\\Shaders\\BasicLighting.frag", COLOR_MATERIAL);
		this->CreateShader(".\\Shaders\\minimal.vert", ".\\Shaders\\minimal.frag", "minimal");
	}

	void RendererGL::EnableLighting(bool enable)
	{
		if (enable)
			glEnable(GL_LIGHTING);
		else
			glDisable(GL_LIGHTING);
	}


	void RendererGL::SetMaterial(const Material& material)
	{
		glDisable(GL_COLOR_MATERIAL);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, &(material.diffuse[0]) );
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, &(material.ambient[0]));
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, &(material.specular[0]));

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_LIGHTING);

		this->UseShader(BASIC_LIGHTING);
	}

	void RendererGL::SetColorMaterial(const Color& color)
	{
		glColor4f(color.R(), color.G(), color.B(), color.A());
		glDisable(GL_LIGHTING);
		this->UseShader(COLOR_MATERIAL);
	}

	void RendererGL::SetMaterial(int)
	{
		glDisable(GL_COLOR_MATERIAL);
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		this->UseShader(BASIC_TEXTURING);
	}

	void RendererGL::DrawMesh(const lib3D::Mesh3D& mesh)
	{}


	void RendererGL::DrawPoint(const math::vector3d& point)
	{}


	void RendererGL::DrawPoint(const math::vector3f& point)
	{}


	void RendererGL::DrawLine(const math::vector3d& start, const math::vector3d& end)
	{
		glBegin(GL_LINES);
		glVertex3d(start.X(), start.Y(), start.Z());
		glVertex3d(end.X(), end.Y(), end.Z());
		glEnd();
	}


	void RendererGL::DrawLine(const math::vector3f& start, const math::vector3f& end)
	{
		glBegin(GL_LINES);
		glVertex3f(start.X(), start.Y(), start.Z());
		glVertex3f(end.X(), end.Y(), end.Z());
		glEnd();

	}


	void RendererGL::DrawText(const std::string& text, const op::math::vector3d& origin, const op::math::vector3d& dirX, const op::math::vector3d& dirY, float textHeight)
	{
		if (_activeFontFace == nullptr)
		{
			_activeFontFace = std::make_unique<FontFaceGL>(_fontMgr->GetDefaultFontFace());
		}

		if (_activeFontFace != nullptr)
			_activeFontFace->DrawText(text, origin, dirX, dirY, textHeight);
	}

	//void RendererGL::SetModelTransform(const std::array<float, 16>& t)
	//{
	//	this->modelTransform = t;
	//}

	//void RendererGL::SetCameraTransform(const std::array<float, 16>& t)
	//{
	//	this->cameraTransform = t;
	//}

	//void RendererGL::SetProjectionTransform(const std::array<float, 16>& t)
	//{
	//	this->projectionTranform = t;
	//}


	unsigned int RendererGL::CreateHardwareBuffer(const char* filename)
	{
		if (filename == nullptr)
		{
			return 0;
		}

		op::lib3D::ObjImporter importer;
		auto mesh = importer.ReadMesh(std::string(filename));
		if (mesh)
		{
			return this->CreateHardwareBuffer(*mesh);
		}
		
		return 0;
	}


	unsigned int RendererGL::CreateHardwareBuffer(const Mesh3D& mesh)
	{
		auto buffer = std::make_unique<HardwareBufferGL>(mesh);

		unsigned int id = 0;
		if (buffer)
		{
			id = buffer->GetBufferId();
			this->_vboMap[buffer->GetBufferId()] = std::move(buffer);
		}

		return id;
	}


	unsigned int RendererGL::CreateHardwareBuffer(const std::vector<math::vector3d>& points)
	{
		throw std::exception("CreateHardwareBuffer is not implemented\n");
		return 0;
	}


	void RendererGL::DrawHardwareBuffer(unsigned int bufferId)
	{
		auto iter = this->_vboMap.find(bufferId);
		if (iter != this->_vboMap.end())
		{
			iter->second->Draw();
		}
	}


	void RendererGL::DeleteHardwareBuffer(unsigned int bufferId)
	{
		auto iter = this->_vboMap.find(bufferId);
		if (iter != this->_vboMap.end())
		{
			this->_vboMap.erase(iter);
		}
	}


	
	unsigned int RendererGL::CreateTexture2D(const char* textureFile)
	{
		if (textureFile == nullptr)
		{
			return 0;
		}

		// Load file and decode image.
		std::vector<unsigned char> image;
		unsigned width, height;
		unsigned error = lodepng::decode(image, width, height, textureFile);
		if (error)
		{
			return 0;
		}


		GLuint texId = 0;

		glGenTextures(1, &texId);

		glBindTexture(GL_TEXTURE_2D, texId);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &(image[0]));

		return texId;
	}


	void RendererGL::LoadTexture(unsigned int textureId)
	{

	}


	unsigned int RendererGL::CreateShader(const char* vertexShaderFile, const char* fragShaderFile, const char* shaderName )
	{
		const unsigned char* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);

		GLuint f, v;
		GLuint shaderId = 0;

		char *vs, *fs;

		v = glCreateShader(GL_VERTEX_SHADER);
		f = glCreateShader(GL_FRAGMENT_SHADER);

		// load shaders & get length of each
		GLint vlen;
		GLint flen;
		vs = loadFile(vertexShaderFile, vlen);
		fs = loadFile(fragShaderFile, flen);

		const char * vv = vs;
		const char * ff = fs;

		glShaderSource(v, 1, &vv, &vlen);
		glShaderSource(f, 1, &ff, &flen);

		GLint compiled;

		std::cout << "compiling vertex shader : " << vertexShaderFile << "\n";
		glCompileShader(v);
		glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
		if (compiled)
		{
			std::cout << "compiling fragment shader : " << fragShaderFile << "\n";
			glCompileShader(f);
			glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
			if (compiled)
			{
				shaderId = glCreateProgram();

				glBindAttribLocation(shaderId, 0, "in_Position");
				glBindAttribLocation(shaderId, 1, "in_Color");

				glAttachShader(shaderId, v);
				glAttachShader(shaderId, f);

				glLinkProgram(shaderId);

				_shaderMap[std::string(shaderName)] = shaderId;

				//glUseProgram(shaderId);
			}
			else
			{
				cout << "Fragment shader not compiled." << endl;
				printShaderInfoLog(f);
			}
		}
		else
		{
			cout << "Vertex shader not compiled." << endl;
			printShaderInfoLog(v);
		}



		delete[] vs; // dont forget to free allocated memory
		delete[] fs; // we allocated this in the loadFile function...

		return shaderId;
	}


	unsigned int RendererGL::GetShaderId(const char* shaderName)
	{
		unsigned int shaderId = 0;

		if (shaderName != nullptr)
		{
			auto it = this->_shaderMap.find(std::string(shaderName));
			if (it != this->_shaderMap.end())
			{
				shaderId = it->second;
			}
		}

		return shaderId;
	}



	void RendererGL::UseShader(unsigned int shaderId)
	{
		glUseProgram(shaderId);

		GLuint matrixID;
		matrixID = glGetUniformLocation(shaderId, "modelview");
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, this->modelTransform);

		matrixID = glGetUniformLocation(shaderId, "camera");
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, this->cameraTransform);

		matrixID = glGetUniformLocation(shaderId, "projection");
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, this->projectionTranform);
	}


	void RendererGL::UseShader(const char* shaderName)
	{
		this->UseShader(this->GetShaderId(shaderName));
	}


	void RendererGL::DestroyShader(unsigned int shaderId)
	{
		for (auto it = this->_shaderMap.begin(); it != this->_shaderMap.end(); ++it)
		{
			if (it->second == shaderId)
			{
				this->_shaderMap.erase(it);
				break;
			}
		}

		glDeleteProgram(shaderId);
	}



	int RendererGL::GetCurrentProgram(void)
	{
		GLint programId;

		glGetIntegerv(GL_CURRENT_PROGRAM, &programId);

		return programId;
	}


	void RendererGL::SetUniform_1f(unsigned int programId, const char* attrId, float value)
	{
		GLuint location = glGetUniformLocation(programId, attrId );
		glUniform1f(location, value);

	}


	void RendererGL::SetModelTransform(const float matrix[16])
	{
		for (size_t i = 0; i < 16; ++i){ this->modelTransform[i] = matrix[i]; }

		this->UseShader(this->GetCurrentProgram());
	}


	void RendererGL::SetCameraTransform(const float matrix[16])
	{
		for (size_t i = 0; i < 16; ++i){ this->cameraTransform[i] = matrix[i]; }

		this->UseShader(this->GetCurrentProgram());
	}


	void RendererGL::SetProjection(const float matrix[16])
	{
		for (size_t i = 0; i < 16; ++i){ this->projectionTranform[i] = matrix[i]; }

		this->UseShader(this->GetCurrentProgram());
	}


	void RendererGL::SetModelTransform(const double matrix[16])
	{
		for (size_t i = 0; i < 16; ++i)
		{ 
			this->modelTransform[i] = (float)matrix[i]; 
		}

		this->UseShader(this->GetCurrentProgram());
	}


	void RendererGL::SetCameraTransform(const double matrix[16])
	{
		for (size_t i = 0; i < 16; ++i){ this->cameraTransform[i] = (float)matrix[i]; }

		this->UseShader(this->GetCurrentProgram());
	}


	void RendererGL::SetProjection(const double matrix[16])
	{
		for (size_t i = 0; i < 16; ++i){ this->projectionTranform[i] = (float)matrix[i]; }

		this->UseShader(this->GetCurrentProgram());
	}


	// printShaderInfoLog
	// From OpenGL Shading Language 3rd Edition, p215-216
	// Display (hopefully) useful error messages if shader fails to compile
	void printShaderInfoLog(GLint shader)
	{
		int infoLogLen = 0;
		int charsWritten = 0;
		GLchar *infoLog;

		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLen);

		// should additionally check for OpenGL errors here

		if (infoLogLen > 0)
		{
			infoLog = new GLchar[infoLogLen];
			// error check for fail to allocate memory omitted
			glGetShaderInfoLog(shader, infoLogLen, &charsWritten, infoLog);
			cout << "InfoLog:" << endl << infoLog << endl;
			delete[] infoLog;
		}

		// should additionally check for OpenGL errors here
	}



	// loadFile - loads text file into char* fname
	// allocates memory - so need to delete after use
	// size of file returned in fSize
	char* loadFile(const char *fname, GLint &fSize)
	{
		ifstream::pos_type size;
		char * memblock;
		string text;

		// file read based on example in cplusplus.com tutorial
		ifstream file(fname, ios::in | ios::binary | ios::ate);
		if (file.is_open())
		{
			size = file.tellg();
			fSize = (GLuint)size;
			memblock = new char[size];
			file.seekg(0, ios::beg);
			file.read(memblock, size);
			file.close();
			cout << "file " << fname << " loaded" << endl;
			text.assign(memblock);
		}
		else
		{
			cout << "Unable to open file " << fname << endl;
			exit(1);
		}
		return memblock;
	}
}}