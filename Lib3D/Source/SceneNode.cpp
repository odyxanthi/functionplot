#include "stdafx.h"
#include "SceneNode.h"
#include "IRenderable.h"
#include "IRenderer3D.h"
#include <algorithm>


namespace op
{
	namespace lib3D
	{
		SceneNode::SceneNode()
			:
			worldObject(nullptr)
		{
			this->transform.SetToIdentity();
		}


		SceneNode::SceneNode(const std::string& nodeName)
			:
			name(nodeName),
			worldObject(nullptr)
		{
			this->transform.SetToIdentity();
		}

		
		SceneNode::~SceneNode()
		{

		}

		
		const std::string& SceneNode::GetName(void) const
		{
			return this->name;
		}


		const Transform3D& SceneNode::GetTransform(void) const
		{
			return (this->transform);
		}


		void SceneNode::SetTransform(const Transform3D& t)
		{
			this->transform = t;
		}

		
		void SceneNode::Transform(const Transform3D& transform_)
		{
			this->transform *= transform_;
		}

		

		IRenderable* SceneNode::GetObject(void) const
		{
			return this->worldObject.get();
		}



		void SceneNode::SetObject(unique_ptr<IRenderable> object)
		{
			if (object.get() != this->worldObject.get())
			{
				this->worldObject = std::move(object);
			}
		}



		void SceneNode::Render(IRenderer3D& renderer, const RenderArgs& args)
		{
			Transform3D t = this->transform.Transpose();
			
			renderer.SetModelTransform( &t[0] );

			if (this->worldObject)
			{
				this->worldObject->Render(renderer, args);
			}

			for (auto node : this->GetChildren())
			{
				node->Render(renderer, args);
			}

			//renderer.PopTransform();
		}
		


		void SceneNode::Select(bool select)
		{
			throw std::exception("Not implemented");
		}



		bool SceneNode::IsSelected(void) const
		{
			throw std::exception("Not implemented");
			return false;
		}

	}
}