#include "stdafx.h"
#include "PolylineModel3D.h"
#include "IRenderer3D.h"
#include "GL/glew.h"
#include "MathDefs.h"
#include "Vector.h"
#include "Ray3D.h"
#include <cmath>


using op::math::vector3f;


namespace op { namespace lib3D {


	PolylineModel3D::PolylineModel3D()
	{}


	PolylineModel3D::PolylineModel3D(std::initializer_list<const vector3d> v3d_range)
	{
		for (const vector3d& v : v3d_range)
		{
			_points3D.push_back(v);
		}

		this->UpdateRendering();
	}


	PolylineModel3D::~PolylineModel3D()
	{
		glDeleteBuffers(1, &_bufferObject);
		_bufferObject = 0;
	}


	void PolylineModel3D::Render(IRenderer3D& renderer, const RenderArgs&)
	{
		glLineWidth(2.0);

		if (_bufferObject != 0)
		{
			glBindBuffer(GL_ARRAY_BUFFER, _bufferObject);
			glVertexPointer(3, GL_DOUBLE, 0, 0);
			glEnableClientState(GL_VERTEX_ARRAY);
			glDrawArrays(GL_LINE_STRIP, 0, (GLsizei)_points3D.size());
		}
	}


	void PolylineModel3D::UpdateRendering(void)
	{
		if (_bufferObject != 0)
		{
			glDeleteBuffers(1, &_bufferObject);
			_bufferObject = 0;
		}

		if (_points3D.size() > 0)
		{
			glGenBuffers(1, &_bufferObject);
			glBindBuffer(GL_ARRAY_BUFFER, _bufferObject);
			glBufferData(GL_ARRAY_BUFFER, _points3D.size() * 3 * sizeof(double), &_points3D[0], GL_STATIC_DRAW);
		}
	}


	RayModel3D::RayModel3D(const Ray3D& r)
		: ray(r)
	{}



	void RayModel3D::Render(op::lib3D::IRenderer3D& r)
	{
		r.DrawLine(ray.StartPt, ray.StartPt + ray.Dir * 1000);
	}

}}
