#include "stdafx.h"
#include "FontFaceGL.h"
#include "ft2build.h"
#include FT_FREETYPE_H
#include <GL/glew.h>

using op::math::vector3d;

namespace op { namespace lib3D
{

	FontFaceGL::FontFaceGL(const FT_Face& fontFace)
	: _fontFace(fontFace)
	{
		for (size_t i = 0; i < _textures.size(); ++i)
		{
			_textures[i] = 0;
		}

		this->Initialize();
	}


	FontFaceGL::~FontFaceGL()
	{
		glDeleteTextures((GLsizei)_textures.size(), &(_textures[0]));
	}


	void FontFaceGL::Initialize()
	{
		FT_GlyphSlot g = _fontFace->glyph;
		glGenTextures((GLsizei)_textures.size(), &(_textures[0]));
		for (size_t i = 0; i < _textures.size(); ++i)
		{

			glBindTexture(GL_TEXTURE_2D, _textures[i]);

			/* We require 1 byte alignment when uploading texture data */
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

			/* Clamping to edges is important to prevent artifacts when scaling */
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			/* Linear filtering usually looks best for text */
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


			if (FT_Load_Char(_fontFace, (char)i, FT_LOAD_RENDER))
				continue;

			/* Upload the "bitmap", which contains an 8-bit grayscale image, as an alpha texture */
			glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, g->bitmap.width, g->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);
		}
	}


	struct point {
		GLfloat x;
		GLfloat y;
		GLfloat s;
		GLfloat t;
	};


	void FontFaceGL::DrawText(const std::string& text, const op::math::vector3d& origin, const op::math::vector3d& dirX, const op::math::vector3d& dirY, float textHeight)
	{
		glEnable(GL_BLEND);
		//glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

		glColor3f(1.0, 1.0, 1.0);
		

		FT_Load_Char(_fontFace, 'A', FT_LOAD_RENDER);	//	use 'A' as a reference to calculate height.
		FT_GlyphSlot g = _fontFace->glyph;
		float referenceGlyphHeight = (float)g->bitmap.rows;
		const float scalingFactor = textHeight / referenceGlyphHeight;

		float x = 0.0;
		float y = 0.0;
		for (const char* c = text.c_str(); *c; c++)
		{
			FT_Load_Char(_fontFace, *c, FT_LOAD_RENDER);

			/* Calculate the vertex and texture coordinates */
			float x2 = x + g->bitmap_left * scalingFactor;
			float y2 = -y - g->bitmap_top * scalingFactor;
			float w = g->bitmap.width * scalingFactor;
			float h = g->bitmap.rows * scalingFactor;

			point box[4] = {
				{ x2, -y2, 0, 0 },
				{ x2 + w, -y2, 1, 0 },
				{ x2 + w, -y2 - h, 1, 1 },
				{ x2, -y2 - h, 0, 1 }
			};

			/* Advance the cursor to the start of the next character */
			x += (g->advance.x >> 6) * scalingFactor;
			y += (g->advance.y >> 6) * scalingFactor;



			glBindTexture(GL_TEXTURE_2D, _textures[*c]);


			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glBegin(GL_QUADS);

				glTexCoord2f(box[0].s, box[0].t);
				vector3d p = origin + dirX*box[0].x + dirY*box[0].y;
				glVertex3d(p[0], p[1], p[2]);

				glTexCoord2f(box[1].s, box[1].t);
				p = origin + dirX*box[1].x + dirY*box[1].y;
				glVertex3d(p[0], p[1], p[2]);

				glTexCoord2f(box[2].s, box[2].t);
				p = origin + dirX*box[2].x + dirY*box[2].y;
				glVertex3d(p[0], p[1], p[2]);

				glTexCoord2f(box[3].s, box[3].t);
				p = origin + dirX*box[3].x + dirY*box[3].y;
				glVertex3d(p[0], p[1], p[2]);


			glEnd();
		}

		glDisable(GL_TEXTURE_2D);
	}
}}