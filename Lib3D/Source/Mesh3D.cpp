#include "stdafx.h"
#include "Mesh3D.h"


using namespace op::math;


namespace op { 	namespace lib3D
{
	Triangle::Triangle(size_t v1, size_t v2, size_t v3)
		:
		v1(v1),
		v2(v2),
		v3(v3)
	{}


	Vertex::Vertex(const vector3d& position)
		:
		position(position),
		normal( vector3d(0,0,0) ),
		texCoords( vector2d(0,0) ),
		degree(0)
	{}


	Vertex::Vertex(const vector3d& position, const vector3d& normal, const vector2d& texCoords)
		:
		position(position),
		normal(normal),
		texCoords(texCoords),
		degree(0)
	{}


	Mesh3D::Mesh3D(const std::vector<Vertex>& _vertices_, const std::vector<Triangle>& _triangles_)
		:
		_vertices(_vertices_),
		_triangles(_triangles_),
		_bbox()
	{
		this->UpdateBoundingBox();
		this->CalculateNormals();
	}


	Mesh3D::Mesh3D(std::vector<Vertex>&& _vertices_, std::vector<Triangle>&& _triangles_)
		:
		_vertices(_vertices_),
		_triangles(_triangles_),
		_bbox()
	{
		this->UpdateBoundingBox();
		this->CalculateNormals();
	}


	const std::vector<Triangle>& Mesh3D::GetTriangles(void) const
	{
		return this->_triangles;
	}


	const std::vector<Vertex>& Mesh3D::GetVertices(void) const
	{
		return this->_vertices;
	}


	Vertex& Mesh3D::GetVertex(unsigned int index)
	{
		return this->_vertices[index];
	}


	const Vertex& Mesh3D::GetVertex(unsigned int index) const
	{
		return this->_vertices[index];
	}


	void Mesh3D::Scale(double scaleFactor)
	{
		for (auto iter = this->_vertices.begin(); iter != this->_vertices.end(); iter++)
		{
			(*iter).position *= scaleFactor;
		}
		this->UpdateBoundingBox();
	}


	const BoundingBox& Mesh3D::GetBoundingBox(void) const
	{
		return _bbox;
	}


	void Mesh3D::CalculateNormals(void)
	{
		vector3d v(0, 0, 0);

		for (auto iter = this->_vertices.begin(); iter != this->_vertices.end(); iter++)
		{
			(*iter).normal = vector3d(0, 0, 0);
			(*iter).degree = 0;
		}


		for (auto iter = this->_triangles.begin(); iter != this->_triangles.end(); iter++ )
		{
			Vertex& v1 = this->_vertices[(*iter).v1];
			Vertex& v2 = this->_vertices[(*iter).v2];
			Vertex& v3 = this->_vertices[(*iter).v3];


			vector3d a = v1.position - v2.position;
			vector3d b = v3.position - v2.position;


			vector3d normal = a.CrossProduct(b);
			normal.Unitize();


			v1.normal += normal;
			v1.degree++;
				
			v2.normal += normal;
			v2.degree++;

			v3.normal += normal;
			v3.degree++;
		}


		for (auto iter = this->_vertices.begin(); iter != this->_vertices.end(); iter++)
		{
			(*iter).normal.Unitize();
		}
	}


	void Mesh3D::UpdateBoundingBox(void)
	{
		double minDouble = std::numeric_limits<double>::min();
		double maxDouble = std::numeric_limits<double>::max();
		vector3d min(maxDouble, maxDouble, maxDouble);
		vector3d max(minDouble, minDouble, minDouble);

		for (auto& v : _vertices)
		{
			auto& pos = v.position;
			if (pos.X() < min.X()) min.X() = pos.X();
			if (pos.Y() < min.Y()) min.Y() = pos.Y();
			if (pos.Z() < min.Z()) min.Z() = pos.Z();

			if (pos.X() > max.X()) max.X() = pos.X();
			if (pos.Y() > max.Y()) max.Y() = pos.Y();
			if (pos.Z() > max.Z()) max.Z() = pos.Z();
		}

		_bbox = BoundingBox(min, max);
	}
}}
