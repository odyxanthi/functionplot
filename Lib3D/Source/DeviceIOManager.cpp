#include "stdafx.h"
#include "DeviceIOManager.h"
#include "Mouse.h"
#include "Keyboard.h"

namespace op { namespace lib3D
{
	DeviceIOManager::DeviceIOManager()
	{

	}


	DeviceIOManager::~DeviceIOManager()
	{

	}


	void DeviceIOManager::TrackMouse(Mouse& mouse)
	{
		mouse.MouseClick.connect(this, &DeviceIOManager::OnMouseClick);
		mouse.MouseMove.connect(this, &DeviceIOManager::OnMouseMove);
	}


	void DeviceIOManager::TrackKeyboard(Keyboard& keyboard)
	{
		keyboard.KeyPressedEvt.connect(this, &DeviceIOManager::OnKeyPressed);
	}


	void DeviceIOManager::OnKeyPressed(op::lib3D::KeyboardEvtArgs& keyEvtArgs)
	{
		KeyPressed(keyEvtArgs);
	}


	void DeviceIOManager::OnMouseMove(op::lib3D::MouseEvtArgs& mouseEvtArgs)
	{
		MouseMoved(mouseEvtArgs);
	}


	void DeviceIOManager::OnMouseClick(op::lib3D::MouseEvtArgs& mouseEvtArgs)
	{
		MouseClicked(mouseEvtArgs);
	}

}}
