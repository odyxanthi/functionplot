#include "stdafx.h"
#include "Transform3D.h"


namespace op { namespace lib3D
{
	Transform3D::Transform3D()
	{}

		
	Transform3D::Transform3D(const Transform3D& other)
		:
		op::math::Matrix<double, 4, 4>(other)
	{}


	Transform3D::Transform3D(const op::math::Matrix<double, 4, 4>& matrix)
	:
		op::math::Matrix<double, 4, 4>(matrix)
	{}
}}
