#include "stdafx.h"

#include "MathUtils.h"
#include "MathDefs.h"
#include <math.h>


namespace op { namespace math
{
	Matrix<double, 4, 4> MathUtils::CreateRotationMatrix(double rotationAngle, const vector3d& rotationAxis)
	{
		double c = cos(rotationAngle);
		double s = sin(rotationAngle);
		double t = 1 - c;

		vector3d v(rotationAxis);
		v.Unitize();

		double x = v.X();
		double y = v.Y();
		double z = v.Z();

		std::array<double, 16> data = { t*x*x + c, t*x*y - z*s, t*x*z + y*s, 0,
										t*x*y +z*s, t*y*y + c, t*y*z - x*s, 0,
										t*x*z -y*s, t*y*z + x*s, t*z*z + c, 0,
										0,			0,			 0,			1	};

		Matrix<double, 4, 4> m(data);

		return m;
	}


	Matrix<double, 4, 4> MathUtils::CreateTranslationMatrix(const vector3d& translation)
	{
		std::array<double, 16> data =	{	1, 0, 0, translation.X(),
											0, 1, 0, translation.Y(),
											0, 0, 1, translation.Z(),
											0, 0, 0, 1
										};

		Matrix<double, 4, 4> m(data);

		return m;
	}


	double MathUtils::DegreesToRadians(double angleInDegrees)
	{
		return (angleInDegrees / 180.0) * op::mathDefs::PI;
	}


	double MathUtils::RadiansToDegrees(double angleInRadians)
	{
		return (angleInRadians / op::mathDefs::PI) * 180.0;
	}


	template<typename T, size_t TRows, size_t TCols>
	Vector<T, TRows> operator * (const Matrix<T, TRows, TCols>& m, const Vector<T, TCols>& v)
	{
		Vector<T, TRows> resultVec;

		for (size_t i = 0; i < TRows; i++)
		{
			resultVec[i] = 0.0;
			for (size_t j = 0; j < TCols; j++)
			{
				resultVec[i] += m(i, j) * v[j];
			}
		}

		return resultVec;
	}


	vector3d operator * (const Matrix<double, 4, 4>& transform, const vector3d& p)
	{
		std::array<double, 4> data = { p.X(), p.Y(), p.Z(), 1.0 };
		Vector<double, 4> v(data);

		Vector<double, 4> vTransformed = transform * v;

		return vector3d(vTransformed[0], vTransformed[1], vTransformed[2]);
	}


	bool MathUtils::WithinTolerance(double d1, double d2, double tolerance)
	{
		return (abs(d1 - d2) <= tolerance);
	}

}}
