#include "stdafx.h"
#include "Camera.h"
#include "MathUtils.h"


using op::math::MathUtils;


namespace op { namespace lib3D
{
	void Camera::SetViewParams(const vector3d& pos, const vector3d& center, const vector3d& up)
	{
		_position = pos;
		_center = center;
		_up = up;

		UpdateCachedData();
	}


	void Camera::SetPosition(const vector3d& pos)
	{ 
		_position = pos; 
		UpdateCachedData();
	}


	void Camera::SetCenter(const vector3d& center)	
	{ 
		_center = center; 
		UpdateCachedData();
	}


	void Camera::SetUpDir(const vector3d& up)		
	{ 
		_up = up;
		UpdateCachedData();
	}


	void Camera::UpdateCachedData()
	{
		_up.Unitize();
		_viewDir = (_center - _position).GetUnitVec();
		_right = _viewDir.CrossProduct(_up);
		_left = _up.CrossProduct(_viewDir);
	}


	void Camera::SetAspectRatio(double d)
	{
		this->aspectRatio = d;
	}


	void Camera::SetNearPlaneDistance(double d)
	{
		this->nearPlaneDistance = d;
	}


	void Camera::SetFarPlaneDistance(double d)
	{
		this->farPlaneDistance = d;
	}


	void Camera::MoveForwards(double d)
	{
		vector3d viewDir = this->ViewDir();

		_position += viewDir*d;
		_center += viewDir*d;
		UpdateCachedData();
	}


	void Camera::MoveBackwards(double d)
	{
		vector3d viewDir = this->ViewDir();

		_position -= viewDir*d;
		_center -= viewDir*d;
		UpdateCachedData();
	}


	void Camera::MoveLeft(double d)
	{
		_position += this->Left()*d;
		_center += this->Left()*d;
		UpdateCachedData();
	}


	void Camera::MoveRight(double d)
	{
		_position += this->Right()*d;
		_center += this->Right()*d;
		UpdateCachedData();
	}


	void Camera::MoveUp(double d)
	{
		_position += this->Up()*d;
		_center += this->Up()*d;
		UpdateCachedData();
	}


	void Camera::MoveDown(double d)
	{
		_position -= this->Up()*d;
		_center -= this->Up()*d;
		UpdateCachedData();
	}


	void Camera::ZoomIn(double distance)
	{
		if (distance > this->Position().DistanceTo(this->Center()))
		{
			distance = 0.99 * this->Position().DistanceTo(this->Center());
		}

		_position += this->ViewDir()*distance;
		UpdateCachedData();
	}


	void Camera::ZoomOut(double distance)
	{
		_position -= this->ViewDir()*distance;
		UpdateCachedData();
	}


	void Camera::RotateAroundCurrentPos(double angleInDegrees, const vector3d& rotationAxis)
	{

	}


	void Camera::RotateAroundCenter(double angleInDegrees, const vector3d& rotationAxis)
	{
		op::math::Matrix<double, 4, 4> m1 = MathUtils::CreateRotationMatrix( MathUtils::DegreesToRadians(angleInDegrees), rotationAxis );
		
		// Calculate the new camera position
		vector3d newCameraPos = m1 * this->Position();

		// Calculate the new Up vector.
		// Calculate how the UpVector is affected by calculating and transforming a point above the camera position.
		vector3d newUpPoint = m1 * (this->Position() + this->Up());	
		vector3d newUpVector = newUpPoint - newCameraPos;

		this->SetViewParams(newCameraPos, this->Center(), newUpVector);
	}

}}
