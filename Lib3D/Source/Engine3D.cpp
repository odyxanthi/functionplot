#include "stdafx.h"


#include "Engine3D.h"
#include "Logger.h"
#include "SceneManager.h"
#include "DeviceIOManager.h"
#include "RendererGL.h"
#include "SceneNode.h"


namespace op { namespace lib3D
{

	Engine3D* Engine3D::_engineSingleton = nullptr;


	Engine3D::Engine3D()
		:
		logger(new ::core::Logger),
		sceneManager(new SceneManager),
		deviceManager(new DeviceIOManager),
		renderer(new RendererGL)
	{

	}


	Engine3D::~Engine3D()
	{

	}


	Engine3D* Engine3D::CreateInstance(void)
	{
		if (_engineSingleton == nullptr)
		{
			_engineSingleton = new Engine3D;
		}

		return _engineSingleton;
	}


	Engine3D* Engine3D::GetInstance(void)
	{
		return Engine3D::CreateInstance();
	}


	void Engine3D::DestroyInstance(void)
	{
		delete _engineSingleton;
		_engineSingleton = nullptr;
	}


	::core::Logger* Engine3D::GetLogger(void)
	{
		return this->logger.get();
	}


	SceneManager* Engine3D::GetSceneManager(void)
	{
		return this->sceneManager.get();
	}


	DeviceIOManager* Engine3D::GetDeviceManager(void)
	{
		return this->deviceManager.get();
	}


	IRenderer3D* Engine3D::GetRenderer(void)
	{
		return this->renderer.get();
	}
}}
