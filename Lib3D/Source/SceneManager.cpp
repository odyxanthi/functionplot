#include "stdafx.h"
#include "SceneManager.h"
#include "SceneNode.h"


namespace op { namespace lib3D
{
	SceneManager::SceneManager()
		:
		rootNode( new SceneNode )
	{}


	SceneManager::~SceneManager()
	{}


	SceneNode* SceneManager::GetRootNode(void) const
	{
		return this->rootNode.get();
	}


	SceneNode* SceneManager::GetNode(const string& name) const
	{
		return nullptr;
	}


	SceneNode* SceneManager::CreateNewNode(SceneNode* parent)
	{
		if (parent == nullptr)
		{
			parent = this->rootNode.get();
		}

		auto node_uptr = std::make_unique<SceneNode>();
		SceneNode* p = node_uptr.get();
		parent->AddChild(std::move(node_uptr));

		// At this point, node_ptr has been added successfully (otherwise an exception would have been thrown by AddChild)
		// node_ptr is safe to access

		return p;
	}


	void SceneManager::DeleteScene(void)
	{
		this->rootNode.reset(new SceneNode);
	}


	void SceneManager::SelectSingleNode(SceneNode* node)
	{
		auto currentNode = this->GetRootNode();
		while (currentNode != nullptr)
		{
			currentNode->Select(false);
			currentNode = currentNode->GetNext();
		}

		if (node != nullptr)
		{
			node->Select(true);
		}
	}


	SceneNode* SceneManager::GetSelectedNode(void) const
	{
		return this->GetRootNode()->GetNodeInSubtree([](const SceneNode* n){ return n->IsSelected(); });
	}

}}