#include "stdafx.h"
#include "Material.h"

#include <utility>
#include <array>

namespace op { namespace lib3D
{
	Color::Color()
	{}


	Color::Color(float r, float g, float b)
		:
		op::math::vector4f({ r, g, b, 1.0f })
	{}

		
	Color::Color(float r, float g, float b, float a)
		:
		op::math::vector4f({ r, g, b, a })
	{}

		
	Color::Color(const op::math::vector3f& v)
		:
		op::math::vector4f({ v[0], v[1], v[2], 1.0f })
	{}


	Color::Color(const op::math::vector4f& v)
		:
		op::math::vector4f({ v[0], v[1], v[2], v[3] })
	{}


	Material::Material()
		:
		ambient(0.1f,0.1f,0.1f, 1.0f),
		diffuse(1.0f, 1.0f, 1.0f, 1.0f),
		specular(1.0f, 1.0f, 1.0f, 1.0f),
		emissive(0.0f, 0.0f, 0.0f, 1.0f)
	{}


	Material::Material(
		const Color& ambient,
		const Color& diffuse,
		const Color& specular,
		const Color& emissive)
		:
		ambient(ambient),
		diffuse(diffuse),
		specular(specular),
		emissive(emissive)
	{}


	Material::Material(const Material& other)
		:
		ambient(other.ambient),
		diffuse(other.diffuse),
		specular(other.specular),
		emissive(other.emissive),
		transparency(other.transparency),
		shininess(other.shininess)
	{}


	Material& Material::operator = (Material other)
	{
		//TODO : test copy and swap implementation
		//std::swap(*this, other);

		//return (*this);

		ambient=other.ambient;
		diffuse=other.diffuse;
		specular=other.specular;
		emissive=other.emissive;
		transparency=other.transparency;
		shininess = other.shininess;

		return *this;
	}


	bool Material::operator == (const Material& other)
	{
		return (this->ambient == other.ambient &&
				this->diffuse == other.diffuse &&
				this->specular == other.specular &&
				this->emissive == other.emissive &&
				this->transparency == other.transparency &&
				this->shininess == other.shininess);
	}

}}
