#include "stdafx.h"
#include "GeometryUtils.h"
#include "Mesh3D.h"
#include "Ray3D.h"

#include <vector>
#include <limits>

#include "Vector.h"


using namespace op::math;

namespace op { namespace lib3D
{
	bool GeometryUtils::RayMeshIntersection(const Ray3D& ray, const Mesh3D& mesh, vector3d& nearestIntersection)
	{
		bool intersectionFound = false;

		auto faces = mesh.GetTriangles();
		auto vertices = mesh.GetVertices();

		vector3d intersection;
		double nearestIntersectionDistane = std::numeric_limits<double>::max();

		for (auto& f : faces)
		{
			Vertex v1 = vertices[f.v1];
			Vertex v2 = vertices[f.v2];
			Vertex v3 = vertices[f.v3];

			if (RayTriangleIntersection(ray, v1.position, v2.position, v3.position, intersection))
			{
				double d = ray.StartPt.DistanceTo(intersection);
				if (d < nearestIntersectionDistane)
				{
					nearestIntersection = intersection;
					nearestIntersectionDistane = d;
					intersectionFound = true;
				}
			}
		}

		return intersectionFound;
	}


	bool GeometryUtils::RayTriangleIntersection(const Ray3D& ray, const vector3d& v0, const vector3d& v1, const vector3d& v2, vector3d& intersection)
	{
		//quickVect e1, e2, pvec, qvec, tvec;
		const double epsilon = 0.000001;

		double a, f, u, v;

		vector3d e1 = v1-v0;
		vector3d e2 = v2 - v0;

		vector3d h = ray.Dir.CrossProduct(e2);
		a = e1.DotProduct(h);

		if (a > -0.00001 && a < 0.00001)
			return(false);

		f = 1 / a;
		vector3d s = ray.StartPt - v0;
		u = f * (s.DotProduct(h));

		if (u < 0.0 || u > 1.0)
			return(false);

		vector3d q = s.CrossProduct(e1);
		v = f * ray.Dir.DotProduct(q);

		if (v < 0.0 || u + v > 1.0)
			return(false);

		// at this stage we can compute t to find out where
		// the intersection point is on the line
		double t = f * e2.DotProduct(q);

		if (t > epsilon) // ray intersection
		{
			intersection = ray.StartPt + ray.Dir*t;
			return(true);
		}

		else // this means that there is a line intersection
			// but not a ray intersection
			return (false);

	}


	bool GeometryUtils::RayBoxIntersection(const Ray3D &r, const vector3d& boxMin, const vector3d& boxMax)
	{
		double tmin, tmax, tymin, tymax, tzmin, tzmax;

		vector3d invdir(1.0 / r.Dir.X(), 1.0 / r.Dir.Y(), 1.0 / r.Dir.Z());
		
		int sign[3];
		sign[0] = (invdir.X() < 0);
		sign[1] = (invdir.Y() < 0);
		sign[2] = (invdir.Z() < 0);
		vector3d bounds[2];
		bounds[0] = boxMin;
		bounds[1] = boxMax;

		tmin = (bounds[sign[0]].X() - r.StartPt.X()) * invdir.X();
		tmax = (bounds[1 - sign[0]].X() - r.StartPt.X()) * invdir.X();
		tymin = (bounds[sign[1]].Y() - r.StartPt.Y()) * invdir.Y();
		tymax = (bounds[1 - sign[1]].Y() - r.StartPt.Y()) * invdir.Y();

		if ((tmin > tymax) || (tymin > tmax))
			return false;
		if (tymin > tmin)
			tmin = tymin;
		if (tymax < tmax)
			tmax = tymax;

		tzmin = (bounds[sign[2]].Z() - r.StartPt.Z()) * invdir.Z();
		tzmax = (bounds[1 - sign[2]].Z() - r.StartPt.Z()) * invdir.Z();

		if ((tmin > tzmax) || (tzmin > tmax))
			return false;
		if (tzmin > tmin)
			tmin = tzmin;
		if (tzmax < tmax)
			tmax = tzmax;

		return true;
	}


	std::vector<double> GeometryUtils::SampleDomain(double minx, double maxx, int samplePoints)
	{
		if (minx>=maxx)				throw std::exception("minx>=maxx");       
		if (samplePoints<2)			throw std::exception("samplePoints<2");

		std::vector<double> v(samplePoints);

		v[0] = minx;
		double step = (maxx - minx)/(samplePoints-1);
		for (size_t i=1;i<samplePoints-1;++i)
		{
			v[i] = minx + i*step;
		}
		v[samplePoints-1] = maxx;

		return v;
	}
}}
