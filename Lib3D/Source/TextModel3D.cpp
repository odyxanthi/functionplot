#include "stdafx.h"
#include "TextModel3D.h"
#include "IRenderer3D.h"
#include "RenderArgs.h"
#include "IView3D.h"
#include "Camera.h"
#include "FontManager.h"
#include "ft2build.h"
#include FT_FREETYPE_H
#include <GL/glew.h>



namespace op { namespace lib3D 
{

	TextModel3D::TextModel3D()
	: _fontFaceName("FreeSans.ttf")
	, _fontSize(1.0)
	, _text("")
	{
	}


	TextModel3D::~TextModel3D()
	{

	}


	void TextModel3D::SetFontFace(const std::string& fontName)
	{
		_fontFaceName = fontName;
	}


	void TextModel3D::SetFontSize(float fontSize)
	{
		_fontSize = fontSize;
	}


	float TextModel3D::GetFontSize() const
	{
		return _fontSize;
	}


	void TextModel3D::SetText(const std::string& text)
	{
		_text = text;
	}
	

	const std::string& TextModel3D::GetText() const
	{
		return _text;
	}


	BillboardText::BillboardText()
	{
	}


	BillboardText::~BillboardText()
	{
	}


	void BillboardText::SetPosition(const vector3d& pos)
	{
		_position3D = pos;
	}


	void BillboardText::Render(IRenderer3D& renderer, const RenderArgs& args)
	{
		const op::lib3D::IView3D* view = args.view;
		
		if (view != nullptr)
		{
			renderer.SetMaterial(1);
			renderer.DrawText(GetText(), _position3D, view->GetCamera().Right(), view->GetCamera().Up(), GetFontSize());
		}
	}
	

	Oriented3DText::Oriented3DText()
	{}


	Oriented3DText::~Oriented3DText()
	{}


	void Oriented3DText::SetTextPlane(const vector3d& origin, const vector3d& x, const vector3d& y)
	{
		_planeOrigin = origin;
		_planeX = x;
		_planeY = y;
	}


	void Oriented3DText::Render(IRenderer3D& renderer, const RenderArgs& args)
	{
		renderer.SetMaterial(1);
		renderer.DrawText(GetText(), _planeOrigin, _planeX, _planeY, GetFontSize());
	}

}}
