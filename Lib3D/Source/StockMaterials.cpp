#include "stdafx.h"
#include "StockMaterials.h"


namespace op { namespace lib3D
{
	const Material StockMaterials::Black				= Material(Color(0.2f, 0.0f, 0.0f), Color(0.0f, 0.0f, 0.0f), Color(0.1f, 0.1f, 0.1f), Color(0.0f, 0.0f, 0.0f));
	const Material StockMaterials::Blue					= Material(Color(0.2f, 0.2f, 0.2f), Color(0.0f, 0.0f, 1.0f), Color(0.1f, 0.1f, 0.1f), Color(0.0f, 0.0f, 0.0f));
	const Material StockMaterials::Green				= Material(Color(0.2f, 0.2f, 0.2f), Color(0.0f, 1.0f, 0.0f), Color(0.1f, 0.1f, 0.1f), Color(0.0f, 0.0f, 0.0f));
	const Material StockMaterials::Grey					= Material(Color(0.2f, 0.2f, 0.2f), Color(0.7f, 0.7f, 0.7f), Color(0.1f, 0.1f, 0.1f), Color(0.0f, 0.0f, 0.0f));
	const Material StockMaterials::Red					= Material(Color(0.2f, 0.2f, 0.2f), Color(1.0f, 0.0f, 0.0f), Color(0.1f, 0.1f, 0.1f), Color(0.0f, 0.0f, 0.0f));
	const Material StockMaterials::White				= Material(Color(0.2f, 0.2f, 0.2f), Color(1.0f, 1.0f, 1.0f), Color(0.1f, 0.1f, 0.1f), Color(0.0f, 0.0f, 0.0f));
	const Material StockMaterials::Yellow				= Material(Color(0.2f, 0.2f, 0.2f), Color(1.0f, 1.0f, 0.0f), Color(0.1f, 0.1f, 0.1f), Color(0.0f, 0.0f, 0.0f));


	const Material StockMaterials::TransparentRed		= Material(Color(0.8f, 0.8f, 0.8f, 0.7f), Color(1.0f, 0.0f, 0.0f, 0.7f), Color(0.1f, 0.1f, 0.1f, 0.7f), Color(0.0f, 0.0f, 0.0f, 0.7f));

	const Material StockMaterials::MatBlue				= Material(Color(0.5f, 0.5f, 0.5f), Color(0.0f, 0.0f, 1.0f), Color(0.1f, 0.1f, 0.1f), Color(0.0f, 0.0f, 0.0f));

	const Material StockMaterials::TransparentYellow	= Material(Color(0.2f, 0.2f, 0.2f, 1.0f), Color(1.0f, 1.0f, 0.0f, 0.7f), Color(0.0f, 0.0f, 0.0f, 0.0f), Color(1.0f, 1.0f, 0.0f, 0.7f));

	const Material StockMaterials::TransparentPurple	= Material(Color(0.2f, 0.2f, 0.2f, 1.0f), Color(0.6f, 0.45f, 0.72f, 0.7f), Color(0.0f, 0.0f, 0.0f, 0.0f), Color(0.6f, 0.45f, 0.72f, 0.7f));

	StockMaterials::StockMaterials()
	{}

}}
