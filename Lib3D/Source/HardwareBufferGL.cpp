#include "stdafx.h"
#include "GL/glew.h"
#include "HardwareBufferGL.h"
#include "Mesh3D.h"
#include <numeric>

using namespace std;
using namespace op::lib3D;


namespace op { namespace lib3D
{

	unsigned int HardwareBufferGL::bufferCounter = 0;


	HardwareBufferGL::HardwareBufferGL(const Mesh3D& mesh, const vector<unsigned int>& faces)
		:
		bufferId(++bufferCounter)
	{
		this->CreateBuffer(mesh, faces);
	}


	HardwareBufferGL::HardwareBufferGL(const Mesh3D& mesh)
		:
		bufferId(++bufferCounter)
	{
		this->CreateBuffer(mesh, std::vector<unsigned int>());
	}


	void HardwareBufferGL::CreateBuffer(const Mesh3D& mesh, const vector<unsigned int>& faces)
	{
		const std::vector< op::lib3D::Vertex >& vertices = mesh.GetVertices();
		const std::vector<op::lib3D::Triangle>& triangles = mesh.GetTriangles();


		std::vector<GLfloat> vertexData(vertices.size() * 3);
		std::vector<GLfloat> normalData(vertices.size() * 3);
		for (size_t i = 0; i < vertices.size(); i++)
		{
			const op::lib3D::Vertex& vertex = vertices[i];
			vertexData[3 * i] = (GLfloat)vertex.position[0];
			vertexData[3 * i + 1] = (GLfloat)vertex.position[1];
			vertexData[3 * i + 2] = (GLfloat)vertex.position[2];

			normalData[3 * i] = (GLfloat)vertex.normal[0];
			normalData[3 * i + 1] = (GLfloat)vertex.normal[1];
			normalData[3 * i + 2] = (GLfloat)vertex.normal[2];
		}


#if 1
		std::vector<unsigned int> triangleIndices;
		//for (int i = 0; i < 5000; i++){ triangleIndices.push_back(i); }
		if (faces.size() != 0)
		{
			triangleIndices = faces;
		}
		else
		{
			for (size_t i = 0; i < triangles.size(); i++)
			{
				triangleIndices.push_back((unsigned int)i);
			}
		}


		this->triangleCount = triangleIndices.size();
		std::vector<GLuint> indexData(this->triangleCount * 3);
		for (size_t index = 0; index < triangleIndices.size(); index++)
		{
			int i = triangleIndices[index];
			const op::lib3D::Triangle& t = triangles[i];
			indexData[3 * index] = (GLuint)t.v1;
			indexData[3 * index + 1] = (GLuint)t.v2;
			indexData[3 * index + 2] = (GLuint)t.v3;
		}
#endif

#if 0
		std::vector<GLuint> indexData(triangles.size() * 3);
		for (size_t i = 0; i < triangles.size(); i++)
		{
			const op::lib3D::Triangle& t = triangles[i];
			indexData[3 * i] = t.v1;
			indexData[3 * i + 1] = t.v2;
			indexData[3 * i + 2] = t.v3;
		}

		this->triangleCount = triangles.size();
#endif



		glGenBuffers(3, bufferObjects);

		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vertexData.size(), &(vertexData[0]), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*normalData.size(), &(normalData[0]), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[2]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*indexData.size(), &(indexData[0]), GL_STATIC_DRAW);
	}


	HardwareBufferGL::~HardwareBufferGL()
	{
		glDeleteBuffers(3, this->bufferObjects);

		for (int i = 0; i < 3; ++i){ this->bufferObjects[i] = 0; }
	}


	void HardwareBufferGL::Draw(void)
	{
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[0]);
		glVertexPointer(3, GL_FLOAT, 0, 0);
		glEnableClientState(GL_VERTEX_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[1]);
		glNormalPointer(GL_FLOAT, 0, 0);
		glEnableClientState(GL_NORMAL_ARRAY);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[2]);
		glDrawElements(GL_TRIANGLES, (GLsizei)(this->triangleCount * 3), GL_UNSIGNED_INT, 0);
	}


}}