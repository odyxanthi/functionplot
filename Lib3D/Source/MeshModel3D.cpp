#include "stdafx.h"
#include "Engine3D.h"
#include "MeshModel3D.h"
#include "Mesh3D.h"
#include "HardwareBufferGL.h"
#include "IRenderer3D.h"
#include "StockMaterials.h"
#include <numeric>


#include <set>
#include <vector>
#include <map>


using namespace std;
using op::lib3D::Vertex;
using op::lib3D::Triangle;


namespace op { namespace lib3D
{

	MeshModel3D::MeshModel3D()
		:
		_shaderCreated(false)
	{

	}


	MeshModel3D::MeshModel3D(std::unique_ptr<Mesh3D> mesh)
	:
		_shaderCreated(false)
	{
		this->SetMesh(std::move(mesh));
	}


	MeshModel3D::~MeshModel3D()
	{

	}


	void MeshModel3D::SetMesh(std::unique_ptr<Mesh3D> mesh)
	{
		_meshGeometry = std::move(mesh);

		auto renderer = op::lib3D::Engine3D::GetInstance()->GetRenderer();
		_bufferId = renderer->CreateHardwareBuffer(*_meshGeometry);
	}


	Mesh3D* MeshModel3D::GetMesh(void)
	{
		return _meshGeometry.get();
	}


	void MeshModel3D::CreateShader(IRenderer3D& renderer)
	{
		_shaderId = renderer.GetShaderId("BasicLighting");

		_shaderCreated = true;
	}


	void MeshModel3D::Render(IRenderer3D& renderer, const RenderArgs&)
	{
		if (_meshGeometry)
		{
			//if (!_shaderCreated)
			//{
			//	_CreateShader(renderer);
			//}
			//renderer.UseShader(_shaderId);

			renderer.DrawHardwareBuffer(_bufferId);
		}
	}


}}