#include "stdafx.h"

#include <algorithm>

#include "CameraMan.h"
#include "Camera.h"
#include "MouseEvtArgs.h"
#include "KeyboardEvtArgs.h"
#include "MathUtils.h"


namespace op { namespace lib3D
{
	CameraMan::CameraMan()
		:
		rotationSpeed(0.15),
		translationSpeed(0.5)
	{}


	void CameraMan::SetCamera(Camera* camera)
	{
		this->_attachedCamera = camera;
	}


	Camera* CameraMan::GetCamera(void)
	{
		return (this->_attachedCamera);
	}


	void CameraMan::OnMouseMove(const op::lib3D::MouseEvtArgs& args)
	{
		if (this->GetCamera() != nullptr)
		{
			vector2i curPos = args.GetMousePosition();
			vector2i change = curPos - this->_prevMousePos;

			int maxPositionChange = 20;		// ignore changes that are verticall or horizontally larger than this value.

			if (std::abs(change.X()) <= maxPositionChange &&
				std::abs(change.Y()) <= maxPositionChange)
			{
				if (args.RightButtonDown() && args.ControlDown() )
				{
					this->GetCamera()->ZoomIn(change.Y());
				}
				else if (args.RightButtonDown() && args.ShiftDown() )
				{
					this->GetCamera()->MoveRight(-this->translationSpeed*change.X());
					this->GetCamera()->MoveUp(this->translationSpeed*change.Y());
				}
				else
				if (args.RightButtonDown())
				{
					this->GetCamera()->RotateAroundCenter(-this->rotationSpeed*change.X(), this->GetCamera()->Up());
					this->GetCamera()->RotateAroundCenter(-this->rotationSpeed*change.Y(), this->GetCamera()->Right());
				}
			}


			this->_prevMousePos = curPos;
		}
	}


	void CameraMan::OnKeyPressed(const op::lib3D::KeyboardEvtArgs& args)
	{
		switch (args.GetKeyCode())
		{
		case 'a': case 'A':
			this->GetCamera()->ZoomIn(10);
			break;
		case 'z': case 'Z':
			this->GetCamera()->ZoomOut(10);
			break;
		default:
			break;
		}
	}

}}