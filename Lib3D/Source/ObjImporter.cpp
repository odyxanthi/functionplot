#include "stdafx.h"
#include <sstream>
#include <list>
#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <exception>
#include <stdexcept>

#include "ObjImporter.h"
#include "Vector.h"

//#include <boost/algorithm/string/trim.hpp>

using namespace std;
using op::math::vector3d;

using FileFormatError = std::runtime_error;

namespace op { namespace lib3D
{
	ObjImporter::ObjImporter()
		:
		vertices(0),
		normals(0),
		textureCoords(0),
		facets(0)
	{}


	unique_ptr<op::lib3D::Mesh3D> ObjImporter::ReadMesh(const std::string& filename)
	{
		fstream f;
		f.open(filename.c_str(), ios_base::in);

		unique_ptr<op::lib3D::Mesh3D> meshPtr = this->ReadMesh(f);

		return meshPtr;
	}


	unique_ptr<op::lib3D::Mesh3D> ObjImporter::ReadMesh(std::istream& stream)
	{
		std::vector<op::lib3D::Vertex> vertices;
		std::vector<op::lib3D::Triangle> triangles;
		std::unique_ptr<op::lib3D::Mesh3D> mesh;

		const int bufferSize = 512;
		char buffer[512];
		std::string s;

		unsigned int lineNumber = 0;

		vector3d v;
		vector2d vt;

		while (true)
		{
			lineNumber++;

			stream >> s;
			if (stream.good())
			{
				if (s[0] == '#')
				{
					stream.getline(buffer, bufferSize);
					continue;
				}
				else if (s[0] == 'v' && s[1] == 't')
				{
					this->ReadTextureCoord(stream, vt);
					this->textureCoords.push_back(vt);
				}
				else if (s[0] == 'v' && s[1] == 'n')
				{
					this->ReadNormal(stream, v);
					this->normals.push_back(v);
				}
				else if (s[0] == 'v')
				{
					this->ReadVertex(stream, v);
					vertices.push_back(op::lib3D::Vertex(v));
				}
				else if (s[0] == 'f')
				{
					// obj allows the same vertex position to be used in compination with different texture coords/normals on different faces.
					// e.g. f 3/2/1 4/5/6 6/6/9 		-> vertex3 with texture 2 and normal 1.
					//      f 3/5/7 �					-> vertex3 with texture 5 and normal 7.
					// For the moment, this issue will be ignored. I.e. assuming the facets of the above example are read, then
					// First facet will result in assigning texture 2 and normal 1 to vertex 3.
					// Second facet will overwrite these values, assigning texture 5 and normal 7.

					FacetInfo facet;
					this->ReadFacet(stream, facet);
						
						
					for (VertexInfo& vertexInfo : facet.vertices)
					{
						int vertexIndex = vertexInfo.positionIndex - 1;
						int normalIndex = vertexInfo.normalIndex - 1;
						int texCoordsIndex = vertexInfo.textureIndex - 1;

						if (texCoordsIndex >= 0)
						{
							vertices[vertexIndex].texCoords = this->textureCoords[texCoordsIndex];
						}

						if (normalIndex >= 0)
						{
							vertices[vertexIndex].normal = this->normals[normalIndex];
						}
					}

					if (facet.vertices.size() == 4)
					{
						throw std::runtime_error("OBJ file contains quads. Currently only pure triangular meshes are supported.");
					}
					if (facet.vertices.size() == 3)
					{
						auto iter = facet.vertices.begin();
						op::lib3D::Triangle t(iter->positionIndex-1, (iter++)->positionIndex-1, (iter++)->positionIndex-1 );
						triangles.push_back(t);
					}

					cout << facet;
				}

			}
			else if (stream.bad())
			{
				// TODO - report the error to local info log.
				mesh.reset(nullptr);	// clean up and return
				break;
			}
			else if (stream.eof())
			{
				break;
			}
		}

		mesh.reset(new op::lib3D::Mesh3D(vertices, triangles));
		return mesh;
	}


	void ObjImporter::ReadVertex(std::istream& s, vector3d& v)
	{
		v = vector3d(0, 0, 0);
		s >> v[0] >> v[1] >> v[2];
	}


	void ObjImporter::ReadTextureCoord(std::istream& s, vector2d& vt)
	{
		vt = vector2d(0, 0);
		s >> vt[0] >> vt[1];
	}


	void ObjImporter::ReadNormal(std::istream& s, vector3d& v)
	{
		v = vector3d(0, 0, 0);
		s >> v[0] >> v[1] >> v[2];
	}


	void ObjImporter::ReadFacet(std::istream& s, FacetInfo& facetInfo)
	{
		facetInfo.vertices.clear();

		std::string lineStr;
		std::getline(s, lineStr);
		boost::algorithm::trim(lineStr);

		list<string> substrings;
		if (!lineStr.empty())
		{
			SplitString(lineStr, substrings);
		}


		VertexInfo vertexInfo;
		for (auto& str : substrings)
		{
			vertexInfo.positionIndex = -1;
			vertexInfo.textureIndex = -1;
			vertexInfo.normalIndex = -1;

			list<string> vertexInfoStrList;
			SplitString(str, '/', vertexInfoStrList);


			if (vertexInfoStrList.size() == 0)
			{
				throw FileFormatError("Empty facet definition " + str);
			}

			auto iter = vertexInfoStrList.begin();
			vertexInfo.positionIndex = std::atoi(iter->c_str());

			if (vertexInfoStrList.size() >= 2)
			{
				++iter;
				if ((*iter).empty() == false)       // second parameter may be empty (position and normal are defined but texture coordinate is omitted).
				{
					vertexInfo.textureIndex = std::atoi(iter->c_str());
				}
			}

			if (vertexInfoStrList.size() >= 3)
			{
				++iter;
				vertexInfo.normalIndex = std::atoi(iter->c_str());
			}

			if (vertexInfoStrList.size() >= 4)
			{
				throw FileFormatError("Vertex info contains more than 3 parameters");
				//throw FileFormatError();
			}

			facetInfo.vertices.push_back(vertexInfo);
		}

		return;
	}


	unsigned int SplitString(const string& s, char delim, list<string>& substrings)
	{
		substrings.clear();

		stringstream sstr(s);

		std::string subStr;
		while (std::getline(sstr, subStr, delim))
		{
			substrings.push_back(subStr);
		}

		return (unsigned int)substrings.size();
	}


	unsigned int SplitString(const string& s, list<string>& substrings)
	{
		substrings.clear();

		stringstream sstr(s);

		while (sstr.good())
		{
			std::string subStr;
			sstr >> subStr;
			substrings.push_back(subStr);
		}

		return (unsigned int)substrings.size();
	}

}}
