#include "stdafx.h"
#include "FontManager.h"

#include "ft2build.h"
#include FT_FREETYPE_H

using namespace std;

namespace op { namespace lib3D 
{

	FontManager::FontManager()
	: _ftLib(nullptr)
	{
		if (FT_Init_FreeType(&_ftLib)) 
		{
			throw new std::runtime_error("Could not initialize freetype library");
		}

		
		const char *defaultFontFilename = "FreeSans.ttf";
		if (FT_New_Face(_ftLib, defaultFontFilename, 0, &_defaultFont))
		{
			throw new std::runtime_error("Could not load default font");
		}
		FT_Set_Pixel_Sizes(_defaultFont, 0, 24);
	}


	FontManager::~FontManager()
	{
		for (auto iter = _fontFaces.begin(); iter != _fontFaces.end(); ++iter)
		{
			if (iter->second != _defaultFont)
			{
				FT_Done_Face(iter->second);
			}
		}
		FT_Done_Face(_defaultFont);

		if (_ftLib != nullptr)
		{
			FT_Done_FreeType(_ftLib);
		}
	}


	FT_Face FontManager::GetDefaultFontFace() const
	{
		return _defaultFont;
	}


	FT_Face FontManager::GetFontFace(const std::string& fontFaceName)
	{
		auto iter = _fontFaces.find(fontFaceName);
		if (iter != _fontFaces.end())
		{
			return iter->second;
		}
		else
		{
			FT_Face face = nullptr;
			if (FT_New_Face(_ftLib, fontFaceName.c_str(), 0, &face))
			{
				face = _defaultFont;
			}
			
			_fontFaces[fontFaceName] = face;
			return face;
		}
	}

}}
