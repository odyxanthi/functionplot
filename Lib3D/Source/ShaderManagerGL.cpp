#include "stdafx.h"
//#include "GL/glew.h"
//#include "ShaderManagerGL.h"
//#include <fstream>
//
//using namespace std;
//
//
//
//
//
//
//namespace op { namespace lib3D
//{
//	void printShaderInfoLog(GLint shader);
//	char* loadFile(const char *fname, GLint &fSize);
//
//
//	ShaderManagerGL* ShaderManagerGL::singleton = nullptr;
//
//	ShaderManagerGL* ShaderManagerGL::GetSingleton(void)
//	{
//		if (singleton == nullptr)
//		{
//			singleton = new ShaderManagerGL;
//		}
//
//		return singleton;
//	}
//
//
//	unsigned int ShaderManagerGL::CreateShader(const char* vertexShaderFile, const char* fragShaderFile, const char* shaderName)
//	{
//		const unsigned char* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
//
//		GLuint f, v;
//		GLuint shaderId = 0;
//
//		char *vs, *fs;
//
//		v = glCreateShader(GL_VERTEX_SHADER);
//		f = glCreateShader(GL_FRAGMENT_SHADER);
//
//		// load shaders & get length of each
//		GLint vlen;
//		GLint flen;
//		vs = loadFile(vertexShaderFile, vlen);
//		fs = loadFile(fragShaderFile, flen);
//
//		const char * vv = vs;
//		const char * ff = fs;
//
//		glShaderSource(v, 1, &vv, &vlen);
//		glShaderSource(f, 1, &ff, &flen);
//
//		GLint compiled;
//
//		glCompileShader(v);
//		glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
//		if (compiled)
//		{
//			glCompileShader(f);
//			glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
//			if (compiled)
//			{
//				shaderId = glCreateProgram();
//
//				glBindAttribLocation(shaderId, 0, "in_Position");
//				glBindAttribLocation(shaderId, 1, "in_Color");
//
//				glAttachShader(shaderId, v);
//				glAttachShader(shaderId, f);
//
//				glLinkProgram(shaderId);
//
//				this->shaderIndex[std::string(shaderName)] = shaderId;
//
//				//glUseProgram(shaderId);
//			}
//			else
//			{
//				cout << "Fragment shader not compiled." << endl;
//				printShaderInfoLog(f);
//			}
//		}
//		else
//		{
//			cout << "Vertex shader not compiled." << endl;
//			printShaderInfoLog(v);
//		}
//
//
//
//		delete[] vs; // dont forget to free allocated memory
//		delete[] fs; // we allocated this in the loadFile function...
//
//		return shaderId;
//	}
//
//
//
//	unsigned int ShaderManagerGL::GetShaderId(const char* shaderName)
//	{
//		unsigned int shaderId = 0;
//
//		if (shaderName != nullptr)
//		{
//			auto it = this->shaderIndex.find(std::string(shaderName));
//			if (it != this->shaderIndex.end())
//			{
//				shaderId = it->second;
//			}
//		}
//
//		return shaderId;
//	}
//
//
//
//	void ShaderManagerGL::DestroyShader(unsigned int shaderId)
//	{
//		for (auto it = this->shaderIndex.begin(); it != this->shaderIndex.end(); ++it)
//		{
//			if (it->second == shaderId)
//			{
//				this->shaderIndex.erase(it);
//				break;
//			}
//		}
//
//		glDeleteProgram(shaderId);
//	}
//
//
//	
//	void ShaderManagerGL::DestroyAllShaders(void)
//	{
//		for (auto it = this->shaderIndex.begin(); it != this->shaderIndex.end(); ++it)
//		{
//			this->DestroyShader(it->second);
//		}
//	}
//}}