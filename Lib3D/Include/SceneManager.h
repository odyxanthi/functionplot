#ifndef __SCENEMANAGER_H__
#define __SCENEMANAGER_H__

#include <memory>
#include <string>

using namespace std;

namespace op { namespace lib3D
{

	class SceneNode;
	class NodeIterator;


	//class SceneManager
	//{
	//public:

	//	virtual ~SceneManager(){};

	//public:

	//	virtual SceneNode* GetRootNode(void) = 0;

	//	virtual SceneNode* GetNode(const string& name) = 0;

	//	virtual SceneNode* CreateNewNode(SceneNode* parent = nullptr) = 0;

	//	virtual void DeleteScene(void) = 0;

	//public:

	//	NodeIterator GetNodeIterator();



	//};



	class SceneManager
	{
	public:

		SceneManager();

		virtual ~SceneManager();

	public:

		virtual SceneNode* GetRootNode(void) const;

		virtual SceneNode* GetNode(const string& name) const;

		virtual SceneNode* CreateNewNode(SceneNode* parent = nullptr);

		virtual void DeleteScene(void);

		virtual void SelectSingleNode(SceneNode* node);

		virtual SceneNode* GetSelectedNode(void) const;

	private:

		unique_ptr<SceneNode> rootNode;
	};

}}


#endif