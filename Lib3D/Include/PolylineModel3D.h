#ifndef __POLYLINEMODEL3D_H_
#define __POLYLINEMODEL3D_H_

#include "GL/glew.h"
#include "IRenderable.h"
#include "Ray3D.h"
#include <memory>
#include <initializer_list>


namespace op { namespace lib3D 
{

	class IRenderer3D;

	class PolylineModel3D : public IRenderable
	{
	public:

		PolylineModel3D();

		template <class vector3d_Range>
		explicit PolylineModel3D(const vector3d_Range& v3d_range);

		explicit PolylineModel3D(std::initializer_list<const vector3d> v3d_range);

		virtual ~PolylineModel3D();

	public:

		template <class IterType>
		void SetPoints(IterType rangeBegin, IterType rangeEnd);

		virtual void Render(IRenderer3D&, const RenderArgs& ) override;

	private:

		void UpdateRendering(void);

	private:

		GLuint _bufferObject = 0;
		size_t _pointCount   = 0;

		std::vector<vector3d> _points3D;
	};



	struct RayModel3D : IRenderable
	{
		RayModel3D(const Ray3D& r);

		virtual void Render(op::lib3D::IRenderer3D& r);

		const op::lib3D::Ray3D ray;
	};
}}


#include "../Source/PolylineModel3D.inl"

#endif