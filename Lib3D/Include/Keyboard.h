#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#include "KeyboardEvtArgs.h"
#include "Core/Include/sigslot.h"

using namespace sigslot;

namespace op { namespace lib3D
{
	class Keyboard
	{
	public:
		Keyboard();

		virtual ~Keyboard();

	public:

		signal1<op::lib3D::KeyboardEvtArgs&, single_threaded> KeyPressedEvt;

	};
	
}}


#endif