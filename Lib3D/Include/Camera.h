#ifndef __CAMERA_H__
#define __CAMERA_H__


#include "stdafx.h"
#include "Vector.h"

using op::math::vector3d;


namespace op { namespace lib3D
{
	class Camera
	{
	public:

		Camera() = default;

	public:

		void SetViewParams(const vector3d& pos, const vector3d& center, const vector3d& up);

		void SetPosition(const vector3d& pos);

		void SetCenter(const vector3d& center);

		void SetUpDir(const vector3d& up);

		void SetAspectRatio(double d);

		void SetNearPlaneDistance(double d);

		void SetFarPlaneDistance(double d);


		vector3d Position(void) const { return _position; };

		vector3d Center(void) const { return _center; };

		vector3d Up(void) const { return _up; };

		vector3d ViewDir(void) const { return _viewDir; };

		vector3d Right(void) const{ return _right; }

		vector3d Left(void) const{ return _left; }


		double AspectRatio(void) const { return aspectRatio; };

		double NearPlaneDistance(void) const { return nearPlaneDistance; };

		double FarPlaneDistance(void) const { return farPlaneDistance; };


		void MoveForwards(double d);

		void MoveBackwards(double d);

		void MoveLeft(double d);

		void MoveRight(double d);

		void MoveUp(double d);

		void MoveDown(double d);


		//! Zoom in by moving camera position by the specified distance
		void ZoomIn(double distance);

		//! Zoom out by moving camera position by the specified distance
		void ZoomOut(double distance);

		void RotateAroundCurrentPos(double angleInDegrees, const vector3d& rotationAxis);

		void RotateAroundCenter(double angleInDegrees, const vector3d& rotationAxis);

	private:

		void UpdateCachedData();

	private:

		vector3d _position;
		vector3d _center;
		vector3d _up;
		vector3d _left;
		vector3d _right;
		vector3d _viewDir;

		double aspectRatio;
		double nearPlaneDistance;
		double farPlaneDistance;
	};
}}

#endif