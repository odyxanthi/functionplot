//#ifndef __SceneNode_H__
//#define __SceneNode_H__
//
//#include <string>
//#include <functional>
//#include <memory>
//#include "IRenderable.h"
//
//using std::unique_ptr;
//
//namespace op
//{
//
//	namespace lib3D
//	{
//		class IRenderer3D;
//		class Transform3D;
//		class IWorldElement;
//
//
//		class SceneNode : public IRenderable
//		{
//		public:
//
//			virtual void SetObject(IWorldElement* object) = 0;
//
//			
//			virtual IWorldElement* GetObject(void) const = 0;
//
//
//			virtual const std::string& GetName(void) const = 0;
//
//
//			virtual void AddChild(unique_ptr<SceneNode> sceneNode) = 0;
//
//
//			virtual void RemoveChild(SceneNode* sceneNode) = 0;
//
//			
//			virtual SceneNode* GetChild(const unsigned int index) const = 0;
//
//
//			virtual SceneNode* GetChild(const std::string& name) const = 0;
//
//
//			virtual SceneNode* GetChild(std::function<bool(const SceneNode*)> predicate) const = 0;
//
//
//			virtual SceneNode* FindNodeInSubtree(std::function<bool(const SceneNode*)> predicate) const = 0;
//
//
//			virtual const Transform3D& GetTransform(void) const = 0;
//
//
//			virtual void SetTransform(const Transform3D&) = 0;
//
//
//			virtual void Transform(const Transform3D& transform) = 0;
//			
//		};
//	}
//}
//
//
//#endif