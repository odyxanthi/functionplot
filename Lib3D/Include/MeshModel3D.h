#ifndef __MESHMODEL3D_H__
#define __MESHMODEL3D_H__

#include "IRenderable.h"
#include <memory>
#include <vector>


#include "GL/glew.h"
#include "Material.h"


namespace op { namespace lib3D
{
	class Mesh3D;


	class MeshModel3D : public IRenderable
	{

	public:
			
		MeshModel3D();

		MeshModel3D(std::unique_ptr<Mesh3D> mesh);

		virtual ~MeshModel3D();

	public:

		void SetMesh(std::unique_ptr<Mesh3D> mesh);

		Mesh3D* GetMesh(void);

		void Render(IRenderer3D&, const RenderArgs&) override;

	private:

		void CreateShader(IRenderer3D& renderer);

	private:

		//! The geometry to render.
		std::unique_ptr<op::lib3D::Mesh3D> _meshGeometry;
		unsigned int _bufferId;

		bool _shaderCreated;
		unsigned int _shaderId;
	};

}}



#endif