#ifndef __HARDWAREBUFFERGL_H__
#define __HARDWAREBUFFERGL_H__

#include <memory>
#include <map>
#include <vector>
#include "GL/glew.h"


using namespace std;


namespace op { namespace lib3D
{
	class Mesh3D;

	class HardwareBufferGL
	{
	public:

		HardwareBufferGL(const Mesh3D& mesh);

		HardwareBufferGL(const Mesh3D& mesh, const vector<unsigned int>& faces);

		~HardwareBufferGL(void);

	public:

		void Draw(void);

		unsigned int GetBufferId(void) const { return bufferId; };

	private:

		void CreateBuffer(const Mesh3D& mesh, const vector<unsigned int>& faces);

	private:

		GLuint bufferObjects[3];
		size_t triangleCount;
		unsigned int bufferId;

		static unsigned int bufferCounter;
	};

}}


#endif