#ifndef __TRANSFORM3D_H__
#define __TRANSFORM3D_H__

#include "Matrix.h"
#include "Vector.h"

namespace op { namespace lib3D
{
	class Transform3D : public op::math::Matrix<double, 4, 4>
	{
	public:

		Transform3D();

		Transform3D( const Transform3D& other );

		Transform3D( const Matrix<double, 4, 4>& matrix);

	public:

		template <typename T>
		inline void Translation(const op::math::Vector<T, 3>& v)
		{
			matrixData[0] = 1.0;
			matrixData[1] = 0.0;
			matrixData[2] = 0.0;
			matrixData[3] = v[0];
			matrixData[4] = 0.0;
			matrixData[5] = 1.0;
			matrixData[6] = 0.0;
			matrixData[7] = v[1];
			matrixData[8] = 0.0;
			matrixData[9] = 0.0;
			matrixData[10] = 1.0;
			matrixData[11] = v[2];
			matrixData[12] = 0.0;
			matrixData[13] = 0.0;
			matrixData[14] = 0.0;
			matrixData[15] = 1.0;
		}

		inline void Scale(double sx, double sy, double sz)
		{
			matrixData[0] = sx;
			matrixData[1] = 0.0;
			matrixData[2] = 0.0;
			matrixData[3] = 0;
			matrixData[4] = 0.0;
			matrixData[5] = sy;
			matrixData[6] = 0.0;
			matrixData[7] = 0.0;
			matrixData[8] = 0.0;
			matrixData[9] = 0.0;
			matrixData[10] = sz;
			matrixData[11] = 0.0;
			matrixData[12] = 0.0;
			matrixData[13] = 0.0;
			matrixData[14] = 0.0;
			matrixData[15] = 1.0;
		}

	public:


	};


}}


#endif
