#ifndef __SCENENODE_H__
#define __SCENENODE_H__


#include "Transform3D.h"
#include "IRenderable.h"
#include <memory>
#include <vector>
#include <functional>

using std::unique_ptr;


namespace op { namespace lib3D
{

	template <typename T>
	class TreeNode
	{
	public:

		TreeNode()
			:
			parent_(nullptr)
		{}

		virtual ~TreeNode();

	public:

		void AddChild(std::unique_ptr<T> TreeNode);


		// Removes child but doesn't destroy it.
		std::unique_ptr<T> RemoveChild(T* TreeNode);


		void DeleteChild(T* TreeNode);


		int GetChildIndex(const T* TreeNode) const;


		T* GetChild(const unsigned int index) const;


		T* GetChild(const std::string& name) const;


		T* GetChild(std::function<bool(const T*)> predicate) const;


		T* GetNodeInSubtree(std::function<bool(const T*)> predicate) const;


		const std::vector<T* >& GetChildren(void) const;


		T* GetParent(void) const;


		T* GetNext(void) const;


		T* GetPrev(void) const;


		T* GetNextSibling(void) const;


		inline const T* GetConstT(void) const
		{
			return static_cast<const T*>(this);
		}


		inline T* GetT(void) const
		{
			return static_cast<T*>( const_cast<TreeNode<T>* >(this) );
		}

		//T* FindNodeInSubtree(std::function<bool(const T*)> predicate) const;


	private:

		//T* x_;
		T* parent_;
		std::vector<T*> children_;
	};


	template<typename T>
	TreeNode<T>::~TreeNode()
	{
		for (auto iter = this->children_.begin(); iter != this->children_.end(); ++iter)
		{
			delete *iter;
		}
	}


	template<typename T>
	void TreeNode<T>::AddChild(std::unique_ptr<T> node)
	{
		if (node != nullptr)
		{
			children_.push_back(node.get());
			node->parent_ = static_cast<T*>(this);
			node.release();
		}
	}


	template<typename T>
	std::unique_ptr<T> TreeNode<T>::RemoveChild(T* node)
	{
		if (node != nullptr)
		{
			auto it = std::find(std::begin(children_), std::end(children_), node);
			if (it != std::end(children_))
			{
				T* child = *it;
				children_.erase(it);
				child->parent_ = nullptr;
				return std::unique_ptr(child);	// creating a unique_ptr is a no-except operation, 
												// safe to be performed after removing the child from the vector.
			}
		}

		return nullptr;
	}


	template<typename T>
	void TreeNode<T>::DeleteChild(T* node)
	{
		if (node != nullptr)
		{
			auto it = std::find(std::begin(children_), std::end(children_), node);
			if (it != std::end(children_))
			{
				T* child = *it;
				children_.erase(it);
				child->parent_ = nullptr;
				delete child;
			}
		}
	}


	template<typename T>
	int TreeNode<T>::GetChildIndex(const T* node) const
	{
		if (node != nullptr)
		{
			auto it = std::find(std::begin(children_), std::end(children_), node);
			if (it != std::end(children_))
			{
				return ((int)(it - std::begin(children_)));
			}

		}
		return -1;

	}


	template<typename T>
	T* TreeNode<T>::GetChild(const unsigned int index) const
	{
		if (children_.size() > index)
		{
			return children_[index];
		}

		return nullptr;
	}


	template<typename T>
	T* TreeNode<T>::GetChild(std::function<bool(const T*)> predicate) const
	{
		T* node = nullptr;

		auto iter = std::find_if(children_.begin(), children_.end(), predicate);

		if (iter != children_.end())
		{
			node = *iter;
		}
		return node;
	}


	template<typename T>
	T* TreeNode<T>::GetNodeInSubtree(std::function<bool(const T*)> predicate) const
	{
		T* node = nullptr;
		if (predicate(this->GetConstT() ))
		{
			node = this->GetT();
		}
		else
		{
			// using list instead of a vector; pushing elements to the vector may invalidate the iterator.
			std::list<T*> nodesToSearch(children_.begin(), children_.end());

			for (auto iter = nodesToSearch.begin(); iter != nodesToSearch.end(); ++iter)
			{
				if (predicate(*iter))
				{
					node = *iter;
					break;
				}
				else
				{
					auto children = (*iter)->GetChildren();
					nodesToSearch.insert(nodesToSearch.end(), std::begin(children), std::end(children));
				}
			}
		}

		return node;
	}


	template<typename T>
	const std::vector<T*>& TreeNode<T>::GetChildren(void) const
	{
		return children_;
	}


	template<typename T>
	T* TreeNode<T>::GetParent(void) const
	{
		return parent_;
	}


	//T* TreeNode<T>::FindNodeInSubtree(std::function<bool(const T*)> predicate) const{}
	template<typename T>
	T* TreeNode<T>::GetNext(void) const
	{
		T* next = nullptr;
		if (children_.size() > 0)
		{
			next = children_[0];
		}
		else
		{
			const T* curNode = this->GetConstT();
			while (curNode != nullptr)
			{
				T* nextSibling = curNode->GetNextSibling();
				if (nextSibling != nullptr)
				{
					next = nextSibling;
					break;
				}
				else
				{
					curNode = curNode->GetParent();
				}
			}
		}

		return next;
	}


	template<typename T>
	T* TreeNode<T>::GetPrev(void) const
	{
		return nullptr;
	}


	template<typename T>
	T* TreeNode<T>::GetNextSibling(void) const
	{
		T* nextSibling = nullptr;
		if (parent_ != nullptr)
		{
			unsigned int index = parent_->GetChildIndex(this->GetConstT() );
			if (index < parent_->GetChildren().size() - 1)
			{
				nextSibling = parent_->GetChild(index + 1);
			}
		}

		return nextSibling;
	}
}}


namespace op { namespace lib3D
{

	class IRenderer3D;
	class Transform3D;
	class DocObject;


	class SceneNode : public IRenderable, public TreeNode<SceneNode>
	{
	public:

		SceneNode();

		SceneNode(const std::string& name);

		virtual ~SceneNode();

	public:

		virtual void SetObject(unique_ptr<IRenderable> object);


		virtual IRenderable* GetObject(void) const;


		virtual const std::string& GetName(void) const;


		virtual const Transform3D& GetTransform(void) const;


		virtual void SetTransform(const Transform3D&);


		virtual void Transform(const Transform3D& transform);


		virtual void Render(IRenderer3D&, const RenderArgs&);


		bool IsSelected(void) const;

	public:

		void Select(bool select);

	private:

		std::string name;
		Transform3D transform;
		unique_ptr<IRenderable> worldObject;
	};

}}

#endif