#ifndef __RAY3D_H__
#define __RAY3D_H__

#include "stdafx.h"
#include "Vector.h"

#include "Lib3D/GLM/glm/glm.hpp"
#include "Lib3D/GLM/glm/gtc/matrix_transform.hpp"

using op::math::vector2i;
using op::math::vector3d;

namespace op { namespace lib3D
{
	struct Ray3D
	{
		Ray3D(const vector3d& startPt, const vector3d& dir)
		:
			StartPt(startPt),
			Dir(dir.GetUnitVec())
		{}

		void Transform(const glm::tmat4x4<double>& transform )
		{
			glm::tvec4<double> start = glm::tvec4<double>(StartPt.X(), StartPt.Y(), StartPt.Z(), 1.0);
			glm::tvec4<double> end = glm::tvec4<double>(StartPt.X() + Dir.X(), StartPt.Y() + Dir.Y(), StartPt.Z() + Dir.Z(), 1.0);

			auto inverseStart = transform * start;
			auto inverseEnd = transform * end;

			StartPt = vector3d(inverseStart.x, inverseStart.y, inverseStart.z);
			Dir = vector3d(inverseEnd.x - inverseStart.x, inverseEnd.y - inverseStart.y, inverseEnd.z - inverseStart.z);
		}
		
		vector3d StartPt;
		vector3d Dir;
	};
}}

#endif