#ifndef __DEVICEIOMANAGER_H__
#define __DEVICEIOMANAGER_H__

#include "KeyboardEvtArgs.h"
#include "MouseEvtArgs.h"
#include "Core/Include/sigslot.h"

using namespace sigslot;

namespace op { namespace lib3D
{
	class Mouse;
	class Keyboard;

	class DeviceIOManager : public has_slots<single_threaded>
	{
	public:
		
		DeviceIOManager();

		virtual ~DeviceIOManager();
		
	public:

		void TrackMouse(Mouse& mouse);

		void TrackKeyboard(Keyboard& keyboard);

	public:

		sigslot::signal1<op::lib3D::KeyboardEvtArgs&,sigslot::single_threaded> KeyPressed;

		sigslot::signal1<op::lib3D::MouseEvtArgs&, sigslot::single_threaded> MouseClicked;

		sigslot::signal1<op::lib3D::MouseEvtArgs&, sigslot::single_threaded> MouseMoved;
			
	private:

		void OnKeyPressed(op::lib3D::KeyboardEvtArgs& keyEvtArgs);

		void OnMouseMove(op::lib3D::MouseEvtArgs& mouseEvtArgs);

		void OnMouseClick(op::lib3D::MouseEvtArgs& mouseEvtArgs);


	private:

	};

}}


#endif