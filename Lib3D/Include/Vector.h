#ifndef __Vector_H__
#define __Vector_H__

#include <array>
#include <ostream>
#include <math.h>

#include "Matrix.h"
#include "MathDefs.h"

namespace op { namespace math
{
	template <typename T, size_t Length>
	class Vector
	{
	public:
		Vector()
		{};



		Vector(const std::array<T, Length>& data)
			:
			data(data)
		{};


		Vector(const Vector<T, Length>& v)
			:
			data(v.data)
		{};


	public:
			
		bool operator == (const Vector<T, Length>& v)
		{
			bool areEqual = true;
			for (int i = 0; i < Length; i++)
			{
				if (abs(this->data[i] - v.data[i]) > op::mathDefs::Tolerance)
				{
					areEqual = false;
					break;
				}
			}
			return areEqual;
		}


	public:

		T& operator [] (size_t index)
		{
			return data[index];
		}


		const T& operator [] (size_t index) const
		{
			return data[index];
		}


		Vector<T, Length>& operator = (Vector<T, Length> v)        // copy and swap
		{
			std::swap(this->data, v.data);

			return *this;
		}


		Vector<T, Length> operator + (const Vector<T, Length>& v) const
		{
			Vector<T, Length> resultV;
			for (size_t i = 0; i<Length; i++)
			{
				resultV.data[i] = this->data[i] + v.data[i];
			}
			return resultV;
		}


		Vector<T, Length>& operator += (const Vector<T, Length>& v)
		{
			for (size_t i = 0; i<Length; i++)
			{
				this->data[i] += v.data[i];
			}
			return *this;
		}


		Vector<T, Length> operator - (const Vector<T, Length>& v) const
		{
			Vector<T, Length> resultV;
			for (size_t i = 0; i<Length; i++)
			{
				resultV.data[i] = this->data[i] - v.data[i];
			}
			return resultV;
		}


		Vector<T, Length>& operator -= (const Vector<T, Length>& v)
		{
			for (size_t i = 0; i<Length; i++)
			{
				this->data[i] -= v.data[i];
			}
			return *this;
		}



		Vector<T, Length> operator * (const T& t) const
		{
			Vector<T, Length> resultV;
			for (size_t i = 0; i<Length; i++)
			{
				resultV.data[i] = this->data[i] * t;
			}
			return resultV;
		}


		Vector<T, Length>& operator *= (const T& t)
		{
			for (size_t i = 0; i<Length; i++)
			{
				this->data[i] *= t;
			}
			return *this;
		}



		Vector<T, Length> operator / (const T& t) const
		{
			Vector<T, Length> resultV;
			for (size_t i = 0; i<Length; i++)
			{
				resultV.data[i] = this->data[i] / t;
			}
			return resultV;
		}


		Vector<T, Length>& operator /= (const T& t)
		{
			for (size_t i = 0; i<Length; i++)
			{
				this->data[i] /= t;
			}
			return *this;
		}




		T DotProduct(const Vector<T, Length>& v) const
		{
			T t = 0;
			for (size_t i = 0; i<Length; i++)
			{
				t += this->data[i] * v.data[i];
			}
			return t;
		}



		double SquaredLength(void) const
		{
			double squaredLength = 0.0;
			for (size_t i = 0; i<Length; i++)
			{
				squaredLength += this->data[i] * this->data[i];
			}
			return squaredLength;
		}



		double GetLength(void) const
		{
			return sqrt(this->SquaredLength());
		}



		Vector<T, Length>& Unitize(void)
		{
			return *this = this->GetUnitVec();
		}



		Vector<T, Length> GetUnitVec(void) const
		{
			double length = this->GetLength();
			if (length > 0)
			{
				return (*this) / this->GetLength();
			}

			return (*this);
		}



		double DistanceTo(const Vector<T, Length>& otherVec) const
		{
			Vector3<T> v = *this - otherVec;
			return v.GetLength();
		}


		template <typename U>
		explicit operator Vector<U, Length> ()// (void)
		{
			std::array<U, Length> vectorUData;
			for (size_t i = 0; i < Length; i++)
			{
				vectorUData[i] = (U)(data[i]);
			}

			return Vector<U, Length>(vectorUData);
		}


	protected:
		std::array<T, Length> data;
	};


	template <typename T, size_t Length >
	std::ostream& operator << (std::ostream& s, const Vector<T, Length>& v)
	{
		if (Length > 0)
			s << "(" << v[0];
		for (size_t i = 1; i < Length; i++)
			s << "," << v[i];
		s << ")";

		return s;
	}


	template <typename T>
	class Vector3 : public Vector<T, 3>
	{
	public:
		Vector3() = default;

		Vector3(const T& x, const T& y, const T& z)
			:
			Vector<T, 3>(std::array<T, 3>{x, y, z})
		{}


		Vector3(const Vector<T, 3>& v )
			:
			Vector<T, 3>(v)
		{}

	public:

		inline T& X(void)
		{
			return this->data[0];
		}


		inline const T& X(void) const
		{
			return this->data[0];
		}


		inline T& Y(void)
		{
			return this->data[1];
		}


		inline const T& Y(void) const
		{
			return this->data[1];
		}


		inline T& Z(void)
		{
			return this->data[2];
			//return this->operator[](2);
		}


		inline const T& Z(void) const
		{
			return this->data[2];
		}

	public:

		Vector3<T> CrossProduct(const Vector3<T>& vec) const
		{
			return Vector3<T>(Y() * vec.Z() - Z() * vec.Y(),
				Z() * vec.X() - X() * vec.Z(),
				X() * vec.Y() - Y() * vec.X());
		}

	};


	template <typename T>
	class Vector2 : public Vector<T, 2>
	{
	public:
		Vector2() = default;


		Vector2(const T& x, const T& y)
			:
			Vector<T, 2>(std::array<T, 2>{x, y})
		{}


		Vector2(const Vector<T, 2>& v)
			:
			Vector<T, 2>(v)
		{}

	public:

		inline T& X(void)
		{
			return this->data[0];
		}


		inline const T& X(void) const
		{
			return this->data[0];
		}


		inline T& Y(void)
		{
			return this->data[1];
		}


		inline const T& Y(void) const
		{
			return this->data[1];
		}
	};


	typedef Vector2<double> vector2d;
	typedef Vector2<float>  vector2f;
	typedef Vector2<int>	vector2i;
	typedef Vector2<unsigned int>	vector2ui;

	typedef Vector3<double> vector3d;
	typedef Vector3<float>  vector3f;
	typedef Vector3<int>	vector3i;
	typedef Vector3<unsigned int> vector3ui;

	typedef Vector<double,4> vector4d;
	typedef Vector<float,4> vector4f;
	typedef Vector<int,4>	vector4i;
	typedef Vector<unsigned int,4>	vector4ui;
}}


#endif
