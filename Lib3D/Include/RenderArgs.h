#ifndef __RENDERARGS_H__
#define __RENDERARGS_H__


namespace op { namespace lib3D
{
	class IView3D;

	enum class RenderPass : int
	{
		BACKGROUND = 0,
		FOREGROUND
	};

	struct RenderArgs
	{
		RenderPass renderPass;
		const IView3D* view;
	};
}}



#endif