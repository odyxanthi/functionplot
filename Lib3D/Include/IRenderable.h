#ifndef __IRENDERABLE_H__
#define __IRENDERABLE_H__


namespace op { namespace lib3D
{
	class IRenderer3D;
	struct RenderArgs;

	class IRenderable
	{
	public:

		virtual ~IRenderable() = default;

	public:

		virtual void Render( IRenderer3D&, const RenderArgs& ) = 0;
	};
}}




#endif