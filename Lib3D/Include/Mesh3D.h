#ifndef __MESH3D_H__
#define __MESH3D_H__

#include <vector>

#include "Vector.h"
#include "BoundingBox.h"


using namespace op::math;


namespace op { namespace lib3D
{
	struct Triangle
	{
		Triangle() = default;
		Triangle(size_t v1, size_t v2, size_t v3);

		size_t v1;
		size_t v2;
		size_t v3;
	};


	struct Vertex
	{
		Vertex() = default;
		Vertex(const vector3d& position);
		Vertex(const vector3d& position, const vector3d& normal, const vector2d& texCoords);

		vector3d position;
		vector3d normal;
		vector2d texCoords;
		unsigned short degree;
	};


	class Mesh3D
	{
	public:

		Mesh3D() = default;

		Mesh3D(const std::vector<Vertex>& vertices, const std::vector<Triangle>& triangles);
			
		Mesh3D(std::vector<Vertex>&& vertices, std::vector<Triangle>&& triangles);

		~Mesh3D() = default;

	public:

		Vertex&	GetVertex(unsigned int index);

		const Vertex&	GetVertex(unsigned int index) const;

		const std::vector<Triangle>& GetTriangles(void) const;

		const std::vector<Vertex>& GetVertices(void) const;

		void Scale(double scaleFactor);

		const BoundingBox& GetBoundingBox(void) const;


	private:

		void CalculateNormals(void);

		void UpdateBoundingBox(void);

	private:

		std::vector<Triangle> _triangles;
		std::vector<Vertex> _vertices;
		BoundingBox _bbox;
	};
}}


#endif
