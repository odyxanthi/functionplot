#ifndef __MOUSEEVTARGS_H__
#define __MOUSEEVTARGS_H__

#include "stdafx.h"
#include "Vector.h"

using op::math::vector2i;

namespace op { namespace lib3D
{
	class IView3D;

	class MouseEvtArgs
	{
	public:
		MouseEvtArgs(const IView3D& v)
			:
			view(v)
		{}

		virtual ~MouseEvtArgs(){};

	public:

		virtual vector2i GetMousePosition(void) const = 0;

		virtual bool LeftButtonDown(void) const = 0;
		virtual bool LeftButtonClick(void) const = 0;
		virtual bool RightButtonDown(void) const = 0;
		virtual bool RightButtonClick(void) const = 0;
		virtual bool MiddleButtonDown(void) const = 0;
		virtual bool MiddleButtonClick(void) const = 0;

	public:

		virtual bool ControlDown(void) const = 0;
		virtual bool ShiftDown(void) const = 0;

	public:

		const IView3D& Get3DView(void) { return view; }

	private:

		const IView3D& view;
	};
}}


#endif