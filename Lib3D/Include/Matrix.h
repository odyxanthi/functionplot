#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <array>
#include <type_traits>
#include <ostream>

#include "Vector.h"

namespace op { namespace math
{
	template <typename T, size_t TRows, size_t TCols>
	class Matrix
	{
	public:

		Matrix()
		{
			std::fill(std::begin(this->matrixData), std::end(this->matrixData), 0.0);
		};


		Matrix(const Matrix<T,TRows,TCols>& other)
			:
			matrixData(other.matrixData)
		{};


		Matrix(const std::array<T, TRows*TCols>& m)
			:
			matrixData(m)
		{};

	public:

		T& operator ()(size_t row, size_t col)
		{
			size_t index = row*TCols + col;

			return this->matrixData[index];
		};


		const T& operator ()(size_t row, size_t col) const
		{
			size_t index = row*TCols + col;

			return this->matrixData[index];
		};


		T& operator [](size_t index)
		{
			return this->matrixData[index];
		};


		const T& operator [](size_t index) const
		{
			return this->matrixData[index];
		};


	public:

		template<size_t rhs_Cols>
		Matrix < T, TRows, rhs_Cols> operator * (const Matrix<T, TCols, rhs_Cols>& rhs)
		{
			Matrix < T, TRows, rhs_Cols> resultMatrix;
			for (size_t i = 0; i < TRows; i++)
			{
				for (size_t j = 0; j < rhs_Cols; j++)
				{
					for (size_t k = 0; k < TCols; k++)
					{
						resultMatrix(i, j) += (*this)(i, k) * rhs(k, j);
					}
				}
			}

			return resultMatrix;
		};


		template<size_t rhs_Cols>
		Matrix < T, TRows, rhs_Cols>& operator *= (const Matrix<T, TCols, rhs_Cols>& rhs)
		{
			Matrix < T, TRows, rhs_Cols> product = (*this)*rhs;
			*this = product;
			return *this;
		};

	public:

		static Matrix<T, TRows, TCols> CreateIdentity(void)
		{
			Matrix<T, TRows, TCols> m;
			m.SetToIdentity();
			return m;
		}


		void SetToIdentity(void)
		{
			for (int i = 0; i < TRows; ++i)
			{
				if (i < TCols)
					(*this)(i,i) = 1.0;
			}
		}


		Matrix<T, TCols, TRows> Transpose(void) const
		{
			Matrix<T, TCols, TRows> transpose;
			for (int i = 0; i < TRows; ++i)
			{
				for (int j = 0; j < TCols; ++j)
				{
					transpose(j, i) = (*this) (i, j);
				}
			}
			return transpose;
		}


	protected:
			
		std::array<T,TRows*TCols> matrixData;
	};


	template <typename T, size_t TRows, size_t TCols>
	std::ostream& operator << (std::ostream& s, const Matrix<T, TRows, TCols>& m)
	{
		for (size_t i = 0; i<TRows; i++)
		{
			for (size_t j = 0; j<TCols; j++)
			{
				s << m(i, j) << ",";
			}
			s << "\n";
		}

	}

}}

#endif
