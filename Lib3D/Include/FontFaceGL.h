#ifndef __FONTFACEGL_H__
#define __FONTFACEGL_H__

#include "Vector.h"

#include <string>
#include <array>
#include "GL/glew.h"


struct FT_FaceRec_;
typedef struct FT_FaceRec_*  FT_Face;


namespace op { namespace lib3D
{
	class FontFaceGL
	{
	public:

		FontFaceGL(const FT_Face& face);

		~FontFaceGL(void);

	public:

		void DrawText(const std::string& text, const op::math::vector3d& origin, const op::math::vector3d& dirX, const op::math::vector3d& dirY, float textHeight);

	private:

		void Initialize();

	private:
		FT_Face _fontFace;
		std::array<GLuint, 128> _textures;
		
	};
}}


#endif