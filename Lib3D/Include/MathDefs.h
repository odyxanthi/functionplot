#ifndef __MATHDEFS_H__
#define __MATHDEFS_H__

namespace op { namespace mathDefs
{
	const double PI = 3.14159265;

	const double Tolerance = 0.0000001;
}}

#endif