#ifndef __IRENDERER3D_H__
#define __IRENDERER3D_H__


#include "Vector.h"
#include <vector>
#include <memory>
#include <string>

using namespace std;

namespace op { namespace lib3D
{
	
	class Mesh3D;
	class Transform3D;
	class Material;
	class TextModel3D;
	class Color;
	struct RenderArgs;

	class IRenderer3D
	{
	public:

		virtual ~IRenderer3D(){};

		virtual void InitializeShaders() = 0;
	public:

		virtual void EnableLighting(bool enable) = 0;

	public:

		virtual void SetMaterial(const Material& material) = 0;

		virtual void SetMaterial(int) = 0;

		virtual void SetColorMaterial(const Color& color) = 0;

		virtual void DrawMesh(const lib3D::Mesh3D& mesh) = 0;
		
		virtual void DrawPoint(const math::vector3d& point) = 0;

		virtual void DrawPoint(const math::vector3f& point) = 0;

		virtual void DrawLine(const math::vector3d& start, const math::vector3d& end) = 0;

		virtual void DrawLine(const math::vector3f& start, const math::vector3f& end) = 0;

		virtual void DrawText(const std::string& text, const op::math::vector3d& origin, const op::math::vector3d& dirX, const op::math::vector3d& dirY, float textHeight) = 0;
	public:

		virtual unsigned int CreateHardwareBuffer(const char* filename) = 0;

		virtual unsigned int CreateHardwareBuffer(const Mesh3D& mesh) = 0;

		virtual unsigned int CreateHardwareBuffer(const std::vector<math::vector3d>& points) = 0;

		virtual void DrawHardwareBuffer(unsigned int bufferId) = 0;

		virtual void DeleteHardwareBuffer(unsigned int bufferId) = 0;

	public:

		virtual unsigned int CreateTexture2D(const char* textureFile) = 0;

		virtual void LoadTexture(unsigned int textureId) = 0;

	public:

		virtual unsigned int CreateShader(const char* vertexShaderFile, const char* fragShaderFile, const char* shaderName ) = 0;

		virtual unsigned int GetShaderId(const char* shaderName) = 0;

		virtual void UseShader(unsigned int shaderId) = 0;

		virtual void UseShader(const char* shaderName) = 0;

		virtual void DestroyShader(unsigned int shaderId) = 0;

	public:

		virtual void SetModelTransform(const float matrix[16]) = 0;

		virtual void SetCameraTransform(const float matrix[16]) = 0;

		virtual void SetProjection(const float matrix[16]) = 0;

		virtual void SetModelTransform(const double matrix[16]) = 0;

		virtual void SetCameraTransform(const double matrix[16]) = 0;

		virtual void SetProjection(const double matrix[16]) = 0;

	public:

		virtual void SetUniform_1f(unsigned int programId, const char* attrId, float value) = 0;

	};
}}


#endif