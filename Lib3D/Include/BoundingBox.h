#ifndef __BOUNDINGBOX_H_
#define __BOUNDINGBOX_H_

#include "Vector.h"


using namespace op::math;

namespace op { namespace lib3D 
{

	class BoundingBox
	{
	public:

		BoundingBox() = default;

		BoundingBox(const vector3d& min, const vector3d& max)
			:
			_min(min),
			_max(max)
		{}

		~BoundingBox() = default;

	public:

		inline const vector3d& Min(void) const { return _min; }

		inline const vector3d& Max(void) const { return _max; }

		inline double ExtentsX(void) const { return _max.X() - _min.X(); };

		inline double ExtentsY(void) const { return _max.Y() - _min.Y(); };

		inline double ExtentsZ(void) const { return _max.Z() - _min.Z(); };

	public:

		vector3d _min;
		vector3d _max;
	};

}}


#endif
