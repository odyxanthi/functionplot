#ifndef __RENDERERGL_H__
#define __RENDERERGL_H__

#include "IRenderer3D.h"
#include <map>
#include <string>
#include <array>
#include <memory>

using std::unique_ptr;

namespace op { namespace lib3D
{
	class Mesh3D;
	class Transform3D;
	class HardwareBufferGL;
	class TextModel3D;
	class FontFaceGL;
	class FontManager;

	class RendererGL : public IRenderer3D
	{
	public:

		RendererGL();

		virtual ~RendererGL();

		virtual void InitializeShaders() override;
	public:

		virtual void EnableLighting(bool enable);

	public:

		virtual void SetMaterial(const Material& material) override;

		virtual void SetMaterial(int) override;

		virtual void SetColorMaterial(const Color& color) override;

	public:
		// mesh
		virtual void DrawMesh(const op::lib3D::Mesh3D& mesh);
			
		// points
		virtual void DrawPoint(const op::math::vector3d& point);

		virtual void DrawPoint(const op::math::vector3f& point);

		// lines
		virtual void DrawLine(const math::vector3d& start, const math::vector3d& end);

		virtual void DrawLine(const math::vector3f& start, const math::vector3f& end);

		virtual void DrawText(const std::string& text, const op::math::vector3d& origin, const op::math::vector3d& dirX, const op::math::vector3d& dirY, float textHeight);
		
	public:

		virtual unsigned int CreateHardwareBuffer(const char* filename);

		virtual unsigned int CreateHardwareBuffer(const Mesh3D& mesh);

		virtual unsigned int CreateHardwareBuffer(const std::vector<math::vector3d>& points);

		virtual void DrawHardwareBuffer(unsigned int bufferId);

		virtual void DeleteHardwareBuffer(unsigned int bufferId);

	public:

		virtual unsigned int CreateTexture2D(const char* textureFile);

		virtual void LoadTexture(unsigned int textureId);

	public:

		// TODO - Fix crash. It crashes when file is not found
		virtual unsigned int CreateShader(const char* vertexShaderFile, const char* fragShaderFile, const char* shaderName);

		virtual unsigned int GetShaderId(const char* shaderName);

		virtual void UseShader(unsigned int shaderId);

		virtual void UseShader(const char* shaderName);

		virtual void DestroyShader(unsigned int shaderId);

		int GetCurrentProgram(void);

	public:

		virtual void SetModelTransform(const float matrix[16]) override;

		virtual void SetCameraTransform(const float matrix[16]) override;

		virtual void SetProjection(const float matrix[16]) override;

		virtual void SetModelTransform(const double matrix[16]) override;

		virtual void SetCameraTransform(const double matrix[16]) override;

		virtual void SetProjection(const double matrix[16]) override;

	public:

		virtual void SetUniform_1f(unsigned int programId, const char* attrId, float value);

	private:

		std::map<std::string, unsigned int> _shaderMap;
		std::map<unsigned int, unique_ptr<HardwareBufferGL>> _vboMap;
		std::unique_ptr<FontFaceGL> _activeFontFace;
		unique_ptr<FontManager> _fontMgr;


		float modelTransform[16];
		float cameraTransform[16];
		float projectionTranform[16];
	};

}}


#endif