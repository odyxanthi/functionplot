#ifndef __STDAFX_LIB3D_H__
#define __STDAFX_LIB3D_H__


#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>


#ifdef _DEBUG
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>

	#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
	#define new DEBUG_NEW
#endif


#endif