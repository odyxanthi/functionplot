#ifndef __ENGINE3D_H__
#define __ENGINE3D_H__

#include "stdafx.h"
#include <memory>


using namespace std;


namespace core
{
	class Logger;
}


namespace op { namespace lib3D
{
	class SceneManager;
	class DeviceIOManager;
	class IRenderer3D;



	class Engine3D
	{
	private:

		Engine3D();

		virtual ~Engine3D();

	public:

		static Engine3D* CreateInstance(void);

		static Engine3D* GetInstance(void);

		static void DestroyInstance(void);

	public:

		::core::Logger* GetLogger(void);

		SceneManager* GetSceneManager(void);

		DeviceIOManager* GetDeviceManager(void);

		IRenderer3D* GetRenderer(void);

	private:

		unique_ptr<::core::Logger> logger;
		unique_ptr<SceneManager> sceneManager;
		unique_ptr<DeviceIOManager> deviceManager;
		unique_ptr<IRenderer3D> renderer;

	private:

		static Engine3D* _engineSingleton;
	};
}}


#endif