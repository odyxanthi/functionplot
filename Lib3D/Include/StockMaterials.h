/*!
* \file StockMaterials.h
* \brief Declaration of MeshModel3D
* \author Odysseas Petrocheilos
*
* contact: odyxanthi@gmail.com
*/

#ifndef __STOCKMATERIALS_H__
#define __STOCKMATERIALS_H__

#include "Material.h"



namespace op { namespace lib3D
{
	class StockMaterials
	{
	public:
		StockMaterials();

	public:

		static const Material Black;
		static const Material Blue;
		static const Material Green;
		static const Material Grey;
		static const Material Red;
		static const Material White;
		static const Material Yellow;
			
		static const Material MatBlue;
		static const Material TransparentRed;
		static const Material TransparentYellow;
		static const Material TransparentPurple;
	};
}}


#endif
