#ifndef __MOUSE_H__
#define __MOUSE_H__

#include "MouseEvtArgs.h"
#include "Core/Include/sigslot.h"

using namespace sigslot;


namespace op { namespace lib3D
{
	class Mouse
	{
	public:
		Mouse();

		virtual ~Mouse();

	public:

		signal1<op::lib3D::MouseEvtArgs&,single_threaded> MouseMove;

		signal1<op::lib3D::MouseEvtArgs&, single_threaded> MouseClick;

	};

}}


#endif