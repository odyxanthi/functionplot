#ifndef __IVIEW3D_H__
#define __IVIEW3D_H__

#include "stdafx.h"
#include "Vector.h"
#include "Ray3D.h"

using op::math::vector2i;
using op::math::vector3d;

namespace op { namespace lib3D
{
	class Camera;

	class IView3D
	{
	public:

		virtual vector2i GetSize(void) const = 0;

		virtual unsigned int GetHeight(void) const = 0;

		virtual unsigned int GetWidth(void) const = 0;

		virtual Ray3D Unproject(const vector2i pos) const = 0;

		virtual const Camera& GetCamera(void) const = 0;
	};
}}


#endif