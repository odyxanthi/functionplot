#ifndef MATRIXUTILS_H__
#define MATRIXUTILS_H__

#include "Matrix.h"
#include "Lib3D/GLM/glm/glm.hpp"
#include "Lib3D/GLM/glm/mat4x4.hpp"

namespace op { namespace math {

	template <typename T, size_t MatrixSize>
	Matrix<T, MatrixSize, MatrixSize> IdentityMatrix(void)
	{
		Matrix<T, MatrixSize, MatrixSize> m;
		for (size_t i = 0; i < MatrixSize; ++i)
		{
			m(i, i) = 1;
		}

		return m;
	}


	template <typename T>
	glm::mat4 ConvertToGLMMat4(const Matrix<T, 4, 4>& matrixIn)
	{
		glm::mat4 matrixOut;
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				matrixOut[j][i] = (float)matrixIn[i*4+j];
			}
		}

		return matrixOut;
	}


	template <typename T>
	void MatrixToArray(const glm::tmat4x4<T>& m, std::array<T,16>& arrayOut)
	{
		for (int i = 0; i < 4; ++i)
		{
			auto column = m[i];
			for (int j = 0; j < 4; ++j)
			{
				arrayOut[i * 4 + j] = column[j];
			}
		}
	}
}}
#endif