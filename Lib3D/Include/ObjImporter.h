#ifndef __OBJIMPORTER_H__
#define __OBJIMPORTER_H__

#include <string>
#include <vector>
#include <list>
#include <memory>
#include <iostream>
#include <memory>
#include "Mesh3D.h"
#include "Vector.h"

using namespace std;


namespace op { namespace math
{
	//class vector3d;
}}


namespace op { namespace lib3D
{
	struct VertexInfo
	{
		int positionIndex;
		int textureIndex;
		int normalIndex;
	};


	inline std::ostream& operator << (std::ostream& s, const VertexInfo& vInfo)
	{
		s << "p: " << vInfo.positionIndex << "   ";
		s << "t: " << vInfo.textureIndex << "   ";
		s << "n: " << vInfo.normalIndex;

		return s;
	};


	struct FacetInfo
	{
		std::list<VertexInfo> vertices;
	};


	inline std::ostream& operator << (std::ostream& s, const FacetInfo& fInfo)
	{
		s << "Facet:\n";
		for (auto& vInfo : fInfo.vertices)
		{
			s << "  " << vInfo << "\n";
		}
		return s;
	};


	class ObjImporter
	{
	public:

		ObjImporter();

	public:

		unique_ptr<op::lib3D::Mesh3D> ReadMesh(const std::string& filename);

		unique_ptr<op::lib3D::Mesh3D> ReadMesh(std::istream& inputStream);


	private:

		void ReadVertex(std::istream& s, vector3d& v);

		void ReadTextureCoord(std::istream& s, vector2d& vt);

		void ReadNormal(std::istream& s, vector3d& vn);

		void ReadFacet(std::istream& s, FacetInfo& facetInfo);

	private:

		std::vector< op::lib3D::Vertex > vertices;
		std::vector< vector3d > normals;
		std::vector< vector2d > textureCoords;
		std::vector< op::lib3D::Triangle > triangles;
		std::list< FacetInfo >  facets;
	};


	unsigned int SplitString(const string& s, char delim, list<string>& substrings);

	unsigned int SplitString(const string& s, list<string>& substrings);

}}


#endif
