#ifndef __GEOMETRYUTILS_H__
#define __GEOMETRYUTILS_H__

#include <vector>
#include "Vector.h"


using namespace op::math;

namespace op {  namespace lib3D
{
	class Mesh3D;
	struct Ray3D;

	class GeometryUtils
	{
	public:
		static bool RayTriangleIntersection(const Ray3D& ray, const vector3d& v0, const vector3d& v1, const vector3d& v2, vector3d& intersection);

		static bool RayMeshIntersection(const Ray3D& ray, const Mesh3D& mesh, vector3d& nearestIntersection);

		static bool RayBoxIntersection(const Ray3D& ray, const vector3d& boxMin, const vector3d& boxMax);

		static std::vector<double> SampleDomain(double minx, double maxx, int samplePoints);
	};
}}

#endif
