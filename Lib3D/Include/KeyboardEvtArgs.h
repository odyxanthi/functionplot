#ifndef __KEYBOARDEVTARGS_H__
#define __KEYBOARDEVTARGS_H__

#include "stdafx.h"


namespace op { namespace lib3D
{
	class KeyboardEvtArgs
	{
	public:
		virtual ~KeyboardEvtArgs(){};

		virtual int GetKeyCode(void) const = 0;

	};
}}

#endif