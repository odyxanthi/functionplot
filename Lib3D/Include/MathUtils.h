#ifndef __MATHUTILS_H__
#define __MATHUTILS_H__

#include "Matrix.h"
#include "Vector.h"


namespace op { namespace math
{

	class MathUtils
	{
	public:

		static Matrix<double, 4, 4> CreateTranslationMatrix(const vector3d& translation);


		static Matrix<double, 4, 4> CreateRotationMatrix(double rotationAngle, const vector3d& rotationAxis);


		static double DegreesToRadians(double angleInDegrees);


		static double RadiansToDegrees(double angleInRadians);


		static bool WithinTolerance(double d1, double d2, double tolerance);

	};


	vector3d operator * (const Matrix<double, 4, 4>& transform, const vector3d& p);
}}


#endif 