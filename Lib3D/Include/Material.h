#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include "Vector.h"


namespace op { namespace lib3D
{
	class Color : public op::math::vector4f
	{
	public:

		Color();

		Color(float r, float g, float b);

		Color(float r, float g, float b, float a);

		explicit Color(const op::math::vector3f& v);

		explicit Color(const op::math::vector4f& v);

	public:

		//inline bool operator == (const Color& other) { return this->rgba == other.rgba; };

	public:

		inline float& R(){ return (*this)[0]; };
		inline float& G(){ return (*this)[1]; };
		inline float& B(){ return (*this)[2]; };
		inline float& A(){ return (*this)[3]; };

		inline float R() const { return (*this)[0]; };
		inline float G() const { return (*this)[1]; };
		inline float B() const { return (*this)[2]; };
		inline float A() const { return (*this)[3]; };

	private:

	};

	
	class Material
	{
	public:

		Material();

		Material(const Color& ambient,
				const Color& diffuse,
				const Color& specular,
				const Color& emissive);
		
		Material(const Material& other );

		Material& operator = (Material other);

	public:

		bool operator == (const Material& other);

	public:

		Color ambient;
		Color diffuse;
		Color specular;
		Color emissive;
		double shininess;
		double transparency;
	};
}}


#endif
