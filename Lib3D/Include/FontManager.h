#ifndef _FONTMANAGER_H__
#define _FONTMANAGER_H__


#include <string>
#include <map>
#include <memory>

using namespace std;

struct FT_FaceRec_;
typedef struct FT_FaceRec_*  FT_Face;

struct FT_LibraryRec_;
typedef struct FT_LibraryRec_  *FT_Library;


namespace op { namespace lib3D 
{

	class FontManager
	{
	public:

		FontManager();

		virtual ~FontManager();

	public:

		FT_Face GetDefaultFontFace() const;
		FT_Face GetFontFace(const std::string& fontFaceName);

	private:

		map<string, FT_Face> _fontFaces;
		FT_Library _ftLib	 = nullptr;
		FT_Face _defaultFont = nullptr;
	};

}}

#endif