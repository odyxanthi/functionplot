#ifndef __CAMERAMAN_H__
#define __CAMERAMAN_H__

#include <memory>
#include "Vector.h"


using namespace std;
using op::math::vector2i;


namespace op { namespace lib3D
{
	class Camera;
	class MouseEvtArgs;
	class KeyboardEvtArgs;


	class CameraMan
	{

	public:

		CameraMan();

		~CameraMan(){};

	public:

		void SetCamera(Camera* camera);

		Camera* GetCamera(void);

		void OnMouseMove(const op::lib3D::MouseEvtArgs& args);

		void OnKeyPressed(const op::lib3D::KeyboardEvtArgs& args);

	private:

		Camera* _attachedCamera;
		vector2i _prevMousePos;

		const double translationSpeed;
		const double rotationSpeed;
	};
}}


#endif