#ifndef _TEXTMODEL3D_H__
#define _TEXTMODEL3D_H__

#include "IRenderable.h"
#include "Vector.h"

#include <string>
#include <memory>

struct FT_FaceRec_;
typedef struct FT_FaceRec_*  FT_Face;

using op::math::vector3d;

namespace op { namespace lib3D 
{

	class IRenderer3D;
	class FontManager;


	enum class TextStyle { BILLBOARD, FIXED3D };


	class TextModel3D : public IRenderable
	{
	public:

		TextModel3D();

		virtual ~TextModel3D();

	public:

		void SetFontFace(const std::string& fontName);

		void SetFontSize(float fontSize);

		float GetFontSize() const;

		void SetText(const std::string& text);

		const std::string& GetText() const;

		virtual void Render(IRenderer3D&, const RenderArgs&) = 0;

	private:
		float _fontSize;
		std::string _text;
		std::string _fontFaceName;
	};


	class BillboardText : public TextModel3D
	{
	public:

		BillboardText();

		virtual ~BillboardText();

		void SetPosition(const vector3d& pos);

		virtual void Render(IRenderer3D&, const RenderArgs&) override;

	private:
		vector3d _position3D;
	};


	class Oriented3DText : public TextModel3D
	{
	public:

		Oriented3DText();

		virtual ~Oriented3DText();

	public:

		void SetTextPlane(const vector3d& origin, const vector3d& x, const vector3d& y);

		virtual void Render(IRenderer3D&, const RenderArgs&) override;

	private:
		vector3d _planeOrigin;
		vector3d _planeX;
		vector3d _planeY;
	};

}}

#endif