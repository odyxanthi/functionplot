#include "gtest/gtest.h"

#include "ft2build.h"
#include FT_FREETYPE_H

#include "GL/glew.h"

TEST(LibraryIntegration, FreeType)
{
	FT_Library _ftLib = nullptr;
	if (FT_Init_FreeType(&_ftLib))
	{
		throw new std::runtime_error("Could not initialize freetype library");
	}
}


//TEST(LibraryIntegration, GLEW)
//{
//	::glLineWidth(2.0);
//}


int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc,argv);
	return RUN_ALL_TESTS();
}