#include "../Include/SceneNode.h"
#include <memory>
#include <list>
#include "gtest/gtest.h"

using std::unique_ptr;
using namespace op::lib3D;

//! IntNode: Class to facilitate testing of the TreeNode class.
class IntNode : public TreeNode<IntNode>
{
public:
	IntNode(const int& val)
		:
		val_(val)
	{};


	int GetValue(void) const { return val_; }


private:

	int val_;
};


std::unique_ptr<IntNode> CreateTestTree()
{
	auto p = std::make_unique<IntNode>(1);

	auto p2 = std::make_unique<IntNode>(2);
	p2->AddChild(std::make_unique<IntNode>(3));
	p->AddChild(std::move(p2));

	auto p4 = std::make_unique<IntNode>(4);
	p4->AddChild(std::make_unique<IntNode>(5));
	p4->AddChild(std::make_unique<IntNode>(6));
	p->AddChild(std::move(p4));

	auto p7 = std::make_unique<IntNode>(7);
	p7->AddChild(std::make_unique<IntNode>(8));
	p->AddChild(std::move(p7));

	p->AddChild(std::make_unique<IntNode>(9));

	return p;
}


TEST(SceneNodeTest, FindChildIndexByRef) 
{
	unique_ptr<IntNode> root(new IntNode(1));

	auto p0 = new IntNode(0);
	auto p1 = new IntNode(1);
	auto p2 = new IntNode(2);

	root->AddChild(std::unique_ptr<IntNode>(p0));
	root->AddChild(std::unique_ptr<IntNode>(p1));
	root->AddChild(std::unique_ptr<IntNode>(p2));

	EXPECT_EQ(0, root->GetChildIndex(p0));
	EXPECT_EQ(1, root->GetChildIndex(p1));
}



TEST(GetNodeInSubtree, FindNodeByPredicate)
{
	auto rootNode = CreateTestTree();

	auto p0 = rootNode->GetNodeInSubtree([](const IntNode *n){ return n->GetValue() == 0; });
	auto p1 = rootNode->GetNodeInSubtree([](const IntNode *n){ return n->GetValue() == 1; });
	auto p4 = rootNode->GetNodeInSubtree([](const IntNode *n){ return n->GetValue() == 4; });
	auto p6 = rootNode->GetNodeInSubtree([](const IntNode *n){ return n->GetValue() == 6; });
	auto p8 = rootNode->GetNodeInSubtree([](const IntNode *n){ return n->GetValue() == 8; });
	
	EXPECT_TRUE(p0 == nullptr);
	ASSERT_TRUE(p1 != nullptr);
	ASSERT_TRUE(p4 != nullptr);
	ASSERT_TRUE(p6 != nullptr);
	ASSERT_TRUE(p8 != nullptr);

	EXPECT_EQ(1, p1->GetValue());
	EXPECT_EQ(4, p4->GetValue());
	EXPECT_EQ(6, p6->GetValue());
	EXPECT_EQ(8, p8->GetValue());
}


TEST(ChildNodeIsNext, GetNext)
{
	auto n1 = CreateTestTree();

	auto n = n1->GetNext();
	ASSERT_TRUE(n != nullptr);
	ASSERT_EQ(2, n->GetValue());
}


TEST(SiblingNodeIsNext, GetNext)
{
	auto root = CreateTestTree();

	auto current = root->GetNodeInSubtree([](const IntNode* n){ return n->GetValue() == 3; });
	ASSERT_TRUE(current != nullptr);
	
	auto next = current->GetNext();
	ASSERT_TRUE(next != nullptr);
	ASSERT_EQ(4, next->GetValue());
}


TEST(NextNodeInDifferentBranch, GetNext)
{
	auto root = CreateTestTree();

	auto current = root->GetNodeInSubtree([](const IntNode* n){ return n->GetValue() == 8; });
	ASSERT_TRUE(current != nullptr);

	auto next = current->GetNext();
	ASSERT_TRUE(next != nullptr);
	ASSERT_EQ(9, next->GetValue());
}


TEST(NoNextNode, GetNext)
{
	auto root = CreateTestTree();

	auto current = root->GetNodeInSubtree([](const IntNode* n){ return n->GetValue() == 9; });
	ASSERT_TRUE(current != nullptr);

	auto next = current->GetNext();
	ASSERT_TRUE(next == nullptr);
}


TEST(RootNodeOnly, GetNext)
{
	auto root = std::make_unique<IntNode>(1);
	auto next = root->GetNext();

	ASSERT_TRUE(next == nullptr);
}


TEST(VisitAll, GetNext)
{
	auto tree = CreateTestTree();
	auto current = tree.get();

	std::vector<int> values;
	while (current != nullptr)
	{
		values.push_back(current->GetValue());
		current = current->GetNext();
	}

	for (int i = 0; i < values.size(); ++i)
	{
		EXPECT_EQ(i + 1, values[i]);
	}
}