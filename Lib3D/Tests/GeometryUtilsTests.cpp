#include "../Include/MathUtils.h"
#include "../Include/GeometryUtils.h"
#include "gtest/gtest.h"


TEST(SampleDomainTest, ExpectedCase)
{
	auto pts = op::lib3D::GeometryUtils::SampleDomain(10.0, 20.0, 5);

	EXPECT_TRUE(pts.size() == 5);
	EXPECT_TRUE(op::math::MathUtils::WithinTolerance(pts[0], 10.0, 0.000001));
	EXPECT_TRUE(op::math::MathUtils::WithinTolerance(pts[1], 12.5, 0.000001));
	EXPECT_TRUE(op::math::MathUtils::WithinTolerance(pts[2], 15.0, 0.000001));
	EXPECT_TRUE(op::math::MathUtils::WithinTolerance(pts[3], 17.5, 0.000001));
	EXPECT_TRUE(op::math::MathUtils::WithinTolerance(pts[4], 20.0, 0.000001));
}