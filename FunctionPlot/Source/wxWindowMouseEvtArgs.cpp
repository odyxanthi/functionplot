#include "stdafx.h"
#include "WxWindowMouseEvtArgs.h"


namespace op
{
	namespace wxWidgets
	{
		WxWinodwMouseEvtArgs::WxWinodwMouseEvtArgs(const wxMouseEvent& mouseEvt, const op::lib3D::IView3D& view)
			:
			op::lib3D::MouseEvtArgs(view),
			_wxMouseEvt(mouseEvt)
		{}


		vector2i WxWinodwMouseEvtArgs::GetMousePosition(void) const
		{
			vector2i mousePos(_wxMouseEvt.m_x, _wxMouseEvt.m_y);
			
			return mousePos;
		}


		bool WxWinodwMouseEvtArgs::LeftButtonDown(void) const
		{
			return _wxMouseEvt.LeftIsDown();
		}


		bool WxWinodwMouseEvtArgs::LeftButtonClick(void) const
		{
			return _wxMouseEvt.LeftDown();
		}


		bool WxWinodwMouseEvtArgs::RightButtonDown(void) const
		{
			return _wxMouseEvt.RightIsDown();
		}


		bool WxWinodwMouseEvtArgs::RightButtonClick(void) const
		{
			return _wxMouseEvt.RightDown();
		}


		bool WxWinodwMouseEvtArgs::MiddleButtonDown(void) const
		{
			return _wxMouseEvt.MiddleDown();
		}


		bool WxWinodwMouseEvtArgs::MiddleButtonClick(void) const
		{
			return _wxMouseEvt.MiddleDown();
		}


		bool WxWinodwMouseEvtArgs::ControlDown(void) const
		{
			return _wxMouseEvt.ControlDown();
		}
		
		
		bool WxWinodwMouseEvtArgs::ShiftDown(void) const
		{
			return _wxMouseEvt.ShiftDown();
		}

	}
}
