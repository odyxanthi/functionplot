#ifndef __FUNCTIONVISUALIZATIONENTITY_H__
#define __FUNCTIONVISUALIZATIONENTITY_H__

#include <memory>
#include <list>
#include <tuple>
#include <string>
#include ".\Lib3D\Include\IRenderable.h"
#include ".\Lib3D\Include\Vector.h"
#include ".\Lib3D\GLM\glm\mat4x4.hpp"

using namespace std;



namespace op { namespace lib3D 
{
	class IRenderer3D;
}}


namespace op { namespace fplot 
{
	class IEvaluatorFactory;
	class IFunctionEvaluator;
	class FunctionModel3D;
	class AxisModel3D;

	class FunctionVisualizationEntity : public op::lib3D::IRenderable
	{
	public:

		FunctionVisualizationEntity();

		virtual ~FunctionVisualizationEntity();

	public:

		void DefineFunction(const std::string& functionFormula, const std::string& sampleX, const std::string& sampleY);

		void CreateMeshFromFunction(const std::string& functionFormula, const std::string& sampleX, const std::string& sampleY);

		virtual void Render(op::lib3D::IRenderer3D& renderer, const op::lib3D::RenderArgs&) override;

	private:

		unique_ptr<IEvaluatorFactory> _evaluatorFactory;
		unique_ptr<FunctionModel3D> _model3D;
		unique_ptr<AxisModel3D> _axisModel3D;
		//op::math::vector3d _modelBboxMin;
		//op::math::vector3d _modelBboxMax;

		const double extentsX = 200.0;
		const double extentsY = 200.0;
		const double extentsZ = 200.0;
	};
}}


#endif //FUNCTIONVISUALIZATIONENTITY_H__
