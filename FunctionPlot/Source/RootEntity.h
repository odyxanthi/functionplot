#ifndef __ROOTENTITY_H__
#define __ROOTENTITY_H__

#include <memory>
#include <list>
#include <tuple>
#include <string>
#include "sigslot.h"

#include ".\Lib3D\Include\IRenderable.h"
#include "GL/glew.h"

using namespace std;
using namespace sigslot;


namespace op { namespace lib3D
{
	class IRenderable;
	class MeshModel3D;
	class PolylineModel3D;
	class TextModel3D;
	struct RenderArgs;
}}


namespace op { namespace fplot
{
	class FunctionVisualizationEntity;


	class RootEntity : public has_slots<single_threaded>, public op::lib3D::IRenderable
	{
	public:

		RootEntity();

	public:

		void Init();

		FunctionVisualizationEntity* GetFunctionVisualizationEntity(void);

		virtual void Render(op::lib3D::IRenderer3D&, const op::lib3D::RenderArgs&);


	private:

		void DrawReferenceBox(op::lib3D::IRenderer3D&, const op::lib3D::RenderArgs&);
		void DrawGrid(void);

	private:

		unique_ptr<op::fplot::FunctionVisualizationEntity> functionVisualizationEntity;
		unique_ptr<op::lib3D::TextModel3D> textModel;
	};
}}


#endif