#include "stdafx.h"

#include "RootEntity.h"
#include "FunctionPlotApp.h"
#include "FunctionVisualizationEntity.h"

#include ".\Lib3D\Include\SceneManager.h"
#include ".\Lib3D\Include\SceneNode.h"
#include ".\Lib3D\Include\Engine3D.h"
#include ".\Lib3D\Include\ObjImporter.h"
#include ".\Lib3D\Include\IRenderer3D.h"
#include ".\Lib3D\Include\TextModel3D.h"
#include ".\Lib3D\Include\Material.h"
#include ".\Lib3D\Include\RenderArgs.h"

#include <algorithm>
// include OpenGL
#include <GL/glu.h>
#include <GL/gl.h>





using namespace std;

using op::lib3D::Vertex;
using op::lib3D::Triangle;

using op::lib3D::Mesh3D;
using op::lib3D::MeshModel3D;
using op::lib3D::PolylineModel3D;
using op::fplot::FunctionVisualizationEntity;





namespace op { namespace fplot
{
	RootEntity::RootEntity()
	{}


	void RootEntity::Init()
	{
		functionVisualizationEntity.reset(new FunctionVisualizationEntity);
		functionVisualizationEntity->DefineFunction("10*cos( (x^2+y^2) * 0.001 )", "-100 100 1", "-100 100 1");
	}


	op::fplot::FunctionVisualizationEntity* RootEntity::GetFunctionVisualizationEntity(void)
	{
		return functionVisualizationEntity.get();
	}


	void InitGLParams()
	{
		static bool first = true;

		if (first)
		{
			GLfloat light_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
			GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
			GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

			GLfloat light_diffuse1[] = { 0.6f, 0.6f, 0.6f, 0.6f };
			GLfloat light_specular1[] = { 0.6f, 0.6f, 0.6f, 0.6f };

			GLfloat light_position0[] = { -1.0f, -1.0f, -1.0f, 0.0f };
			GLfloat light_position1[] = {  0.0f,  0.0f, -1.0f, 0.0f };
			//GLfloat light_position2[] = {  1.0f,  1.0f, -1.0f, 0.0f };
			//GLfloat light_position3[] = {  1.0f, -1.0f, -1.0f, 0.0f };

			glClearColor(0.0, 0.0, 0.0, 0.0);
			glShadeModel(GL_SMOOTH);


			glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
			glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
			glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
			glLightfv(GL_LIGHT0, GL_POSITION, light_position0);

			glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
			glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse1);
			glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular1);
			glLightfv(GL_LIGHT1, GL_POSITION, light_position1);

			//glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient);
			//glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse);
			//glLightfv(GL_LIGHT2, GL_SPECULAR, light_specular);
			//glLightfv(GL_LIGHT2, GL_POSITION, light_position2);

			//glLightfv(GL_LIGHT3, GL_AMBIENT, light_ambient);
			//glLightfv(GL_LIGHT3, GL_DIFFUSE, light_diffuse);
			//glLightfv(GL_LIGHT3, GL_SPECULAR, light_specular);
			//glLightfv(GL_LIGHT3, GL_POSITION, light_position3);

			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
			glEnable(GL_LIGHT1);
			//glEnable(GL_LIGHT2);
			//glEnable(GL_LIGHT3);

			//GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
			//GLfloat mat_shininess[] = { 50.0 };
			//glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
			//glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);

			glEnable(GL_DEPTH_TEST);
			first = false;
		}
	}


	void RootEntity::Render(op::lib3D::IRenderer3D& renderer, const op::lib3D::RenderArgs& args)
	{
		InitGLParams();

		float material[4] = { 0.0, 0.0, 1.0, 0.0 };

		::glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material);
		::glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material);
		::glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material);
		::glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


		op::lib3D::Engine3D::GetInstance()->GetSceneManager()->GetRootNode()->Render(renderer, args);
			
		this->DrawReferenceBox(renderer, args);

		this->functionVisualizationEntity->Render(renderer, args);
	}


	void RootEntity::DrawReferenceBox(op::lib3D::IRenderer3D& renderer, const op::lib3D::RenderArgs& args)
	{
		if (args.renderPass == op::lib3D::RenderPass::FOREGROUND)
		{
			renderer.SetColorMaterial(op::lib3D::Color(1.0f, 1.0f, 1.0f, 0.5f));

			renderer.DrawLine(op::math::vector3f(-100, -100, -100), op::math::vector3f(100, -100, -100));
			renderer.DrawLine(op::math::vector3f(100, -100, -100), op::math::vector3f(100, 100, -100));
			renderer.DrawLine(op::math::vector3f(100, 100, -100), op::math::vector3f(-100, 100, -100));
			renderer.DrawLine(op::math::vector3f(-100, 100, -100), op::math::vector3f(-100, -100, -100));

			renderer.DrawLine(op::math::vector3f(-100, -100, 100), op::math::vector3f(100, -100, 100));
			renderer.DrawLine(op::math::vector3f(100, -100, 100), op::math::vector3f(100, 100, 100));
			renderer.DrawLine(op::math::vector3f(100, 100, 100), op::math::vector3f(-100, 100, 100));
			renderer.DrawLine(op::math::vector3f(-100, 100, 100), op::math::vector3f(-100, -100, 100));

			op::lib3D::BillboardText t1, t2;
			t1.SetPosition(vector3d(-100, -100, -100));
			t1.SetText("(-100,-100,-100)");
			t1.SetFontSize(6);
			t1.Render(renderer, args);

			t2.SetPosition(vector3d(100, 100, 100));
			t2.SetText("(100, 100, 100)");
			t2.SetFontSize(6);
			t2.Render(renderer, args);
		}
	}


	void RootEntity::DrawGrid(void)
	{
		const double minX = -120.0;
		const double maxX =  120.0;
		const double stepX = 10.0;

		const double minY = -150.0;
		const double maxY =  150.0;
		const double stepY = 10.0;

		glDisable(GL_LIGHTING);
		glDisable(GL_COLOR_MATERIAL);
		glEnable(GL_LINE_SMOOTH);
		glDisable(GL_BLEND);
			
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
		glLineWidth(1.0f);
		glColor3f(0.4f, 0.4f, 0.4f);
		glBegin(GL_LINES);
		for (double y = minY; y <= (maxY + 0.1*stepY); y += stepY)
		{
			glVertex3d(minX, y, 0.0);
			glVertex3d(maxX, y, 0.0);
		}

		for (double x = minX; x <= (maxX + 0.1*stepX); x += stepX)
		{
			glVertex3d(x, minY, 0.0);
			glVertex3d(x, maxY, 0.0);
		}
		glEnd();


		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
		glLineWidth(4.0f);
		glBegin(GL_LINES);

		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3d(.0f, .0f, .0f);
		glVertex3d(maxX, .0f, .0f);

		glColor3f( .0f, 1.0f, .0f);
		glVertex3d(.0f, .0f, .0f);
		glVertex3d(.0f, maxY, .0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3d(.0f, .0f, .0f);
		glVertex3d(.0f, .0f, maxY);
		glEnd();

	}
}}

