#ifndef __AXISMODEL3D_H__
#define __AXISMODEL3D_H__


#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include ".\Lib3D\Include\Material.h"
#include ".\Lib3D\Include\Vector.h"
#include ".\Lib3D\Include\IRenderable.h"


using namespace std;
using namespace op::lib3D;

using op::math::vector3d;

namespace op { namespace lib3D
{
	class MeshModel3D;
	class PolylineModel3D;
	class Mesh3D;
	class IRenderer3D;
}}

namespace op { namespace fplot
{
	//! A class responsible for rendering the 3d-axis XYZ
	/*!
	*  Detailed description...
	*/
	class AxisModel3D : public op::lib3D::IRenderable
	{
	public:

		AxisModel3D();

		virtual ~AxisModel3D();

	public:

		void SetExtents(const vector3d& min, const vector3d& max);

		void SetMajorIntervals(unsigned int intervalCount);

		void SetMinorIntervals(unsigned int intervalCount);

		virtual void Render(op::lib3D::IRenderer3D&, const op::lib3D::RenderArgs&) override;

	private:

		void UpdateRendering(void);

	private:
		vector3d _min;
		vector3d _max;
		unsigned int _majorIntervals = 5;
		unsigned int _minorIntervals = 5;
			
		std::vector<unique_ptr<PolylineModel3D>> _xAxisCurves;
		std::vector<unique_ptr<PolylineModel3D>> _yAxisCurves;
		std::vector<unique_ptr<PolylineModel3D>> _zAxisCurves;

		static const Material _axisMaterial;
	};
}}


#endif //__FUNCTIONMODEL3D_H