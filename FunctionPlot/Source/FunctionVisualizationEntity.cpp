#include "stdafx.h"

#include "FunctionVisualizationEntity.h"
#include "FunctionPlotApp.h"
#include "RootEntity.h"
#include "IEvaluatorFactory.h"
#include "IFunctionEvaluator.h"
#include "PythonFunctionEvaluator.h"
#include "FunctionModel3D.h"
#include "AxisModel3D.h"

#include ".\Lib3D\Include\Engine3D.h"
#include ".\Lib3D\Include\SceneManager.h"
#include ".\Lib3D\Include\SceneNode.h"
#include ".\Lib3D\Include\IRenderer3D.h"
#include ".\Lib3D\Include\Mesh3D.h"
#include ".\Lib3D\Include\MeshModel3D.h"

#include ".\Lib3D\GLM\glm\glm.hpp"
#include ".\Lib3D\GLM\glm\vec3.hpp"
#include ".\Lib3D\GLM\glm\mat4x4.hpp"
#include ".\Lib3D\GLM\glm\gtc/matrix_transform.hpp"

#include <functional>

using op::lib3D::Mesh3D;


namespace op { namespace fplot {

	FunctionVisualizationEntity::FunctionVisualizationEntity()
	{
		_evaluatorFactory = make_unique<PythonEvaluatorFactory>();
		_axisModel3D = make_unique<AxisModel3D>();
	}


	FunctionVisualizationEntity::~FunctionVisualizationEntity()
	{
	
	}

	
	void FunctionVisualizationEntity::DefineFunction(const std::string& functionFormula, const std::string& sampleX, const std::string& sampleY)
	{
		auto evaluator = _evaluatorFactory->CreateEvaluator();
		if (evaluator)
		{
			if (!_model3D)
			{
				_model3D = std::make_unique<FunctionModel3D>(std::move(evaluator));
			}
			_model3D->SetFunction(functionFormula, op::fplot::FunctionEvaluationParams(sampleX, sampleY));
		}
	}


	void FunctionVisualizationEntity::CreateMeshFromFunction(const std::string& functionFormula, const std::string& sampleX, const std::string& sampleY)
	{
		// TODO: Create a mesh object and store it in the list of geometries
		// (assuming at some point there will be a list of geometries along
		//  the list of functions).
	}


	void FunctionVisualizationEntity::Render(op::lib3D::IRenderer3D& renderer, const op::lib3D::RenderArgs& args)
	{
		if (_model3D)
		{
			_model3D->Render(renderer, args);
		}

		if (_axisModel3D)
		{
			_axisModel3D->Render(renderer, args);
		}


	}
}}