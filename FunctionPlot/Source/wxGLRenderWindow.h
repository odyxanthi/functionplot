﻿#ifndef __WXGLRenderWindow_H__
#define __WXGLRenderWindow_H__

#include "stdafx.h"
#include <memory>
#include <list>
#include "gl/glew.h"
#include <wx/glcanvas.h>
#include ".\Lib3D\GLM\glm\mat4x4.hpp"

#include ".\Lib3D\Include\Camera.h"
#include ".\Lib3D\Include\CameraMan.h"
#include ".\Lib3D\Include\IRenderable.h"
#include ".\Lib3D\Include\IView3D.h"

#include ".\Lib3D\Include\Keyboard.h"
#include ".\Lib3D\Include\Mouse.h"

#include "Core/Include/sigslot.h"

using std::list;
using std::unique_ptr;
using std::shared_ptr;

namespace op { namespace lib3D 
{
	class Camera;
	class IRenderer3D;
	struct RenderArgs;
}}

namespace op
{
	namespace IO
	{
		class MouseEvtArgs;
		class KeyboardEvtArgs;
	}
}

using op::lib3D::Camera;
using op::lib3D::CameraMan;


class WxMouse : public op::lib3D::Mouse
{
public:

	WxMouse(){};

	virtual ~WxMouse(){};

private:

	void OnMouseMove(op::lib3D::MouseEvtArgs& args)
	{
		this->MouseMove(args);
	}

	void OnMouseClick(op::lib3D::MouseEvtArgs& args)
	{
		this->MouseClick(args);
	}


	friend class wxGLRenderWindow;
};



class WxKeyboard : public op::lib3D::Keyboard
{
public:

	WxKeyboard(){};

	virtual ~WxKeyboard(){};

private:

	void OnKeyPressed(op::lib3D::KeyboardEvtArgs& args)
	{
		this->KeyPressedEvt(args);
	}

	friend class wxGLRenderWindow;
};



class wxGLRenderWindow :	public wxGLCanvas,
							public op::lib3D::IView3D
{
public:
	wxGLRenderWindow(wxFrame* parent, int* args);
	virtual ~wxGLRenderWindow();

	void resized(wxSizeEvent& evt);

	void Render();
	//void prepare3DViewport(int topleft_x, int topleft_y, int bottomrigth_x, int bottomrigth_y);
	void prepare3DViewport_shaders(int topleft_x, int topleft_y, int bottomrigth_x, int bottomrigth_y);

public:

	virtual vector2i GetSize(void) const override;

	virtual unsigned int GetHeight(void) const override;

	virtual unsigned int GetWidth(void) const override;

	virtual op::lib3D::Ray3D Unproject(const vector2i pos) const override;

	virtual const op::lib3D::Camera& GetCamera() const override;

	sigslot::signal2<op::lib3D::IRenderer3D&, const op::lib3D::RenderArgs&, single_threaded> RenderEvt;

protected:

	void OnRenderTick(wxTimerEvent& evt);
	void OnPaint(wxPaintEvent& evt);

	// events
	void mouseMoved(wxMouseEvent& event);
	void mouseDown(wxMouseEvent& event);
	void mouseWheel(wxMouseEvent& event);
	void mouseReleased(wxMouseEvent& event);
	void rightClick(wxMouseEvent& event);
	void mouseLeftWindow(wxMouseEvent& event);
	void keyPressed(wxKeyEvent& event);
	void keyReleased(wxKeyEvent& event);

protected:

	void init(void);
	void display(void);

	glm::mat4 GetProjectionMatrix(void) const;

	glm::mat4 GetCameraMatrix(void) const;

	glm::mat4 GetModelMatrix(void) const;

	glm::mat4 GetModelViewMatrix(void) const;

private:

	wxTimer*		m_timer;
	WxMouse			m_mouse;
	WxKeyboard		m_keyboard;
	Camera			m_camera;
	CameraMan		m_cameraMan;

private:

	static wxGLContext*	m_context;

private:
	DECLARE_EVENT_TABLE()
};







#endif