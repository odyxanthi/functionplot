#include "stdafx.h"

#include "View3DFrame.h"

//#include "Ogre.h"

BEGIN_EVENT_TABLE(View3DFrame, wxDocChildFrame)
EVT_CLOSE(View3DFrame::OnClose)
EVT_SIZE(View3DFrame::OnResize)
EVT_PAINT(View3DFrame::OnPaint)
EVT_MOUSE_EVENTS(View3DFrame::OnMouseEvent)
END_EVENT_TABLE()



unsigned int View3DFrame::framesCreated = 0;



View3DFrame::View3DFrame(
		wxFrame* parent,
		wxWindowID id,
		const wxString& title)
	:	wxFrame ( parent, id, title)
{
	//this->ogreCanvas = new wxOgreRenderWindow(Ogre::Root::getSingletonPtr(), this, static_cast<wxWindowID>(++View3DFrame::framesCreated), wxPoint(10, 10), wxSize(320, 240));

	this->SendSizeEvent();
}


View3DFrame::~View3DFrame()
{

}



void View3DFrame::Redraw()
{
	
}


void View3DFrame::OnResize(wxSizeEvent& evt)
{
	//this->ogreCanvas->SetPosition(wxPoint(15, 15));
	//wxRect rect = this->GetClientRect();
	//this->ogreCanvas->SetSize(wxSize(rect.width - 30, rect.height - 30));

	//this->ogreCanvas->Update();
}



void View3DFrame::OnPaint(wxPaintEvent& evt)
{
	//this->ogreCanvas->Update();
}


void View3DFrame::OnClose(wxCloseEvent& closeEvt)
{
}


void View3DFrame::OnMouseEvent(wxMouseEvent& event)
{
}


