#include "stdafx.h"
#include "FunctionDefinitionCtrl.h"
#include "FunctionPlotApp.h"
#include "RootEntity.h"
#include "FunctionVisualizationEntity.h"
#include "Logger.h"

#include ".\Lib3D\Include\Engine3D.h"

#include <tuple>

using namespace std;


namespace op { namespace fplot
{

	enum
	{
		APPLY_BUTTON_ID = 1,
		MESH_BUTTON_ID,
		PLOT_BUTTON_ID,
		CLEAR_BUTTON_ID
	};


	wxBEGIN_EVENT_TABLE(FunctionDefinitionCtrl, wxPanel)
		EVT_BUTTON(APPLY_BUTTON_ID, FunctionDefinitionCtrl::OnApplyButtonClick)
		EVT_BUTTON(CLEAR_BUTTON_ID, FunctionDefinitionCtrl::OnClearButtonClick)
		EVT_BUTTON(MESH_BUTTON_ID, FunctionDefinitionCtrl::OnMeshButtonClick)
	wxEND_EVENT_TABLE()


	FunctionDefinitionCtrl::FunctionDefinitionCtrl (wxWindow* parent, wxWindowID id, const wxPoint &pos, const wxSize &size)
		:
		wxPanel(parent, id, pos, size)
	{
		this->Initialize();
	}




	void FunctionDefinitionCtrl::Initialize(void)
	{
		const int border = 2;

		wxBoxSizer* topsizer = new wxBoxSizer(wxVERTICAL);

		wxStaticText* fxyLabel = new wxStaticText(this, wxID_ANY, wxT("f(x,y) = "), wxDefaultPosition, wxSize(60, 30), 0);
		topsizer->Add(fxyLabel, wxSizerFlags(0).Align(0).Expand().Border(wxALL, border));

		this->functionFormulaTxtCtrl = new wxTextCtrl (this, wxID_ANY, "10 * sin ( (x^2 + y^2 ) / 300 )", wxDefaultPosition, wxSize(120, 60), wxTE_MULTILINE );
		topsizer->Add(this->functionFormulaTxtCtrl, wxSizerFlags(1).Align(0).Expand().Border(wxALL, border));

		const int textBoxHeight = 25;

		// x params
		{
			wxBoxSizer* xParamsSizer = new wxBoxSizer(wxHORIZONTAL);
			wxStaticText* xLabel = new wxStaticText(this, wxID_ANY, wxT("  < x <"), wxDefaultPosition, wxSize(40, textBoxHeight), 0);
			this->xMinText = new wxTextCtrl(this, wxID_ANY, "-100", wxDefaultPosition, wxSize(60, textBoxHeight));
			this->xMaxText = new wxTextCtrl(this, wxID_ANY, "100", wxDefaultPosition, wxSize(60, textBoxHeight));
			xParamsSizer->Add(xMinText, wxSizerFlags(0).Align(0).Border(wxALL, border));
			xParamsSizer->Add(xLabel, wxSizerFlags(0).Align(0).Border(wxALL, border));
			xParamsSizer->Add(xMaxText, wxSizerFlags(0).Align(0).Border(wxALL, border));
			topsizer->Add(xParamsSizer, wxSizerFlags(0).Border(wxALL, border).Center());
		}

		// y params
		{
			wxBoxSizer* yParamSizer = new wxBoxSizer(wxHORIZONTAL);
			wxStaticText* yLabel = new wxStaticText(this, wxID_ANY, wxT("  < y <"), wxDefaultPosition, wxSize(40, textBoxHeight), 0);
			this->yMinText = new wxTextCtrl(this, wxID_ANY, "-100", wxDefaultPosition, wxSize(60, textBoxHeight));
			this->yMaxText = new wxTextCtrl(this, wxID_ANY, "100", wxDefaultPosition, wxSize(60, textBoxHeight));
			yParamSizer->Add(this->yMinText, wxSizerFlags(0).Align(0).Border(wxALL, border));
			yParamSizer->Add(yLabel, wxSizerFlags(0).Align(0).Border(wxALL, border));
			yParamSizer->Add(this->yMaxText, wxSizerFlags(0).Align(0).Border(wxALL, border));
			topsizer->Add(yParamSizer, wxSizerFlags(0).Border(wxALL, border).Center());
		}

		// x sample distance
		{
			wxBoxSizer* xSampleDistanceSizer = new wxBoxSizer(wxHORIZONTAL);
			wxStaticText* xSampleDistanceLabel = new wxStaticText(this, wxID_ANY, wxT("X-axis sample distance:"), wxDefaultPosition, wxSize(120, 40), 0);
			this->xSampleDistanceText = new wxTextCtrl(this, wxID_ANY, "1.0", wxDefaultPosition, wxSize(30, textBoxHeight));
			xSampleDistanceSizer->Add(xSampleDistanceLabel, wxSizerFlags(0).Align(0).Border(wxALL, border));
			xSampleDistanceSizer->Add(this->xSampleDistanceText, wxSizerFlags(0).Align(0).Border(wxALL, border).Center());
			topsizer->Add(xSampleDistanceSizer, wxSizerFlags(0).Border(wxALL, border).Center());
		}

		// y sample distance
		{
			wxBoxSizer* ySampleDistanceSizer = new wxBoxSizer(wxHORIZONTAL);
			wxStaticText* ySampleDistanceLabel = new wxStaticText(this, wxID_ANY, wxT("Y-axis sample distance:"), wxDefaultPosition, wxSize(120, 40), 0);
			this->ySampleDistanceText = new wxTextCtrl(this, wxID_ANY, "1.0", wxDefaultPosition, wxSize(30, textBoxHeight));
			ySampleDistanceSizer->Add(ySampleDistanceLabel, wxSizerFlags(0).Align(0).Border(wxALL, border));
			ySampleDistanceSizer->Add(this->ySampleDistanceText, wxSizerFlags(0).Align(0).Border(wxALL, border).Center());
			topsizer->Add(ySampleDistanceSizer, wxSizerFlags(0).Border(wxALL, border).Center());
		}



		// plot and clear buttons
		wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);
		wxButton* clearButton = new wxButton(this, CLEAR_BUTTON_ID, "Clear");
		buttonSizer->Add(clearButton, wxSizerFlags(0).Align(0).Border(wxALL, border));
		wxButton* plotButton = new wxButton(this, APPLY_BUTTON_ID, "Plot");
		buttonSizer->Add(plotButton, wxSizerFlags(0).Align(0).Border(wxALL, border));
		wxButton* meshButton = new wxButton(this, MESH_BUTTON_ID, "Mesh");
		buttonSizer->Add(meshButton, wxSizerFlags(0).Align(0).Border(wxALL, border));
		topsizer->Add(buttonSizer, wxSizerFlags(0).Center() );

		this->SetSizerAndFit(topsizer);
	}


	void FunctionDefinitionCtrl::GetParameters(std::string& formula, std::string& xDomain, std::string& yDomain)
	{
		formula = this->functionFormulaTxtCtrl->GetValue();

		xDomain = this->xMinText->GetValue() + " " + this->xMaxText->GetValue() + " " + this->xSampleDistanceText->GetValue();
		yDomain = this->yMinText->GetValue() + " " + this->yMaxText->GetValue() + " " + this->ySampleDistanceText->GetValue();
	}


	void FunctionDefinitionCtrl::OnApplyButtonClick(wxCommandEvent& evt)
	{
		op::lib3D::Engine3D::GetInstance()->GetLogger()->WriteLine("FunctionDefinitionCtrl::OnApplyButtonClick");


		std::string formula, xDomain, yDomain;
		this->GetParameters( formula, xDomain, yDomain);

		try
		{
			op::fplot::FunctionPlotApp::GetApp()->GetRootEntity()->GetFunctionVisualizationEntity()->DefineFunction(formula, xDomain, yDomain);
		}
		catch (std::exception& e)
		{
			op::fplot::FunctionPlotApp::GetApp()->GetEngine3D()->GetLogger()->WriteLine(e.what());
		}
	}



	void FunctionDefinitionCtrl::OnMeshButtonClick(wxCommandEvent& evt)
	{
		op::lib3D::Engine3D::GetInstance()->GetLogger()->WriteLine("FunctionDefinitionCtrl::OnMeshButtonClick");

		std::string formula, xDomain, yDomain;
		this->GetParameters(formula, xDomain, yDomain);

		try
		{
			op::fplot::FunctionPlotApp::GetApp()->GetRootEntity()->GetFunctionVisualizationEntity()->CreateMeshFromFunction(formula, xDomain, yDomain);
		}
		catch (std::exception& e)
		{
			op::lib3D::Engine3D::GetInstance()->GetLogger()->WriteLine(e.what());
		}
	}

	

	void FunctionDefinitionCtrl::OnClearButtonClick(wxCommandEvent& evt)
	{
		this->functionFormulaTxtCtrl->Clear();
	}
}}
