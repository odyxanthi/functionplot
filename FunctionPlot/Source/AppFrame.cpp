#include "stdafx.h"

#include "AppFrame.h"
#include "View3DFrame.h"
#include "wxGLRenderWindow.h"
#include "FunctionPlotApp.h"
#include "RootEntity.h"
#include "FunctionVisualizationEntity.h"
#include "FunctionDefinitionCtrl.h"
#include "Logger.h"


namespace op { namespace fplot
{
	enum
	{
		ID_NEW_WINDOW_3D = 1,
		ID_FUNCTION_PARAMS,
		ID_DELETE_ALL,
		ID_EXIT
	};



	wxBEGIN_EVENT_TABLE(AppFrame, wxFrame)

		EVT_MENU(ID_NEW_WINDOW_3D, AppFrame::OnNew3DWindow)
		EVT_MENU(ID_FUNCTION_PARAMS, AppFrame::OnEditFunctionParams)
		EVT_MENU(ID_DELETE_ALL, AppFrame::OnDeleteAllGeometries)


		EVT_MENU(wxID_EXIT, AppFrame::OnExit)
		EVT_MENU(wxID_ABOUT, AppFrame::OnAbout)

		EVT_SIZE(AppFrame::OnResize)

	wxEND_EVENT_TABLE()






	AppFrame::AppFrame(
		FunctionPlotApp& app,
		wxWindow*	parent,
		wxWindowID	id,
		const wxString & title,
		const wxPoint& pos,
		const wxSize& size,
		long style,
		const wxString&	name)
	: wxFrame(parent, id, title, pos, size, style, name)
	, m_app(app)
	{
			this->SetIcon(wxICON(frame_icon));

			wxMenu *fileMenu = new wxMenu;
			fileMenu->Append(ID_NEW_WINDOW_3D, "New 3D Window", "Help string shown in status bar for this menu item");
			fileMenu->AppendSeparator();
			fileMenu->Append(wxID_EXIT);

			wxMenu *editMenu = new wxMenu;
			editMenu->Append(ID_DELETE_ALL, "Delete all geometries", "");
			editMenu->Append(ID_FUNCTION_PARAMS, "Define function", "");



			wxMenu *menuHelp = new wxMenu;
			menuHelp->Append(wxID_ABOUT);


			wxMenuBar *menuBar = new wxMenuBar;
			menuBar->Append(fileMenu, "&File");
			menuBar->Append(editMenu, "&Edit");
			menuBar->Append(menuHelp, "&Help");
			
			CreateStatusBar();
			SetMenuBar(menuBar);
			SetStatusText("Welcome to FunctionPlot!");

			//this->m_bookCtrl = new wxNotebook(this, wxID_ANY);
			this->m_bookCtrl = new wxAuiNotebook(this, wxID_ANY);

			this->m_mgr.SetManagedWindow(this);

			wxAuiPaneInfo paneInfo;
			paneInfo.Dockable(true);
			paneInfo.CenterPane();
			paneInfo.Floatable(true);
			paneInfo.GripperTop(true);
			paneInfo.DestroyOnClose(false);

			this->m_mgr.AddPane(this->m_bookCtrl, paneInfo);
			this->m_mgr.Update();

			//this->CreateNew3DWindow();
			//this->m_mgr.Update();
		}



	AppFrame::~AppFrame()
	{
		this->m_mgr.UnInit();

		delete this->m_bookCtrl;
	}


	void AppFrame::AddTabbedWindow(std::unique_ptr<wxWindow> wnd)
	{
		try
		{
			wxSize clientSize = this->m_bookCtrl->GetClientSize();
			this->m_bookCtrl->AddPage(wnd.get(), "3D View");
			wnd.release();
		}
		catch (std::exception& e)
		{
			FunctionPlotApp::GetApp()->GetLogger()->WriteLine(e.what());
			wxMessageBox("Failed to add tabbed window.");
		}
	}


	void AppFrame::OnExit(wxCommandEvent& event)
	{
		Close(true);
	}



	void AppFrame::OnAbout(wxCommandEvent& event)
	{
		wxMessageBox("This is FunctionPlot app.",
			"About FunctionPlot", wxOK | wxICON_INFORMATION);
	}


	void AppFrame::OnNew3DWindow(wxCommandEvent& event)
	{
		this->AddTabbedWindow(m_app.CreateRenderWindow());
	}


	void AppFrame::OnEditFunctionParams(wxCommandEvent& event)
	{
		FunctionDefinitionCtrl* funcDefinitionCtrl = new FunctionDefinitionCtrl(this, wxID_ANY, wxPoint(0, 0), wxSize(200, 200));

		wxAuiPaneInfo paneInfo;
		paneInfo.Dockable(true);
		paneInfo.Floatable(true);
		paneInfo.LeftDockable(true);
		paneInfo.RightDockable(true);
		paneInfo.BottomDockable(true);
		paneInfo.TopDockable(false);
		paneInfo.Float();
		paneInfo.Caption(wxT("Plot function"));
		m_mgr.AddPane(funcDefinitionCtrl, paneInfo);

		//m_mgr.AddPane(text1, wxLEFT, wxT("Function params"));

		m_mgr.Update();
	}


	void AppFrame::OnDeleteAllGeometries(wxCommandEvent& event)
	{
		//FunctionPlotApp::GetApp()->GetRootEntity()->DeleteAllGeometries();
	}


	void AppFrame::OnResize(wxSizeEvent& evt)
	{
		if (this->m_bookCtrl != nullptr)
			this->m_bookCtrl->SetSize(this->GetClientSize());
	}
}}