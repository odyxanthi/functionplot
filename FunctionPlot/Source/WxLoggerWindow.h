#ifndef __WXLOGGERWINDOW_H__
#define __WXLOGGERWINDOW_H__

#include "stdafx.h"
#include <memory>



namespace op { namespace gui
{
	class WxLoggerWindow : public wxPanel
							//, public ILoggerWindow
	{
	public:

		WxLoggerWindow(wxWindow* parent = nullptr,
			wxWindowID id = wxID_ANY,
			const wxPoint &pos = wxDefaultPosition,
			const wxSize &size = wxDefaultSize);


		virtual ~WxLoggerWindow();

	public:

		virtual void WriteLine(const std::string& text);

		virtual void Write(const std::string& text);

		virtual void Clear(void);

	private:
		wxDECLARE_EVENT_TABLE();

		void	OnClearButtonClick(wxCommandEvent& evt);

	private:


		void Initialize(void);

	private:

		std::unique_ptr<wxTextCtrl> outputWindow;
		std::unique_ptr<wxBoxSizer> sizer;
	};
}}

#endif