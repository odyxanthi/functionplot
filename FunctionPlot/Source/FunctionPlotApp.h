#ifndef __FUNCTIONPLOTAPP_H__
#define __FUNCTIONPLOTAPP_H__

#include "stdafx.h"
#include <memory>


class wxFrame;
class wxGLRenderWindow;

namespace core
{
	class Logger;
}


namespace op { namespace lib3D
{
	class Engine3D;
}}


namespace op { namespace fplot 
{

	class RootEntity;


	class FunctionPlotApp : public wxApp
	{
	public:

		virtual bool OnInit();

		virtual ~FunctionPlotApp();

	public:

		RootEntity* GetRootEntity(void);

		op::lib3D::Engine3D* GetEngine3D(void);

		::core::Logger* GetLogger(void);

		std::unique_ptr<wxGLRenderWindow> CreateRenderWindow(void);

	public:

		static FunctionPlotApp* GetApp(void){ return theApp; }

	private:

		wxFrame* appFrame;
		std::unique_ptr<RootEntity> rootEntity;

		static FunctionPlotApp* theApp;
	};

}}

#endif