#include "stdafx.h"
#include "WxLoggerWindow.h"

using namespace std;


namespace op { namespace gui
{

	enum
	{
		CLEAR_BUTTON_ID = 1
	};


	wxBEGIN_EVENT_TABLE(WxLoggerWindow, wxPanel)
		EVT_BUTTON(CLEAR_BUTTON_ID, WxLoggerWindow::OnClearButtonClick)
	wxEND_EVENT_TABLE()


	WxLoggerWindow::WxLoggerWindow (wxWindow* parent, wxWindowID id, const wxPoint &pos, const wxSize &size)
		:
		wxPanel(parent, id, pos, size)
	{
		this->Initialize();
	}


	WxLoggerWindow::~WxLoggerWindow()
	{

	}


	void WxLoggerWindow::Initialize(void)
	{
		const int border = 2;

		this->sizer = unique_ptr<wxBoxSizer>(new wxBoxSizer(wxVERTICAL));

		this->outputWindow = unique_ptr<wxTextCtrl>( new wxTextCtrl (this, wxID_ANY, "", wxDefaultPosition, wxSize(60, 120), wxTE_MULTILINE ));
		this->sizer->Add(this->outputWindow.get(), wxSizerFlags(1).Align(0).Expand().Border(wxALL, border));

		this->SetSizerAndFit(this->sizer.get());
		this->sizer.release();
	}


	void WxLoggerWindow::WriteLine(const std::string& text)
	{
		*(this->outputWindow) << ">" << wxString(text) << "\n";
	}


	void WxLoggerWindow::Write(const std::string& text)
	{
		*(this->outputWindow) << ">" << wxString(text);
	}


	void WxLoggerWindow::Clear(void)
	{
		this->outputWindow->Clear();
	}


	void WxLoggerWindow::OnClearButtonClick(wxCommandEvent& evt)
	{

	}
}}
