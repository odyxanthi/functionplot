#ifndef __IEVALUATORFACTORY_H__
#define __IEVALUATORFACTORY_H__

#include <memory>

using namespace std;

namespace op { namespace fplot
{
	class IFunctionEvaluator;

	class IEvaluatorFactory
	{
	public:

		virtual ~IEvaluatorFactory(){};

		virtual unique_ptr<IFunctionEvaluator> CreateEvaluator() = 0;

	};
}}


#endif