#ifndef __FUNCTIONMODEL3D_H__
#define __FUNCTIONMODEL3D_H__


#include <memory>
#include <tuple>
#include <string>

#include ".\Lib3D\Include\Material.h"
#include ".\Lib3D\Include\Vector.h"
#include ".\Lib3D\Include\IRenderable.h"
#include ".\Lib3D\GLM\glm\mat4x4.hpp"

using namespace op::lib3D;

using op::math::vector3d;

namespace op { namespace lib3D
{
	class MeshModel3D;
	class PolylineModel3D;
	class Mesh3D;
	class TextModel3D;
	class BillboardText;
	class IRenderer3D;
}}


namespace op { namespace fplot
{
	class IFunctionEvaluator;
		

	struct FunctionEvaluationParams
	{
		FunctionEvaluationParams()
			:
			minx(-100),
			maxx(100),
			miny(-100),
			maxy(100),
			sampleDistX(1.0),
			sampleDistY(1.0)
		{}


		FunctionEvaluationParams(double minX, double maxX, double sampleDistanceX, double minY, double maxY, double sampleDistanceY)
			:
			minx(minX),
			maxx(maxX),
			miny(minY),
			maxy(maxY),
			sampleDistX(sampleDistanceX),
			sampleDistY(sampleDistanceX)
		{}


		FunctionEvaluationParams(const std::string& xparams, const std::string& yparams);


		double minx;
		double maxx;
		double miny;
		double maxy;
		double sampleDistX;
		double sampleDistY;
	};


	//! A class responsible for rendering a plot of a two variable function.
	/*!
	 *  Detailed description...
	 */
	class FunctionModel3D : public op::lib3D::IRenderable
	{
	public:
			
		FunctionModel3D(std::unique_ptr<IFunctionEvaluator> evaluator);
			
		virtual ~FunctionModel3D();

		void SetFunction(const std::string& functionFormulal, const FunctionEvaluationParams& params);

		virtual void Render(op::lib3D::IRenderer3D&, const op::lib3D::RenderArgs&) override;

		const op::lib3D::Mesh3D* GetRenderMesh(void) const;

		void SetDisplayArea(const vector3d& areaMin, const vector3d& areaMax);
		
	private:

		void Initialize();

		void UpdateFormulaDependentProperties(void);

		void UpdateDisplayAreaDependentProperties(void);

		void UpdateRenderMesh(void);

		void UpdateXYIsoCurves(void);

		void UpdateViewTransform(void);

		//! dir=0 => sample parallel to x, dir=1 => sample parallel to y
		std::vector<vector3d> GetIsoCurveSamplePoints(int dir, double isoParam);


	private:
		std::unique_ptr<IFunctionEvaluator> _evaluator;
		std::string _functionFormula;
		FunctionEvaluationParams _evaluationParams;

		std::unique_ptr<op::lib3D::MeshModel3D> _meshModel;
		std::unique_ptr<op::lib3D::BillboardText> _textModel;
		std::vector<std::unique_ptr<PolylineModel3D>> _xyCurves;

		vector3d _displayAreaMin = vector3d(0,0,0);
		vector3d _displayAreaMax = vector3d(0,0,0);
		using mat4d = glm::tmat4x4<double>;
		mat4d _viewTransform;


		const unsigned int _minIsoCurveCount = 20;
		const double _maxIsoCurveDistance = 20.0;

		static const Material _surfaceMaterial;
		static const Material _isoCurveMaterial;
	};


	bool GetFunctionMinMax(const FunctionModel3D& model, vector3d& min, vector3d& max);

}}


#endif //__FUNCTIONMODEL3D_H