#include "stdafx.h"
#include "AxisModel3D.h"
#include "IFunctionEvaluator.h"
#include "NotImplementedException.h"
#include ".\Lib3D\Include\GeometryUtils.h"
#include ".\Lib3D\Include\MeshModel3D.h"
#include ".\Lib3D\Include\PolylineModel3D.h"
#include ".\Lib3D\Include\Mesh3D.h"
#include ".\Lib3D\Include\IRenderer3D.h"
#include ".\Lib3D\Include\StockMaterials.h"
#include <sstream>
#include <numeric>



using std::unique_ptr;


namespace op { namespace fplot
{
	const Material AxisModel3D::_axisMaterial = op::lib3D::Material(Color(0.25f, 0.25f, 0.25f), Color(0.8f, 0.0f, 1.0f), Color(0.1f, 0.1f, 0.1f), Color(0.0f, 0.0f, 0.0f));


	AxisModel3D::AxisModel3D()
	{}


	AxisModel3D::~AxisModel3D()
	{}


	void AxisModel3D::SetExtents(const vector3d& min, const vector3d& max)
	{
		_min = min;
		_max = max;

		this->UpdateRendering();
	}

	void AxisModel3D::SetMajorIntervals(unsigned int intervalCount)
	{
		_majorIntervals = intervalCount;

		this->UpdateRendering();
	}

	void AxisModel3D::SetMinorIntervals(unsigned int intervalCount)
	{
		_minorIntervals = intervalCount;

		this->UpdateRendering();
	}

	void AxisModel3D::Render(op::lib3D::IRenderer3D& renderer, const op::lib3D::RenderArgs& args)
	{
		for (auto& curve : _xAxisCurves)
		{
			curve->Render(renderer, args);
		}

		for (auto& curve : _yAxisCurves)
		{
			curve->Render(renderer, args);
		}

		for (auto& curve : _zAxisCurves)
		{
			curve->Render(renderer, args);
		}
	}


	void AxisModel3D::UpdateRendering(void)
	{
		_xAxisCurves.clear();
		_yAxisCurves.clear();
		_zAxisCurves.clear();

		using PLinePtr = std::unique_ptr<op::lib3D::PolylineModel3D>;

			
		_xAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_min.X(), _min.Y(), _min.Z()), vector3d(_max.X(), _min.Y(), _min.Z()) })));
		_xAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_min.X(), _max.Y(), _min.Z()), vector3d(_max.X(), _max.Y(), _min.Z()) })));
		_xAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_min.X(), _min.Y(), _max.Z()), vector3d(_max.X(), _min.Y(), _max.Z()) })));
		_xAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_min.X(), _max.Y(), _max.Z()), vector3d(_max.X(), _max.Y(), _max.Z()) })));
			
		_yAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_min.X(), _min.Y(), _min.Z()), vector3d(_min.X(), _max.Y(), _min.Z()) })));
		_yAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_max.X(), _min.Y(), _min.Z()), vector3d(_max.X(), _max.Y(), _min.Z()) })));
		_yAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_min.X(), _min.Y(), _max.Z()), vector3d(_min.X(), _max.Y(), _max.Z()) })));
		_yAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_max.X(), _min.Y(), _max.Z()), vector3d(_max.X(), _max.Y(), _max.Z()) })));

		_zAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_min.X(), _min.Y(), _min.Z()), vector3d(_min.X(), _min.Y(), _max.Z()) })));
		_zAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_max.X(), _min.Y(), _min.Z()), vector3d(_max.X(), _min.Y(), _max.Z()) })));
		_zAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_min.X(), _max.Y(), _min.Z()), vector3d(_min.X(), _max.Y(), _max.Z()) })));
		_zAxisCurves.push_back(PLinePtr(new op::lib3D::PolylineModel3D({ vector3d(_max.X(), _max.Y(), _min.Z()), vector3d(_max.X(), _max.Y(), _max.Z()) })));
	}
}}