#include "stdafx.h"
#include <sstream>
#include <algorithm>
#include "wxGLRenderWindow.h"
#include "Utils.h"
#include "Logger.h"
#include "wxWindowMouseEvtArgs.h"
#include "wxWindowKeyboardEvtArgs.h"

#include ".\Lib3D\Include\Vector.h"
#include ".\Lib3D\Include\Camera.h"
#include ".\Lib3D\Include\RendererGL.h"
#include ".\Lib3D\Include\Matrix.h"
#include ".\Lib3D\Include\MatrixUtils.h"
#include ".\Lib3D\Include\Engine3D.h"
#include ".\Lib3D\Include\RenderArgs.h"
#include ".\Lib3D\Include\DeviceIOManager.h"

#include ".\Lib3D\GLM\glm\glm.hpp"
#include ".\Lib3D\GLM\glm\vec3.hpp"
#include ".\Lib3D\GLM\glm\mat4x4.hpp"
#include ".\Lib3D\GLM\glm\gtc/matrix_transform.hpp"

#include "FunctionPlotApp.h"

//#include "wx/wx.h"
//#include "wx/sizer.h"
//#include "wx/glcanvas.h"


// include OpenGL
#ifdef __WXMAC__
#include "OpenGL/glu.h"
#include "OpenGL/gl.h"
#else
#include <GL/glu.h>
#include <GL/gl.h>
#endif


using op::math::Vector3;
using op::lib3D::Camera;


enum { RENDER_TICK };



BEGIN_EVENT_TABLE(wxGLRenderWindow, wxGLCanvas)
EVT_MOTION(wxGLRenderWindow::mouseMoved)
EVT_LEFT_DOWN(wxGLRenderWindow::mouseDown)
EVT_LEFT_UP(wxGLRenderWindow::mouseReleased)
EVT_RIGHT_DOWN(wxGLRenderWindow::mouseDown)
EVT_RIGHT_UP(wxGLRenderWindow::mouseReleased)
EVT_MIDDLE_DOWN(wxGLRenderWindow::mouseDown)
EVT_MIDDLE_UP(wxGLRenderWindow::mouseReleased)
//EVT_RIGHT_DOWN(wxGLRenderWindow::rightClick)
EVT_LEAVE_WINDOW(wxGLRenderWindow::mouseLeftWindow)
EVT_SIZE(wxGLRenderWindow::resized)
EVT_KEY_DOWN(wxGLRenderWindow::keyPressed)
EVT_KEY_UP(wxGLRenderWindow::keyReleased)
EVT_MOUSEWHEEL(wxGLRenderWindow::mouseWheel)
EVT_PAINT(wxGLRenderWindow::OnPaint)
EVT_TIMER(RENDER_TICK, wxGLRenderWindow::OnRenderTick)
END_EVENT_TABLE()

// some useful events to use

void wxGLRenderWindow::mouseReleased(wxMouseEvent& event) {}
void wxGLRenderWindow::mouseLeftWindow(wxMouseEvent& event) {}
void wxGLRenderWindow::keyReleased(wxKeyEvent& event) {}



wxGLContext*	wxGLRenderWindow::m_context = nullptr;





wxGLRenderWindow::wxGLRenderWindow(wxFrame* parent, int* args) :
	wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
{
	this->init();

	m_camera.SetViewParams(vector3d(300, 300, 300), vector3d(0, 0, 0), vector3d(-1,-1,1));
	this->m_cameraMan.SetCamera(&m_camera);

	
	op::lib3D::Engine3D::GetInstance()->GetDeviceManager()->TrackMouse( this->m_mouse );
	op::lib3D::Engine3D::GetInstance()->GetDeviceManager()->TrackKeyboard(this->m_keyboard);

	// To avoid flashing on MSW
	SetBackgroundStyle(wxBG_STYLE_CUSTOM);

	m_timer = new wxTimer(this, RENDER_TICK);
	m_timer->Start(5);
}


wxGLRenderWindow::~wxGLRenderWindow()
{
	delete m_timer;
}




void wxGLRenderWindow::init(void)
{
	if (m_context == nullptr)
	{
		m_context = new wxGLContext(this);
		wxGLCanvas::SetCurrent(*m_context);
		glewInit();
		
		auto renderer = op::lib3D::Engine3D::GetInstance()->GetRenderer();
		renderer->InitializeShaders();
	}
}


void wxGLRenderWindow::resized(wxSizeEvent& evt)
{
	glViewport(0, 0, (GLsizei)evt.GetSize().GetWidth(), (GLsizei)evt.GetSize().GetHeight() );
}


template <typename T>
glm::tvec3<T> GetGLMVector(const op::math::Vector3<T>& v)
{
	return glm::tvec3<T>(v.X(), v.Y(), v.Z());
}


/** Inits the OpenGL viewport for drawing in 3D. */
void wxGLRenderWindow::prepare3DViewport_shaders(int topleft_x, int topleft_y, int bottomrigth_x, int bottomrigth_y)
{
	glClearColor(0.3f, 0.4f, 1.0f, 1.0f); // Black Background
	glClearDepth(1.0f);	// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST); // Enables Depth Testing
	glDepthFunc(GL_LEQUAL); // The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//glEnable(GL_COLOR_MATERIAL);

	glViewport(topleft_x, topleft_y, bottomrigth_x - topleft_x, bottomrigth_y - topleft_y);

	glm::mat4 projectionMatrix = this->GetProjectionMatrix();
	glm::mat4 cameraMatrix = this->GetCameraMatrix();

	// modelview matrix
	//op::math::Matrix<GLfloat, 4, 4> modelview = op::math::IdentityMatrix<GLfloat, 4>();
	glm::mat4 modelview = glm::mat4(1.0f);

	auto renderer = op::lib3D::Engine3D::GetInstance()->GetRenderer();

	// set up m-v-p matrices for Basic-Lighting shader
	{
		GLuint glProgramId = renderer->GetShaderId("BasicLighting");
		renderer->UseShader(glProgramId);

		renderer->SetCameraTransform(&cameraMatrix[0][0]);
		renderer->SetModelTransform(&modelview[0][0]);
		renderer->SetProjection(&projectionMatrix[0][0]);
    }
		//renderer->UseShader(glProgramId);
}



void wxGLRenderWindow::Render(void)
{
	if (!IsShown()) return;

	wxGLCanvas::SetCurrent(*m_context);
	wxPaintDC(this); // only to be used in paint events. use wxClientDC to paint outside the paint event

	glClearColor(0.3f, 0.4f, 1.0f, 1.0f); // Black Background
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// ------------- draw some 3D ----------------
	//prepare3DViewport(0, 0, GetWidth(), GetHeight());
	prepare3DViewport_shaders(0, 0, GetWidth(), GetHeight());

	op::lib3D::IRenderer3D* renderer = op::lib3D::Engine3D::GetInstance()->GetRenderer();
	

	//		BACKGROUND PASS
	op::lib3D::RenderArgs args;
	args.renderPass = op::lib3D::RenderPass::BACKGROUND;
	args.view = this;
	this->RenderEvt(*renderer, args);


	//		FOREGROUND PASS
	glClear(GL_DEPTH_BUFFER_BIT);
	args.renderPass = op::lib3D::RenderPass::FOREGROUND;
	this->RenderEvt(*renderer, args);

	glFlush();
	SwapBuffers();
}


void wxGLRenderWindow::OnPaint(wxPaintEvent& evt)
{
	wxGLCanvas::SetCurrent(*m_context);
	this->Render();
}


void wxGLRenderWindow::OnRenderTick(wxTimerEvent& evt)
{
	this->Refresh();
}


vector2i wxGLRenderWindow::GetSize(void) const
{
	return op::math::vector2i(this->GetWidth(), this->GetHeight());
}


unsigned int wxGLRenderWindow::GetWidth(void) const
{
	return (unsigned int)wxGLCanvas::GetSize().x;
}


unsigned int wxGLRenderWindow::GetHeight(void) const
{
	return (unsigned int)wxGLCanvas::GetSize().y;
}


op::lib3D::Ray3D wxGLRenderWindow::Unproject(const vector2i pos) const
{
	auto projection = this->GetProjectionMatrix();
	auto modelview = this->GetModelViewMatrix();

	int x = pos.X();
	int y = this->GetHeight() - pos.Y();

	//int y = pos.X();
	//int x = this->GetHeight() - pos.Y();

	//int y = pos.X();
	//int x = pos.Y();

	auto start = glm::unProject(
		glm::vec3(x,y, 0.0),
		modelview,
		projection,
		glm::vec4(0, 0, this->GetWidth(), this->GetHeight())
		);

	auto end = glm::unProject(
		glm::vec3(x,y, 1.0),
		modelview,
		projection,
		glm::vec4(0, 0, this->GetWidth(), this->GetHeight())
		);

	return op::lib3D::Ray3D(
		op::math::vector3d(start.x, start.y, start.z),
		op::math::vector3d(end.x - start.x, end.y - start.y, end.z - start.z).Unitize()
		);

}


const op::lib3D::Camera& wxGLRenderWindow::GetCamera() const
{
	return this->m_camera;
}


glm::mat4 wxGLRenderWindow::GetProjectionMatrix(void) const
{
	float ratio_w_h = (float)(this->GetWidth()) / (float)(this->GetHeight());
	double distanceFromCenter = m_camera.Position().DistanceTo(m_camera.Center());

	glm::mat4 projectionMatrix = glm::perspective(
		45.0f,         // The horizontal Field of View, in degrees : the amount of "zoom". Think "camera lens". Usually between 90� (extra wide) and 30� (quite zoomed in)
		(float)ratio_w_h, // Aspect Ratio. Depends on the size of your window. Notice that 4/3 == 800/600 == 1280/960, sounds familiar ?
		(float)(0.1 * distanceFromCenter),        // Near clipping plane. Keep as big as possible, or you'll get precision issues.
		(float)(3.0 * distanceFromCenter)       // Far clipping plane. Keep as little as possible.
		);

	return projectionMatrix;
}


glm::mat4 wxGLRenderWindow::GetCameraMatrix(void) const
{
	vector3d viewPos = m_camera.Position();
	vector3d center = m_camera.Center();
	vector3d up = m_camera.Up();
	return glm::lookAt(GetGLMVector(viewPos), GetGLMVector(center), GetGLMVector(up));
}


glm::mat4 wxGLRenderWindow::GetModelMatrix(void) const
{
	return glm::mat4(1.0f);
}


glm::mat4 wxGLRenderWindow::GetModelViewMatrix(void) const
{
	return this->GetCameraMatrix() * this->GetModelMatrix();
}


void wxGLRenderWindow::mouseMoved(wxMouseEvent& event) 
{
	op::wxWidgets::WxWinodwMouseEvtArgs args(event, *this);

	this->m_cameraMan.OnMouseMove(args);

	this->m_mouse.OnMouseMove(args);
}


void wxGLRenderWindow::mouseDown(wxMouseEvent& event) 
{
	op::wxWidgets::WxWinodwMouseEvtArgs args(event, *this);

	this->m_mouse.OnMouseClick(args);
}


void wxGLRenderWindow::rightClick(wxMouseEvent& event) 
{
	op::wxWidgets::WxWinodwMouseEvtArgs args(event, *this);

	this->m_mouse.OnMouseClick(args);
}


void wxGLRenderWindow::mouseWheel(wxMouseEvent& event) 
{
	//op::wxWidgets::WxWinodwMouseEvtArgs args(event);

	//this->m_cameraMan.OnMouse
}


void wxGLRenderWindow::keyPressed(wxKeyEvent& event)
{
	op::wxWidgets::WxWindowKeyboardEventArgs args(event);

	this->m_cameraMan.OnKeyPressed(args);

	this->m_keyboard.OnKeyPressed(args);
}
