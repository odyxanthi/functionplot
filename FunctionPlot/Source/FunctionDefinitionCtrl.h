#ifndef __FUNCTIONDEFINITIONCTRL_H__
#define __FUNCTIONDEFINITIONCTRL_H__

#include "stdafx.h"

//#include "wx/wxprec.h"
//#ifndef WX_PRECOMP
//#include "wx/wx.h"
//#endif
//#include <wx/docview.h>
//#include "wx/notebook.h"
//#include "wx/aui/aui.h"


namespace op { namespace fplot
{
	class FunctionDefinitionCtrl : public wxPanel
	{
	public:

		FunctionDefinitionCtrl(wxWindow* parent = nullptr,
			wxWindowID id = wxID_ANY,
			const wxPoint &pos = wxDefaultPosition,
			const wxSize &size = wxDefaultSize);


		virtual ~FunctionDefinitionCtrl(){};

	public:

	private:
		wxDECLARE_EVENT_TABLE();

		void	OnApplyButtonClick(wxCommandEvent& evt);

		void	OnMeshButtonClick(wxCommandEvent& evt);

		void	OnClearButtonClick(wxCommandEvent& evt);

	private:

		void GetParameters(std::string& formula, std::string& xDomain, std::string& yDomain);

		void Initialize(void);

	private:

		
		wxButton*	applyButton;
		wxStaticText* defineFunctionLabel;
		wxTextCtrl* functionFormulaTxtCtrl;
		wxTextCtrl* xMinText;
		wxTextCtrl* xMaxText;
		wxTextCtrl* yMinText;
		wxTextCtrl* yMaxText;
		wxTextCtrl* xSampleDistanceText;
		wxTextCtrl* ySampleDistanceText;

	private:

		//static long applyButtonID;

	};
}}

#endif