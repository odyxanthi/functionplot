#include "stdafx.h"
#include "FunctionModel3D.h"
#include "IFunctionEvaluator.h"
#include "NotImplementedException.h"

#include ".\Lib3D\Include\GeometryUtils.h"
#include ".\Lib3D\Include\MeshModel3D.h"
#include ".\Lib3D\Include\PolylineModel3D.h"
#include ".\Lib3D\Include\TextModel3D.h"
#include ".\Lib3D\Include\Mesh3D.h"
#include ".\Lib3D\Include\IRenderer3D.h"
#include ".\Lib3D\Include\RenderArgs.h"
#include ".\Lib3D\Include\StockMaterials.h"

#include <sstream>
#include <numeric>



using std::unique_ptr;


namespace op { namespace fplot
{
	const Material FunctionModel3D::_surfaceMaterial = 
		op::lib3D::Material(
			Color(0.25f, 0.25f, 0.25f, 0.80f), 
			Color(0.8f, 0.0f, 1.0f, 0.80f), 
			Color(0.1f, 0.1f, 0.1f, 0.80f), 
			Color(0.0f, 0.0f, 0.0f, 0.80f)
		);


	const Material FunctionModel3D::_isoCurveMaterial = 
		op::lib3D::Material(
			Color(0.25f, 0.25f, 0.25f, 0.80f), 
			Color(0.8f, 0.8f, 1.0f, 0.80f), 
			Color(0.1f, 0.1f, 0.1f, 0.80f), 
			Color(0.0f, 0.0f, 0.0f, 0.80f)
		);


	FunctionModel3D::FunctionModel3D(unique_ptr<IFunctionEvaluator> evaluator)
	: _evaluator(std::move(evaluator))
	, _meshModel(new op::lib3D::MeshModel3D)
	, _textModel(new op::lib3D::BillboardText)
	, _viewTransform(1.0)	// identity matrix
	{
		this->Initialize();
	}


	FunctionModel3D::~FunctionModel3D()
	{}


	void FunctionModel3D::Initialize()
	{
		_textModel->SetFontSize(10.0f);
		this->SetDisplayArea(vector3d(-100, -100, -100), vector3d(100, 100, 100));
	}


	void FunctionModel3D::SetFunction(const std::string& s, const FunctionEvaluationParams& params)
	{
		//if (this->_evaluator->SetFormula(s.c_str()))
		this->_evaluator->SetFormula(s.c_str());
		{
			_functionFormula = s;
			_evaluationParams = params;
		}
		//else
		//{
		//	_functionFormula = "";
		//	_evaluationParams = FunctionEvaluationParams(0, 0, 0, 0, 0, 0);
		//}

		std::string label = "f(x,y) = ";
		label += s;
		_textModel->SetText(label);

		this->UpdateFormulaDependentProperties();
	}


	void FunctionModel3D::SetDisplayArea(const vector3d& areaMin, const vector3d& areaMax)
	{
		_displayAreaMin = areaMin;
		_displayAreaMax = areaMax;
		this->UpdateDisplayAreaDependentProperties();
	}


	void FunctionModel3D::Render(op::lib3D::IRenderer3D& renderer, const op::lib3D::RenderArgs& args)
	{
		if (args.renderPass == RenderPass::BACKGROUND)
		{
			if (_meshModel != nullptr)
			{
				renderer.EnableLighting(true);

				renderer.SetModelTransform(&_viewTransform[0][0]);

				renderer.SetMaterial(FunctionModel3D::_surfaceMaterial);
				_meshModel->Render(renderer, args);

				renderer.SetMaterial(FunctionModel3D::_isoCurveMaterial);
				for (auto it = _xyCurves.begin(); it != _xyCurves.end(); ++it)
				{
					(*it)->Render(renderer, args);
				}

				auto identity = mat4d(1.0);
				renderer.SetModelTransform(&identity[0][0]);
			}
		}
		else if (args.renderPass == RenderPass::FOREGROUND)
		{
			_textModel->Render(renderer, args);
		}
	}


	const op::lib3D::Mesh3D* FunctionModel3D::GetRenderMesh(void) const
	{
		if (_meshModel)
		{
			return _meshModel->GetMesh();
		}
		return nullptr;
	}


	void FunctionModel3D::UpdateFormulaDependentProperties(void)
	{
		this->UpdateRenderMesh();
		this->UpdateXYIsoCurves();
		this->UpdateViewTransform();
	}


	void FunctionModel3D::UpdateDisplayAreaDependentProperties(void)
	{
		this->UpdateViewTransform();
		_textModel->SetPosition((_displayAreaMin + _displayAreaMax)*0.5);
	}


	void FunctionModel3D::UpdateViewTransform(void)
	{
		vector3d modelBboxMin, modelBboxMax;

		if (!fplot::GetFunctionMinMax(*this, modelBboxMin, modelBboxMax))
		{
			// Failed to calculate min, max. This can happen if formula is not valid. Set some default extents..
			modelBboxMin = vector3d(-100, -100, -100);
			modelBboxMax = vector3d(100, 100, 100);
		}
		//_axisModel3D->SetExtents(_modelBboxMin, _modelBboxMax);

		const double extentsX = _displayAreaMax.X() - _displayAreaMin.X();
		const double extentsY = _displayAreaMax.Y() - _displayAreaMin.Y();
		const double extentsZ = _displayAreaMax.Z() - _displayAreaMin.Z();

		double sx = extentsX / (modelBboxMax.X() - modelBboxMin.X());
		double sy = extentsY / (modelBboxMax.Y() - modelBboxMin.Y());
		double sz = (modelBboxMax.Z() - modelBboxMin.Z()) <= extentsZ
					? 1.0
					: extentsZ / (modelBboxMax.Z() - modelBboxMin.Z());

		vector3d displayAreaMidPt = (_displayAreaMin + _displayAreaMax)*0.5;
		vector3d modelBboxMidPt = (modelBboxMin + modelBboxMax)*0.5;
		vector3d translateVec = displayAreaMidPt - modelBboxMidPt;

		using vec3d = glm::tvec3<double>;
		_viewTransform = mat4d(1.0);
		_viewTransform = glm::translate(_viewTransform, vec3d(translateVec.X(), translateVec.Y(), translateVec.Z()));
		_viewTransform = glm::scale(_viewTransform, vec3d(sx, sy, sz));
	}


	void FunctionModel3D::UpdateRenderMesh(void)
	{
		_meshModel.reset();

		if (this->_evaluator->IsFunctionValid() == false)
			return;

		int xSampleSize = (int)((_evaluationParams.maxx - _evaluationParams.minx) / _evaluationParams.sampleDistX + 1);
		int ySampleSize = (int)((_evaluationParams.maxy - _evaluationParams.miny) / _evaluationParams.sampleDistY + 1);

		std::vector<double> xPositions(xSampleSize + 1);
		std::vector<double> yPositions(ySampleSize + 1);
		for (int i = 0; i <= xSampleSize; i++)	{ xPositions[i] = (_evaluationParams.minx + _evaluationParams.sampleDistX*i); }
		for (int i = 0; i <= ySampleSize; i++)	{ yPositions[i] = (_evaluationParams.miny + _evaluationParams.sampleDistY*i); }

		std::function<unsigned int(unsigned int, unsigned int)> gridPosToArrayPos = [&xPositions, &yPositions](unsigned int xIndex, unsigned int yIndex) -> unsigned int { return xIndex* (unsigned int)yPositions.size() + yIndex; };


		std::vector<op::lib3D::Vertex> vertices;
		for (unsigned int xIndex = 0; xIndex < xPositions.size(); xIndex++)
		{
			for (unsigned int yIndex = 0; yIndex < yPositions.size(); yIndex++)
			{
				int index = gridPosToArrayPos(xIndex, yIndex);

				double x = xPositions[xIndex];
				double y = yPositions[yIndex];
				double z;
				this->_evaluator->GetValueAt(x, y, z);

				vertices.push_back(op::lib3D::Vertex(op::math::vector3d(x, y, z)));
			}
		}



		//fplot::Root::GetApplication()->Log("vertices = " + std::to_string(xPositions.size()*yPositions.size()));

		std::vector<op::lib3D::Triangle> faces;
		for (unsigned int xIndex = 0; xIndex < xPositions.size() - 1; xIndex++)
		{
			for (unsigned int yIndex = 0; yIndex < yPositions.size() - 1; yIndex++)
			{
				op::lib3D::Triangle f;
				f.v1 = gridPosToArrayPos(xIndex, yIndex);
				f.v2 = gridPosToArrayPos(xIndex + 1, yIndex);
				f.v3 = gridPosToArrayPos(xIndex + 1, yIndex + 1);
				faces.push_back(f);

				f.v1 = gridPosToArrayPos(xIndex, yIndex);
				f.v2 = gridPosToArrayPos(xIndex + 1, yIndex + 1);
				f.v3 = gridPosToArrayPos(xIndex, yIndex + 1);
				faces.push_back(f);
			}
		}


		for (auto& f : faces)
		{
			op::math::vector3d v1 = vertices[f.v2].position - vertices[f.v1].position;
			op::math::vector3d v2 = vertices[f.v3].position - vertices[f.v1].position;

			op::math::vector3d n = v1.CrossProduct(v2);
			n.Unitize();

			vertices[f.v1].normal += n;
			vertices[f.v2].normal += n;
			vertices[f.v3].normal += n;

			vertices[f.v1].degree++;
			vertices[f.v2].degree++;
			vertices[f.v3].degree++;
		}

		for (auto& vertex : vertices)
		{
			vertex.normal /= vertex.degree;
			vertex.normal.Unitize();
		}

		auto mesh = make_unique<Mesh3D>(std::move(vertices), std::move(faces));
		_meshModel.reset(new MeshModel3D(std::move(mesh)));
		
	}


	void FunctionModel3D::UpdateXYIsoCurves(void)
	{
		this->_xyCurves.clear();


		if (this->_evaluator->IsFunctionValid() == false)
			return;
		
		unsigned int xSampleSize = (_evaluationParams.maxx - _evaluationParams.minx) / _maxIsoCurveDistance;
		if (xSampleSize<_minIsoCurveCount)
			xSampleSize = _minIsoCurveCount;
		std::vector<double> xSamplePoints = op::lib3D::GeometryUtils::SampleDomain(_evaluationParams.minx, _evaluationParams.maxx, xSampleSize);


		unsigned int ySampleSize = (_evaluationParams.maxy - _evaluationParams.miny) / _maxIsoCurveDistance;
		if (ySampleSize<_minIsoCurveCount)
			ySampleSize = _minIsoCurveCount;
		std::vector<double> ySamplePoints = op::lib3D::GeometryUtils::SampleDomain(_evaluationParams.miny, _evaluationParams.maxy, ySampleSize);


		// x iso curves
		for (int i = 1; i<ySamplePoints.size()-1;++i)
		{
			double y = ySamplePoints[i];
			auto samplePoints = this->GetIsoCurveSamplePoints(0, y);
			auto polylineModel = std::make_unique<op::lib3D::PolylineModel3D>();
			polylineModel->SetPoints(samplePoints.begin(), samplePoints.end());
			_xyCurves.push_back(std::move(polylineModel));
		}

		// y iso curves
		for (int i = 1; i<xSamplePoints.size() - 1; ++i)
		{
			double x = xSamplePoints[i];
			auto samplePoints = this->GetIsoCurveSamplePoints(1, x);
			auto polylineModel = std::make_unique<op::lib3D::PolylineModel3D>();
			polylineModel->SetPoints(samplePoints.begin(), samplePoints.end());
			_xyCurves.push_back(std::move(polylineModel));
		}
	}


	std::vector<vector3d> FunctionModel3D::GetIsoCurveSamplePoints(int dir, double isoParam)
	{
		const double& minval = (dir == 0) ? this->_evaluationParams.minx : this->_evaluationParams.miny;
		const double& maxval = (dir == 0) ? this->_evaluationParams.maxx : this->_evaluationParams.maxy;
		
		vector<double> xvalues;
		vector<double> yvalues;
		if (dir == 0)
		{
			xvalues = op::lib3D::GeometryUtils::SampleDomain(_evaluationParams.minx, _evaluationParams.maxx, 200);
			yvalues.resize(xvalues.size());
			std::fill_n(yvalues.begin(), xvalues.size(), isoParam);
		}
		else
		{
			yvalues = op::lib3D::GeometryUtils::SampleDomain(_evaluationParams.miny, _evaluationParams.maxy, 200);
			xvalues.resize(yvalues.size());
			std::fill_n(xvalues.begin(), yvalues.size(), isoParam);
		}

		vector<vector3d> samplePoints;
		bool pointsSuccessfullyEvaluated = true;
		for (size_t i = 0; i < xvalues.size(); ++i)
		{
			double x = xvalues[i];
			double y = yvalues[i];
			double z;
			if (this->_evaluator->GetValueAt(x, y, z))
			{
				samplePoints.push_back(vector3d(x, y, z));
			}
			else
			{
				pointsSuccessfullyEvaluated = false;
				samplePoints.clear();
				break;
			}
		}

		return samplePoints;
	}


	FunctionEvaluationParams::FunctionEvaluationParams(const std::string& xparams, const std::string& yparams)
	{
		std::stringstream ssx(xparams);
		ssx >> this->minx >> this->maxx >> this->sampleDistX;

		std::stringstream ssy(yparams);
		ssy >> this->miny >> this->maxy >> this->sampleDistY;
	}


	bool GetFunctionMinMax(const FunctionModel3D& model, vector3d& min, vector3d& max)
	{
		min = vector3d(
			std::numeric_limits<double>::max(),
			std::numeric_limits<double>::max(),
			std::numeric_limits<double>::max());

		max = vector3d(
			std::numeric_limits<double>::min(),
			std::numeric_limits<double>::min(),
			std::numeric_limits<double>::min());


		const op::lib3D::Mesh3D* mesh = model.GetRenderMesh();
		if (mesh != nullptr && mesh->GetVertices().size() > 0)
		{
			for (auto& v : mesh->GetVertices())
			{
				if (v.position.X() < min.X()) min.X() = v.position.X();
				if (v.position.Y() < min.Y()) min.Y() = v.position.Y();
				if (v.position.Z() < min.Z()) min.Z() = v.position.Z();

				if (v.position.X() > max.X()) max.X() = v.position.X();
				if (v.position.Y() > max.Y()) max.Y() = v.position.Y();
				if (v.position.Z() > max.Z()) max.Z() = v.position.Z();
			}

			return true;
		}
		
		return false;
	}
}}