#ifndef __WXWINDOWKEYBOARDEVENTARGS_H__
#define __WXWINDOWKEYBOARDEVENTARGS_H__

#include "stdafx.h"
#include ".\Lib3D\Include\KeyboardEvtArgs.h"

using op::math::vector2i;

namespace op
{
	namespace wxWidgets
	{
		class WxWindowKeyboardEventArgs : public op::lib3D::KeyboardEvtArgs
		{
		public:

			WxWindowKeyboardEventArgs(const wxKeyEvent& keyEvent)
				: _keyEvent(keyEvent)
			{}


		public:

			virtual int GetKeyCode(void) const override
			{
				return this->_keyEvent.GetKeyCode();
			}

		private:

			wxKeyEvent _keyEvent;
		};
	}
}


#endif

