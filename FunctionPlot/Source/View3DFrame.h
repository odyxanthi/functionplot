#ifndef __VIEW3DFRAME_H__
#define __VIEW3DFRAME_H__

#include "stdafx.h"
#include <memory>
#include <wx/docview.h>



class View3DFrame : public wxFrame
{
public:
	View3DFrame(wxFrame* parent,
				wxWindowID id,
				const wxString& title);

	virtual ~View3DFrame();

public:

	void Redraw();

protected:

	void OnClose(wxCloseEvent& closeEvt);

	void OnMouseEvent(wxMouseEvent& event);
	void OnResize(wxSizeEvent& evt);
	void OnPaint(wxPaintEvent& evt);

	wxDECLARE_EVENT_TABLE();

private:
	
	//wxOgreRenderWindow* ogreCanvas;
	static unsigned int framesCreated;
};


#endif