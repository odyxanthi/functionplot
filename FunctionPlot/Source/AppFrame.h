#ifndef __APPFRAME_H__
#define __APPFRAME_H__

#include "stdafx.h"
#include <memory>
#include "wx/notebook.h"
#include "wx/aui/aui.h"


class View3DFrame;
class Generic3DToolbox;


namespace op { namespace lib3D
{
	class Toolbox;
}}


namespace op { namespace fplot
{
	class FunctionPlotApp;

	class AppFrame : public wxFrame
	{
	public:
		AppFrame(
			FunctionPlotApp& app,
			wxWindow* parent,
			wxWindowID id,
			const wxString &  	title,
			const wxPoint &  	pos = wxDefaultPosition,
			const wxSize &  	size = wxDefaultSize,
			long  	style = wxDEFAULT_FRAME_STYLE,
			const wxString &  	name = wxFrameNameStr);


		virtual ~AppFrame();

		wxAuiManager& GetAUIManager(void){ return m_mgr; }

		void AddTabbedWindow(std::unique_ptr<wxWindow> wnd);

	private:

		void OnNew3DWindow(wxCommandEvent& event);
		void OnEditFunctionParams(wxCommandEvent& event);
		void OnDeleteAllGeometries(wxCommandEvent& event);

		void OnResize(wxSizeEvent& evt);
		void OnExit(wxCommandEvent& event);
		void OnAbout(wxCommandEvent& event);

		wxDECLARE_EVENT_TABLE();

	private:

		//wxNotebook* m_bookCtrl;
		wxAuiNotebook * m_bookCtrl{ nullptr };
		wxAuiManager m_mgr;
		FunctionPlotApp& m_app;

		friend class FunctionPlotApp;
	};

}}



#endif