#ifndef __IFUNCTIONEVALUATOR_H__
#define __IFUNCTIONEVALUATOR_H__


namespace op { namespace fplot
{

	class IFunctionEvaluator
	{
	public:

		virtual ~IFunctionEvaluator(){}; 

	public:

		virtual bool SetFormula(const char* formula) = 0;

		virtual bool IsFunctionValid(void) = 0;

		virtual const char* GetFormula(void) = 0;

		virtual bool GetValueAt(double x, double y, double& z) = 0;

	};
}}



#endif