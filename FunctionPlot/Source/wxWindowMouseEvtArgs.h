#ifndef __WXWINDOWMOUSEEVTARGS_H__
#define __WXWINDOWMOUSEEVTARGS_H__

#include "stdafx.h"
#include ".\Lib3D\Include\MouseEvtArgs.h"

using op::math::vector2i;

namespace op { namespace lib3D
{
	class IView3D;
}}

namespace op { namespace wxWidgets
{
	class WxWinodwMouseEvtArgs : public op::lib3D::MouseEvtArgs
	{
	public:

		WxWinodwMouseEvtArgs(const wxMouseEvent& mouseEvt, const op::lib3D::IView3D& view);

	public:

		virtual vector2i GetMousePosition(void) const override;

		virtual bool LeftButtonDown(void) const override;
		virtual bool LeftButtonClick(void) const override;
		virtual bool RightButtonDown(void) const override;
		virtual bool RightButtonClick(void) const override;
		virtual bool MiddleButtonDown(void) const override;
		virtual bool MiddleButtonClick(void) const override;

	public:

		virtual bool ControlDown(void) const override;
		virtual bool ShiftDown(void) const override;

	private:

		wxMouseEvent _wxMouseEvt;
	};
}}


#endif

