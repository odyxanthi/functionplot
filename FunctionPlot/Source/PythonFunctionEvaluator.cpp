#include "stdafx.h"
#include "IFunctionEvaluator.h"
#include "PythonFunctionEvaluator.h"
#include "Python.h"
#include <sstream>
#include <iostream>




namespace op { namespace fplot
{
	
	PythonEvaluatorFactory::PythonEvaluatorFactory()
		:
		pClass(0)
	{
		// Initialize the Python Interpreter
		Py_Initialize();

		char* moduleName = "Calculator";
		char* className = "Calculator";

		// Build the name object
		void* pName = PyString_FromString(moduleName);
		void* pModule = PyImport_Import((PyObject *)pName);
		if (pModule != nullptr)
		{
			PyObject* pDict = PyModule_GetDict((PyObject *)pModule);
			this->pClass = PyDict_GetItemString((PyObject *)pDict, className);

			if (PyCallable_Check((PyObject *)pClass))
			{
				// success
			}
			else
			{
				throw std::exception("Failed to get handle to calculator class");
			}
		}
		else
		{
			throw std::exception("Failed to import module");
		}
	}


	PythonEvaluatorFactory::~PythonEvaluatorFactory()
	{
		Py_Finalize();
	}

	
	unique_ptr<IFunctionEvaluator> PythonEvaluatorFactory::CreateEvaluator()
	{
		void* pInstance = PyObject_CallObject((PyObject *)pClass, NULL);
		if (pInstance != 0)
		{
			IFunctionEvaluator* evaluator = new PythonBasedFunctionEvaluator(pInstance);
			return unique_ptr<IFunctionEvaluator>(evaluator);
		}
		return unique_ptr<IFunctionEvaluator>();
	}

	


	PythonBasedFunctionEvaluator::PythonBasedFunctionEvaluator(void* pInstance)
		:
		_pInstance(pInstance),
		_formula(""),
		_functionValid(false)
	{
		this->AssertAllPythonFunctionsFound();
	}


	PythonBasedFunctionEvaluator::~PythonBasedFunctionEvaluator()
	{}


	void PythonBasedFunctionEvaluator::AssertAllPythonFunctionsFound(void)
	{
		
		PyObject* pValue = PyObject_CallMethod((PyObject *)_pInstance, "BuildFunction2D", "(s)", "x+y");
		if (pValue != nullptr)
		{
			// success
		}
		else
		{
			throw std::exception("Failed to locate function: BuildFunction2D");
		}
	}


	bool PythonBasedFunctionEvaluator::SetFormula(const char* formula)
	{
		bool success = this->BuildFunction2D( formula );
		if (success)
		{
			_formula = std::string(formula);
			_functionValid = true;
		}
		else
		{
			_formula = "";
			_functionValid = false;
		}
		return (success);
	}


	bool PythonBasedFunctionEvaluator::IsFunctionValid(void)
	{
		return _functionValid;
	}


	const char* PythonBasedFunctionEvaluator::GetFormula(void)
	{
		return (_formula.c_str());
	}


	bool PythonBasedFunctionEvaluator::GetValueAt(double x, double y, double& fxy)
	{
		char* funcName = "Eval2D";
		char* evalSuccessful = "LastEvaluationSuccessful";

		bool success = false;
		PyObject* pValue = PyObject_CallMethod((PyObject *)_pInstance, funcName, "dd", x, y);
		if (pValue == Py_None)
		{
			success = false;
		}
		else
		{
			fxy = PyFloat_AsDouble(pValue);
			success = true;
		}

		return success;
	}


	bool PythonBasedFunctionEvaluator::BuildFunction2D(const std::string& formulaAsStr)
	{
		char* funcName = "BuildFunction2D";

		bool setupSuccess = false;

		PyObject* pValue = PyObject_CallMethod((PyObject *)_pInstance, funcName, "(s)", formulaAsStr.c_str());
		if (pValue != nullptr)
		{
			if (pValue == Py_True)
			{
				setupSuccess = true;
			}
		}
		
		return setupSuccess;
	}

}}
