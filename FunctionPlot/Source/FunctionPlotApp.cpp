#include "stdafx.h"

#include "FunctionPlotApp.h"
#include "AppFrame.h"
#include "View3DFrame.h"
#include "Logger.h"
#include "RootEntity.h"
#include "wxGLRenderWindow.h"

#include ".\Lib3D\Include\Engine3D.h"
#include ".\Lib3D\Include\IRenderer3D.h"
#include ".\Lib3D\Include\SceneManager.h"


using op::lib3D::Engine3D;



namespace op { namespace fplot
{

	enum
	{
		ID_NEW_WINDOW_3D = 1,
		ID_NEW_WINDOW_CROSS_SECTION,
		ID_FUNCTION_PARAMS,
		ID_CROSS_SECTION_PARAMS,
		ID_TRANSLATIONTOOL,
		ID_EXIT
	};

	wxIMPLEMENT_APP(FunctionPlotApp);


	FunctionPlotApp* FunctionPlotApp::theApp = nullptr;


	bool FunctionPlotApp::OnInit()
	{
		FunctionPlotApp::theApp = this;

		try
		{
			op::lib3D::Engine3D::CreateInstance();

			AppFrame* theAppFrame = new AppFrame(*this, nullptr, wxID_ANY, "Function Plot");
			this->appFrame = theAppFrame;
			this->appFrame->SetSize(800,600);
			this->GetLogger()->WriteLine("Frame created ...");

			this->rootEntity = unique_ptr<op::fplot::RootEntity>(new RootEntity);

			theAppFrame->AddTabbedWindow(this->CreateRenderWindow());
			theAppFrame->Show(true);

			this->rootEntity->Init();

			//auto renderer = op::lib3D::Engine3D::GetInstance()->GetRenderer();

			return true;
		}
		catch (std::exception& e)
		{
			this->GetLogger()->WriteLine(e.what());
			return false;
		}
	}


	FunctionPlotApp::~FunctionPlotApp()
	{
		op::lib3D::Engine3D::DestroyInstance();
	}


	RootEntity* FunctionPlotApp::GetRootEntity(void)
	{
		return this->rootEntity.get();
	}


	op::lib3D::Engine3D* FunctionPlotApp::GetEngine3D(void)
	{
		return op::lib3D::Engine3D::GetInstance();
	}


	::core::Logger* FunctionPlotApp::GetLogger(void)
	{
		return this->GetEngine3D()->GetLogger();
	}


	std::unique_ptr<wxGLRenderWindow> FunctionPlotApp::CreateRenderWindow(void)
	{
		std::unique_ptr<wxGLRenderWindow> renderWindow(new wxGLRenderWindow(appFrame, nullptr));
		assert(rootEntity != nullptr);
		renderWindow->RenderEvt.connect(rootEntity.get(), &RootEntity::Render);
		return renderWindow;
	}
}}