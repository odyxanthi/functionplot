#ifndef __PYTHONFUNTIONEVALUATOR_H__
#define __PYTHONFUNTIONEVALUATOR_H__


#include "IFunctionEvaluator.h"
#include "IEvaluatorFactory.h"
#include "Python.h"
#include <string>
#include <vector>



namespace op { namespace fplot
{
	struct Coord2D
	{
		double x;
		double y;
	};


	class PythonEvaluatorFactory : public IEvaluatorFactory
	{
	public:

		//! Throws if it fails to initialize the python interpreter, or if it fails to load the python  module.
		PythonEvaluatorFactory();

		virtual ~PythonEvaluatorFactory();

	public:

		virtual unique_ptr<IFunctionEvaluator> CreateEvaluator() override;

	private:

		::PyObject* pClass;
	};


	
	//! Uses a python module to evaluate the function
	class PythonBasedFunctionEvaluator : public IFunctionEvaluator
	{
	private:

		friend class PythonEvaluatorFactory;

		PythonBasedFunctionEvaluator(void* pInstance);

	public:

		virtual ~PythonBasedFunctionEvaluator();

	public:

		virtual bool SetFormula(const char* formula);

		virtual bool IsFunctionValid(void);

		virtual const char* GetFormula(void);

		virtual bool GetValueAt(double x, double y, double& z);

		std::vector<double> EvalArray2D(const std::vector<Coord2D>& coords) const;

	private:

		bool BuildFunction2D(const std::string& formulaAsStr);

		//! try to locate all needed functions in python module and throw if not found.
		void AssertAllPythonFunctionsFound(void);

	private:

		void* _pInstance;
		std::string _formula;
		bool _functionValid;
	};

}}


#endif // __PYTHONFUNTIONEVALUATOR_H__