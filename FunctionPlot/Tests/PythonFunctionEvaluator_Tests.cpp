#include "gtest/gtest.h"
#include "PythonFunctionEvaluator.h"

#include <cmath>
#include <memory>

using namespace std;
using namespace op::fplot;

const double tolerance = 0.0000001;
PythonEvaluatorFactory factory;

TEST(PythonFunctionEvaluator, Constructor)
{
	unique_ptr<IFunctionEvaluator> eval = factory.CreateEvaluator();
	ASSERT_TRUE(eval.get() != nullptr);
}


TEST(PythonFunctionEvaluator, BuildFunction2DFromValidString)
{
	unique_ptr<IFunctionEvaluator> eval = factory.CreateEvaluator();
	ASSERT_TRUE(eval != nullptr);
	bool b = eval->SetFormula("10*x+y");
	ASSERT_TRUE(b);

	double actual;
	double expected = 54.0;
	bool evaluationSuccessful = eval->GetValueAt(5, 4, actual);
	
	ASSERT_TRUE(evaluationSuccessful);
	ASSERT_TRUE(abs(expected - actual) < tolerance);
}



TEST(PythonFunctionEvaluator, BuildFunctionFromInvalidString_InvalidExpression)
{
	unique_ptr<IFunctionEvaluator> eval = factory.CreateEvaluator();
	ASSERT_TRUE(eval != nullptr);
	bool b = eval->SetFormula("this is not a function!!!");
	ASSERT_FALSE(b);
}


TEST(PythonFunctionEvaluator, BuildFunctionFromInvalidString_SyntaxError)
{
	unique_ptr<IFunctionEvaluator> eval = factory.CreateEvaluator();
	ASSERT_TRUE(eval != nullptr);
	bool b = eval->SetFormula("10x+y");  // should be 10*x+y
	ASSERT_FALSE(b);
}


TEST(PythonFunctionEvaluator, BuildFunctionFromInvalidString_EmptyString)
{
	unique_ptr<IFunctionEvaluator> eval = factory.CreateEvaluator();
	ASSERT_TRUE(eval != nullptr);
	bool b = eval->SetFormula("  ");
	ASSERT_FALSE(b);
}


TEST(PythonFunctionEvaluator, BuildFunction2D_ValidFormula_DivisionByZero)
{
	unique_ptr<IFunctionEvaluator> eval = factory.CreateEvaluator();
	ASSERT_TRUE(eval != nullptr);
	bool b = eval->SetFormula("(x+y)/0");
	ASSERT_TRUE(b);
}


TEST(PythonFunctionEvaluator, EvaluateCoordsWhereFunctionIsUndefined)
{
	unique_ptr<IFunctionEvaluator> eval = factory.CreateEvaluator();
	ASSERT_TRUE(eval != nullptr);
	bool b = eval->SetFormula("x/y");
	ASSERT_TRUE(b);

	double z;
	bool evaluationSuccess;

	//Assert that false is returned if function is undefined at given coordinates.
	evaluationSuccess = eval->GetValueAt(1, 0, z);
	ASSERT_FALSE(evaluationSuccess);
}


TEST(PythonFunctionEvaluator, CheckEvaluatorWorksNormallyAfterEvaluationError)
{
	unique_ptr<IFunctionEvaluator> eval = factory.CreateEvaluator();
	ASSERT_TRUE(eval != nullptr);
	bool b = eval->SetFormula("x/y");
	ASSERT_TRUE(b);

	double z;
	bool evaluationSuccess;
	
	//Assert that false is returned if function is undefined at given coordinates.
	evaluationSuccess = eval->GetValueAt(1, 0, z);
	ASSERT_FALSE(evaluationSuccess);

	//Assert that subsequent calls with valid values return true
	evaluationSuccess = eval->GetValueAt(10, 5, z);
	ASSERT_TRUE(evaluationSuccess);
	ASSERT_TRUE(abs(z - 2.0) < tolerance);
}
