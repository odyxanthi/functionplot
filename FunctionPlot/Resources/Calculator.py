import sys
import random
import string
from math import *


def frange ( start, end, step ):
    x = start
    while x <= end:
        yield x
        x += step
        
		
class Calculator:		

	def __init__(self):
		self.functionFormula = ''
		self.symbolReplacementMap = dict();
		self.symbolReplacementMap['^'] = '**';



	def __ConvertFormula ( self, formulaStr ):
		modifiedStr = formulaStr
		for x in self.symbolReplacementMap.keys():
			modifiedStr = modifiedStr.replace(x, self.symbolReplacementMap[x] )
		return modifiedStr


	def __Set2DFunc(self,f):
		self.f_x_y = f

	def __Get2DFunc(self):
		return self.f_x_y


	def LastEvaluationSuccessful():
		return self.evalSuccess


	def BuildFunction2D ( self, formulaAsStr ):
		
		try:
			normalizedStr = formulaAsStr.rstrip().lstrip()
			if len(normalizedStr) == 0:
				return False

			self.functionFormula = self.__ConvertFormula(normalizedStr)

			exec ( 'def f(x,y): return ( %s )' % self.functionFormula )
			if self.IsValid2DFunction(f):
				self.__Set2DFunc(f)
				return True
			else:
				self.__Set2DFunc(None)
				return False
		except:
			self.__Set2DFunc(None)
			return False


	def IsValid2DFunction(self, f):
		try:
			valOut = f(0,0)
			if valOut is None:
				return False
			else:
				return True		# function valid
			
		except ArithmeticError:
			return True		# if arithmetic error e.g. division by zero occurs, consider the function valid
		except:
			return False


	def Eval2D(self, x, y):
		try:
			valOut = self.__Get2DFunc()(x,y)
			return valOut
		except:
			return None
		

	def EvalArray2D(self,inputValues):
		outputValues = list()
		for t in inputValues:
			outputValues.append ( self.Eval2D(t[0],t[1]) )
		return outputValues
		
		
		
	def EvalArrayStr2D (self, arrayAsString ):
	
		try:
			print 'python EvalArrayStr2D called'
		
			tuppleStrList 		= arrayAsString.split(';')
			inputValues			= list()
			for s in tuppleStrList:
				strList = s.split();
				if len(strList) == 2:
					t = float(strList[0]),float(strList[1])
					inputValues.append ( t )
				elif len(strList) == 0:
					pass
				else:
					message = 'cannot convert ' + s + 'to (x,y)'
					print message
					raise Exception(message)
				
			
			outputValues 		= self.EvalArray2D(inputValues)
			outputValuesStrList = [ str(y) for y in outputValues ]
			
			resultStr = ' '.join(outputValuesStrList)
			print 'python EvalArrayStr2D returns', resultStr
			return resultStr
		except:
			print 'exception in python EvalArrayStr2D'
			return None
		
		
    
	
def main():
	'''
	if len(sys.argv) < 2:
		print 'Usage: Calculator.py [fx] [eval positions]'
		print 'e.g. Calculator.py {3*x-1} [0,20,1]'
		return
	'''
	
	calculator = Calculator()
	calculator.BuildFunction1D('3*x - 2')
	
	print 'calculator.Eval1D(5)',calculator.Eval1D(5)
	print 'calculator.EvalArray1D(range(1,11))',calculator.EvalArray1D( range(1,11))
	print 'calculator.EvalArrayStr1D(\'1 2 3 4   5 6 7 8 9 10 11 11\')',calculator.EvalArrayStr1D('1 2 3 4   5 6 7 8 9 10 11 11')

	
	calculator.BuildFunction2D('x*x + y*y')
	s = '1 1;1 2; 1 3; 1 4; 1   5; 2 1; 2 2; 2  3 ; 2 4; 2 5; 2 6; ; ; 3 7 ;'
	print 'evaluating2DFunc with input', s, '-->',calculator.EvalArrayStr2D(s)
	
	
if __name__=="__main__":
	main()
	
	
	