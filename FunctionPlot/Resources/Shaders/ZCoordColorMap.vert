#version 140
 
in  vec3 in_Position;
in  vec3 in_Color;
uniform float upperLimit;
uniform float lowerLimit;

uniform mat4 camera;
uniform mat4 modelview;
uniform mat4 projection;

out vec3 ex_Color;
 
void main(void)
{
	vec4 v = vec4(in_Position,1);
	mat4 MVP = projection * camera * modelview;
	gl_Position = MVP*v;
	
	// calculate ex_Color
	float proportion = (v.z-lowerLimit)/(upperLimit-lowerLimit);
	
	vec3 red = vec3(1.0,0.0,0.0);
	vec3 yel = vec3(1.0,1.0,0.0);
	vec3 gre = vec3(0.0,1.0,0.0);
	vec3 blu = vec3(0.0,0.0,1.0);
	
	float yel_thres = 0.33;
	float gre_thres = 0.66;
	if (proportion < yel_thres)
	{
		float p = proportion/yel_thres;
		ex_Color = (1-p)*red + p*yel;
	}
	else if (proportion < gre_thres)
	{
		float p = (proportion-yel_thres)/(gre_thres-yel_thres);
		ex_Color = (1-p)*yel + p*gre;
	}
	else
	{
		float p = (proportion-gre_thres)/(1.0 - gre_thres);
		ex_Color = (1-p)*gre + p*blu;
	}
	
	//ex_Color = vec3(1.0,0.0,0.0)*proportion + vec3(0.0,0.0,1.0)*(1-proportion);
}



