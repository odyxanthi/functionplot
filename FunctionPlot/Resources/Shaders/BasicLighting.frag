#version 140
 
precision highp float; // needed only for version 1.30
 
in  vec4 outColor;
out vec4 fragColor;
in float myAlpha;
 
void main(void)
{
	//out_Color = vec4(0, 1, 0, 0.2);
	fragColor = outColor;
	fragColor.a = myAlpha;
}