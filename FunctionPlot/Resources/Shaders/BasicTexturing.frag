#version 140
 
precision highp float; // needed only for version 1.30
 
in  vec4 outColor;
out vec4 fragColor;
uniform sampler2D tex;
 
void main(void)
{
	fragColor = outColor;

	vec4 color = texture2D(tex, gl_TexCoord[0].st);
	fragColor = color;
}