#version 140
 
in  vec3 in_Position;
in  vec3 in_Color;

uniform mat4 camera;
uniform mat4 modelview;
uniform mat4 projection;

out vec4 outColor;
out float myAlpha;
 
void main(void)
{
	vec4 v = vec4(in_Position,1);
	mat4 MVP = projection * camera * modelview;
	gl_Position = MVP*v;

	// http://www.lighthouse3d.com/tutorials/glsl-tutorial/directional-lights-i/

	vec3 normal, lightDir;
	vec4 diffuse, ambient, globalAmbient;
	float NdotL;

	normal = normalize(gl_NormalMatrix * gl_Normal);


	for(int i = 0; i < 8; i++)
	{
		lightDir = normalize(vec3(gl_LightSource[i].position));
		NdotL = max(dot(normal, lightDir), 0.0);
		diffuse = gl_FrontMaterial.diffuse * gl_LightSource[i].diffuse;
		ambient = gl_FrontMaterial.ambient * gl_LightSource[i].ambient;
		gl_FrontColor =  gl_FrontColor + NdotL * diffuse + ambient;
	}

	globalAmbient = gl_LightModel.ambient * gl_FrontMaterial.ambient;
	outColor =  gl_FrontColor + globalAmbient;

	//myAlpha = abs(in_Position.z) / 10.0;
	myAlpha = gl_FrontMaterial.diffuse.a;
}



