#version 140
 
in  vec3 in_Position;
in  vec3 in_Color;

uniform mat4 camera;
uniform mat4 modelview;
uniform mat4 projection;

out vec4 outColor;
out float myAlpha;
 
void main(void)
{
	vec4 v = vec4(in_Position,1);
	mat4 MVP = projection * camera * modelview;
	gl_Position = MVP*v;

	outColor =  gl_Color;
	myAlpha = gl_Color.a;
}



